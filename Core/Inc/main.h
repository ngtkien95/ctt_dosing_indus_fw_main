/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
#define USE_DHCP       /* enable DHCP, if disabled static address is used */

#define DEST_IP_ADDR0   (uint8_t) 192
#define DEST_IP_ADDR1   (uint8_t) 168
#define DEST_IP_ADDR2   (uint8_t) 0
#define DEST_IP_ADDR3   (uint8_t) 101

#define DEST_PORT       (uint16_t) 7

/*Static IP ADDRESS: IP_ADDR0.IP_ADDR1.IP_ADDR2.IP_ADDR3 */
#define IP_ADDR0   (uint8_t) 192
#define IP_ADDR1   (uint8_t) 168
#define IP_ADDR2   (uint8_t) 0
#define IP_ADDR3   (uint8_t) 10

/*NETMASK*/
#define NETMASK_ADDR0   (uint8_t) 255
#define NETMASK_ADDR1   (uint8_t) 255
#define NETMASK_ADDR2   (uint8_t) 255
#define NETMASK_ADDR3   (uint8_t) 0

/*Gateway Address*/
#define GW_ADDR0   (uint8_t) 192
#define GW_ADDR1   (uint8_t) 168
#define GW_ADDR2   (uint8_t) 0
#define GW_ADDR3   (uint8_t) 1
/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define US_FS2_Pin GPIO_PIN_2
#define US_FS2_GPIO_Port GPIOE
#define US6_ECHO_EXTI15_Pin GPIO_PIN_3
#define US6_ECHO_EXTI15_GPIO_Port GPIOE
#define US4_TRIG_Pin GPIO_PIN_4
#define US4_TRIG_GPIO_Port GPIOE
#define US4_ECHO_EXTI11_Pin GPIO_PIN_5
#define US4_ECHO_EXTI11_GPIO_Port GPIOE
#define US3_TRIG_Pin GPIO_PIN_6
#define US3_TRIG_GPIO_Port GPIOE
#define US1_ECHO_EXTI10_Pin GPIO_PIN_13
#define US1_ECHO_EXTI10_GPIO_Port GPIOC
#define US1_TRIG_Pin GPIO_PIN_0
#define US1_TRIG_GPIO_Port GPIOC
#define US3_ECHO_EXTI13_Pin GPIO_PIN_2
#define US3_ECHO_EXTI13_GPIO_Port GPIOC
#define US2_TRIG_Pin GPIO_PIN_3
#define US2_TRIG_GPIO_Port GPIOC
#define US2_ECHO_EXTI14_Pin GPIO_PIN_0
#define US2_ECHO_EXTI14_GPIO_Port GPIOA
#define BTN_MODE_Pin GPIO_PIN_7
#define BTN_MODE_GPIO_Port GPIOE
#define BTN_DOWN_Pin GPIO_PIN_8
#define BTN_DOWN_GPIO_Port GPIOE
#define BTN_UP_Pin GPIO_PIN_9
#define BTN_UP_GPIO_Port GPIOE
#define BTN_SET_Pin GPIO_PIN_10
#define BTN_SET_GPIO_Port GPIOE
#define OP_BUZ_Pin GPIO_PIN_11
#define OP_BUZ_GPIO_Port GPIOE
#define LCD_D4_Pin GPIO_PIN_12
#define LCD_D4_GPIO_Port GPIOE
#define LCD_D5_Pin GPIO_PIN_13
#define LCD_D5_GPIO_Port GPIOE
#define LCD_D6_Pin GPIO_PIN_14
#define LCD_D6_GPIO_Port GPIOE
#define LCD_D7_Pin GPIO_PIN_15
#define LCD_D7_GPIO_Port GPIOE
#define LCD_EN_Pin GPIO_PIN_14
#define LCD_EN_GPIO_Port GPIOB
#define LCD_RS_Pin GPIO_PIN_15
#define LCD_RS_GPIO_Port GPIOB
#define OP_EC_Pin GPIO_PIN_8
#define OP_EC_GPIO_Port GPIOD
#define OP_PH_Pin GPIO_PIN_9
#define OP_PH_GPIO_Port GPIOD
#define OP_O1_Pin GPIO_PIN_10
#define OP_O1_GPIO_Port GPIOD
#define OP_O2_Pin GPIO_PIN_11
#define OP_O2_GPIO_Port GPIOD
#define OP_O3_Pin GPIO_PIN_12
#define OP_O3_GPIO_Port GPIOD
#define OP_O4_Pin GPIO_PIN_13
#define OP_O4_GPIO_Port GPIOD
#define OP_O5_Pin GPIO_PIN_14
#define OP_O5_GPIO_Port GPIOD
#define OP_O6_Pin GPIO_PIN_15
#define OP_O6_GPIO_Port GPIOD
#define MCU_PC6_Pin GPIO_PIN_6
#define MCU_PC6_GPIO_Port GPIOC
#define MCU_PC7_Pin GPIO_PIN_7
#define MCU_PC7_GPIO_Port GPIOC
#define MCU_PA8_Pin GPIO_PIN_8
#define MCU_PA8_GPIO_Port GPIOA
#define OP_O7_Pin GPIO_PIN_9
#define OP_O7_GPIO_Port GPIOA
#define OP_O8_Pin GPIO_PIN_10
#define OP_O8_GPIO_Port GPIOA
#define OP_O9_Pin GPIO_PIN_11
#define OP_O9_GPIO_Port GPIOA
#define OP_O10_Pin GPIO_PIN_12
#define OP_O10_GPIO_Port GPIOA
#define LED_SYS_Pin GPIO_PIN_0
#define LED_SYS_GPIO_Port GPIOD
#define MCU_PD1_Pin GPIO_PIN_1
#define MCU_PD1_GPIO_Port GPIOD
#define MCU_PD3_Pin GPIO_PIN_3
#define MCU_PD3_GPIO_Port GPIOD
#define ZB_RST_Pin GPIO_PIN_4
#define ZB_RST_GPIO_Port GPIOD
#define ZB_USART2_TX_Pin GPIO_PIN_5
#define ZB_USART2_TX_GPIO_Port GPIOD
#define ZB_USART2_RX_Pin GPIO_PIN_6
#define ZB_USART2_RX_GPIO_Port GPIOD
#define US_FS1_Pin GPIO_PIN_7
#define US_FS1_GPIO_Port GPIOD
#define RTC_I2C1_SCL_Pin GPIO_PIN_6
#define RTC_I2C1_SCL_GPIO_Port GPIOB
#define RTC_I2C_SDA_Pin GPIO_PIN_7
#define RTC_I2C_SDA_GPIO_Port GPIOB
#define RTC_RST_Pin GPIO_PIN_8
#define RTC_RST_GPIO_Port GPIOB
#define US5_ECHO_EXTI3_Pin GPIO_PIN_9
#define US5_ECHO_EXTI3_GPIO_Port GPIOB
#define US6_TRIG_Pin GPIO_PIN_0
#define US6_TRIG_GPIO_Port GPIOE
#define US5_TRIG_Pin GPIO_PIN_1
#define US5_TRIG_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
