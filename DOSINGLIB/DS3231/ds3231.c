/*
 * ds3231.c
 *
 *  Created on: 2020. 8. 16.
 *      Author: Cuong Le Hung
 */
#include "../include.h"
//#include "ds3231.h"

#define DS3231_ADDR  (0x68 << 1)

extern modeSetting_t modeSetting;

I2C_HandleTypeDef *i2c;
_DS3231 tRtc;

uint8_t nDs3231ResetTimerCount = 0;

static uint8_t B2D(uint8_t bcd);
static uint8_t D2B(uint8_t decimal);

void DS3231_Init(I2C_HandleTypeDef *handle)
{
  i2c = handle;
	tRtc.State = DS3231_IDLE;
	tRtc.Year = 20;
	tRtc.Month = 12;
	tRtc.Date = 2;
	tRtc.DaysOfWeek = 2;
	tRtc.Hour = 0;
	tRtc.Min = 0;
	tRtc.Sec = 0;
}

bool DS3231_Process(void)
{
	switch (tRtc.State)
	{
		case DS3231_IDLE:
			tRtc.State = DS3231_GET_TIME;
			break;
		case DS3231_GET_TIME:
			if (modeSetting != SETTING_RTC)
			{
				if (!DS3231_GetTime(&tRtc))
				{
					tRtc.State = DS3231_RESET_MASTER;
				}
			}
			break;
		case DS3231_SET_TIME:
			if (DS3231_SetTime(&tRtc))
			{
				tRtc.State = DS3231_GET_TIME;
			}
			else
			{
				tRtc.State = DS3231_RESET_MASTER;
			}
			break;
		case DS3231_RESET_MASTER:
			HAL_I2C_DeInit(i2c);
			HAL_I2C_Init(i2c);
		
			nDs3231ResetTimerCount = 2;
			tRtc.State = DS3231_RESET_SLAVE;
			break;
		case DS3231_RESET_SLAVE:
			if (nDs3231ResetTimerCount == 0)
			{
				tRtc.State = DS3231_GET_TIME;
			}
			break;
	}
	
	switch (tRtc.State)
	{
		case DS3231_IDLE:
		case DS3231_GET_TIME:
		case DS3231_SET_TIME:
		case DS3231_RESET_MASTER:
			DS3131_RESET((GPIO_PinState)GPIO_PIN_SET);
			break;
		case DS3231_RESET_SLAVE:
			DS3131_RESET((GPIO_PinState)GPIO_PIN_RESET);
			break;
	}
	
	return true;
}

bool DS3231_Timer(void)
{
	if (nDs3231ResetTimerCount > 0)
		nDs3231ResetTimerCount--;
	return true;
}

bool DS3231_GetTime(_DS3231 *rtc)
{
  uint8_t startAddr = DS3231_REG_TIME;
  uint8_t buffer[7] = {0,};

//	if(HAL_I2C_GetState(i2c) != HAL_I2C_STATE_READY)	return false;
  if(HAL_I2C_Master_Transmit(i2c, (uint16_t)DS3231_ADDR, (uint8_t*)&startAddr, (uint16_t)1, (uint32_t)1000) != HAL_OK) return false;
	if(HAL_I2C_Master_Receive(i2c, (uint16_t)DS3231_ADDR, (uint8_t*)buffer, sizeof(buffer), (uint32_t)1000) != HAL_OK) return false;

  rtc->Sec = B2D(buffer[0] & 0x7F);
  rtc->Min = B2D(buffer[1] & 0x7F);
  rtc->Hour = B2D(buffer[2] & 0x3F);
  rtc->DaysOfWeek = buffer[3] & 0x07;
  rtc->Date = B2D(buffer[4] & 0x3F);
  rtc->Month = B2D(buffer[5] & 0x1F);
  rtc->Year = B2D(buffer[6]);

  return true;
}

bool DS3231_SetTime(_DS3231 *rtc)
{
  uint8_t startAddr = DS3231_REG_TIME;
  uint8_t buffer[8] = {startAddr, 
											D2B(rtc->Sec), 
											D2B(rtc->Min), 
											D2B(rtc->Hour), 
											rtc->DaysOfWeek, 
											D2B(rtc->Date), 
											D2B(rtc->Month), 
											D2B(rtc->Year)};
  
//	if(HAL_I2C_GetState(i2c) != HAL_I2C_STATE_READY)	
//		return false;
	if(HAL_I2C_Master_Transmit(i2c, (uint16_t)DS3231_ADDR, (uint8_t*)buffer, sizeof(buffer), (uint32_t)1000) != HAL_OK) 
		return false;

  return true;
}

bool DS3231_ReadTemperature(float *temp)
{
  uint8_t startAddr = DS3231_REG_TEMP;
  uint8_t buffer[2] = {0,};

  if(HAL_I2C_Master_Transmit(i2c, DS3231_ADDR, &startAddr, 1, HAL_MAX_DELAY) != HAL_OK) return false;
  if(HAL_I2C_Master_Receive(i2c, DS3231_ADDR, buffer, sizeof(buffer), HAL_MAX_DELAY) != HAL_OK) return false;

  int16_t value = (buffer[0] << 8) | (buffer[1]);
  value = (value >> 6);

  *temp = value / 4.0f;
  return true;
}

bool DS3231_SetAlarm1(AlarmMode mode, uint8_t date, uint8_t hour, uint8_t min, uint8_t sec)
{
  uint8_t alarmSecond = D2B(sec);
  uint8_t alarmMinute = D2B(min);
  uint8_t alarmHour = D2B(hour);
  uint8_t alarmDate = D2B(date);

  switch(mode)
  {
  case ALARM_MODE_ALL_MATCHED:
    break;
  case ALARM_MODE_HOUR_MIN_SEC_MATCHED:
    alarmDate |= 0x80;
    break;
  case ALARM_MODE_MIN_SEC_MATCHED:
    alarmDate |= 0x80;
    alarmHour |= 0x80;
    break;
  case ALARM_MODE_SEC_MATCHED:
    alarmDate |= 0x80;
    alarmHour |= 0x80;
    alarmMinute |= 0x80;
    break;
  case ALARM_MODE_ONCE_PER_SECOND:
    alarmDate |= 0x80;
    alarmHour |= 0x80;
    alarmMinute |= 0x80;
    alarmSecond |= 0x80;
    break;
  default:
    break;
  }

  /* Write Alarm Registers */
  uint8_t startAddr = DS3231_REG_ALARM1;
  uint8_t buffer[5] = {startAddr, alarmSecond, alarmMinute, alarmHour, alarmDate};
  if(HAL_I2C_Master_Transmit(i2c, DS3231_ADDR, buffer, sizeof(buffer), HAL_MAX_DELAY) != HAL_OK) return false;

  /* Enable Alarm1 at Control Register */
  uint8_t ctrlReg = 0x00;
  DS3231_ReadRegister(DS3231_REG_CONTROL, &ctrlReg);
  ctrlReg |= DS3231_CON_A1IE;
  ctrlReg |= DS3231_CON_INTCN;
  DS3231_WriteRegister(DS3231_REG_CONTROL, ctrlReg);

  return true;
}

bool DS3231_ClearAlarm1(void)
{
  uint8_t ctrlReg;
  uint8_t statusReg;

  /* Clear Control Register */
  DS3231_ReadRegister(DS3231_REG_CONTROL, &ctrlReg);
  ctrlReg &= ~DS3231_CON_A1IE;
  DS3231_WriteRegister(DS3231_REG_CONTROL, ctrlReg);

  /* Clear Status Register */
  DS3231_ReadRegister(DS3231_REG_STATUS, &statusReg);
  statusReg &= ~DS3231_STA_A1F;
  DS3231_WriteRegister(DS3231_REG_STATUS, statusReg);

  return true;
}

bool DS3231_ReadRegister(uint8_t regAddr, uint8_t *value)
{
  if(HAL_I2C_Master_Transmit(i2c, DS3231_ADDR, &regAddr, 1, HAL_MAX_DELAY) != HAL_OK) return false;
  if(HAL_I2C_Master_Receive(i2c, DS3231_ADDR, value, 1, HAL_MAX_DELAY) != HAL_OK) return false;

  return true;
}

bool DS3231_WriteRegister(uint8_t regAddr, uint8_t value)
{
  uint8_t buffer[2] = {regAddr, value};
  if(HAL_I2C_Master_Transmit(i2c, DS3231_ADDR, buffer, sizeof(buffer), HAL_MAX_DELAY) != HAL_OK) return false;

  return true;
}

static uint8_t B2D(uint8_t bcd)
{
  return (bcd >> 4) * 10 + (bcd & 0x0F);
}

static uint8_t D2B(uint8_t decimal)
{
  return (((decimal / 10) << 4) | (decimal % 10));
}
