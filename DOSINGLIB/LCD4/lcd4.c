/*
 * lcd4.h
 *
 *  Created on: 2020. 8. 16.
 *      Author: Cuong Le Hung
 */
#include "../include.h"
//#include "lcd4.h"

void LCD4_Init(void)
{
	char *ac_BuffLcd;
	ac_BuffLcd = (char *)malloc(20 * sizeof(char));
	
	HAL_Delay(50); 	// wait for >40ms
	LCD4_WriteCommand(0,0x30);
	HAL_Delay(5);	// wait for >4.1m
	LCD4_WriteCommand(0,0x30);
	HAL_Delay(1);	// wait for >100us
	LCD4_WriteCommand(0,0x30);
	HAL_Delay(10);
	LCD4_WriteCommand(0,0x20);	// 4bit mode
	HAL_Delay(10);
	
	LCD4_WriteCommand(0,0x28);	// Function set --> DL=0 (4 bit mode), N = 1 (2 line display) F = 0 (5x8 characters)
	LCD4_WriteCommand(0,0x08);	//Display on/off control --> D=0,C=0, B=0  ---> display off
	LCD4_WriteCommand(0,0x01);	// clear display
	HAL_Delay(1);
	LCD4_WriteCommand(0,0x06);	//Entry mode set --> I/D = 1 (increment cursor) & S = 0 (no shift)
	LCD4_WriteCommand(0,0x0c);	//Display on/off control --> D = 1, C and B = 0. (Cursor and blink, last two bits)
	
	sprintf(ac_BuffLcd,"    SMARTFARM EPC  ");
	LCD4_WriteCursor(0,0,(int8_t*)ac_BuffLcd);
	sprintf(ac_BuffLcd,"     CONTROLLER    ");
	LCD4_WriteCursor(1,0,(int8_t*)ac_BuffLcd);
	sprintf(ac_BuffLcd,"   INITIALIZING...");
	LCD4_WriteCursor(3,0,(int8_t*)ac_BuffLcd);
	
	free(ac_BuffLcd);
}
void LCD4_Clear(void)
{
	LCD4_WriteCommand(0, 0x01);
	HAL_Delay(1);
}

bool LCD4_LoadCustomChar(uint8_t cell, uint8_t *charMap)
{
	if (cell > 7)
	{
		return false;
	}

	cell &= 0x07;
//	TM_HD44780_Cmd(HD44780_SETCGRAMADDR | (cell << 3));
	LCD4_WriteCommand(0, HD44780_SETCGRAMADDR | (cell << 3));

//	LCD4_WriteCommand(0, lcdCommand);

    for (uint8_t i = 0; i < 8; i++) {
//    	TM_HD44780_Data(charMap[i]);
    	LCD4_WriteCommand(1, charMap[i]);
    }

	return true;
}

void LCD4_WriteCursor(uint8_t x, uint8_t y, int8_t *string)
{
	//Set Cursor Position
	#ifdef LCD16xN //For LCD16x2 or LCD16x4
	switch(x){
		case 0: //row 0
			lcdWriteCommand(0, 0x80+0x00+y);
			break;
		case 1: //row 1
			lcdWriteCommand(0, 0x80+0x40+y);
			break;
		case 2: //row 2
			lcdWriteCommand(0, 0x80+0x10+y);
			break;
		case 3: //row 3
			lcdWriteCommand(0, 0x80+0x50+y);
			break;
	}
	#endif

	#ifdef LCD20xN //For LCD 20x4
	switch(x){
		case 0: //row 0
			LCD4_WriteCommand(0, 0x80+0x00+y);
			break;
		case 1: //row 1
			LCD4_WriteCommand(0, 0x80+0x40+y);
			break;
		case 2: //row 2
			LCD4_WriteCommand(0, 0x80+0x14+y);
			break;
		case 3: //row 3
			LCD4_WriteCommand(0, 0x80+0x54+y);
			break;
	}
	#endif

	while(*string){
		LCD4_WriteCommand(1, *string);
		string++;
	}
}

void LCD4_WriteCommand(uint8_t type,uint8_t data)
{
//	HAL_Delay(1);
	if(type){
		PIN_HIGH(LCD_RS_GPIO_Port, LCD_RS_Pin);
	}else{
		PIN_LOW(LCD_RS_GPIO_Port, LCD_RS_Pin);
	}

	//Send High Nibble
	if(data&0x80){
		PIN_HIGH(LCD_D7_GPIO_Port, LCD_D7_Pin);
	}else{
		PIN_LOW(LCD_D7_GPIO_Port, LCD_D7_Pin);
	}

	if(data&0x40){
		PIN_HIGH(LCD_D6_GPIO_Port, LCD_D6_Pin);
	}else{
		PIN_LOW(LCD_D6_GPIO_Port, LCD_D6_Pin);
	}

	if(data&0x20){
		PIN_HIGH(LCD_D5_GPIO_Port, LCD_D5_Pin);
	}else{
		PIN_LOW(LCD_D5_GPIO_Port, LCD_D5_Pin);
	}

	if(data&0x10){
		PIN_HIGH(LCD_D4_GPIO_Port, LCD_D4_Pin);
	}else{
		PIN_LOW(LCD_D4_GPIO_Port, LCD_D4_Pin);
	}
	PIN_HIGH(LCD_EN_GPIO_Port, LCD_EN_Pin);
	PIN_LOW(LCD_EN_GPIO_Port, LCD_EN_Pin);
	
	//Send Low Nibble
	if(data&0x08){
		PIN_HIGH(LCD_D7_GPIO_Port, LCD_D7_Pin);
	}else{
		PIN_LOW(LCD_D7_GPIO_Port, LCD_D7_Pin);
	}

	if(data&0x04){
		PIN_HIGH(LCD_D6_GPIO_Port, LCD_D6_Pin);
	}else{
		PIN_LOW(LCD_D6_GPIO_Port, LCD_D6_Pin);
	}

	if(data&0x02){
		PIN_HIGH(LCD_D5_GPIO_Port, LCD_D5_Pin);
	}else{
		PIN_LOW(LCD_D5_GPIO_Port, LCD_D5_Pin);
	}

	if(data&0x01){
		PIN_HIGH(LCD_D4_GPIO_Port, LCD_D4_Pin);
	}else{
		PIN_LOW(LCD_D4_GPIO_Port, LCD_D4_Pin);
	}
	PIN_HIGH(LCD_EN_GPIO_Port, LCD_EN_Pin);
	PIN_LOW(LCD_EN_GPIO_Port, LCD_EN_Pin);

	HAL_Delay(1);
}
