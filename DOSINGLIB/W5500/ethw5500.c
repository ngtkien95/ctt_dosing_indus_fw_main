/*
 * ethw5500.c
 *
 *  Created on: 2020. 8. 31.
 *      Author: 
 */
#include "../include.h"
//#include "ethw5500.h"
#include "string.h"
///* include to import data */
//#include "config.h"

/* extern structure */
extern _SENSOR_IRRI tSensorIrriValue;
extern _SENSOR_CLI tSensorCliValue;
extern _HCSR04 tDistance;
extern _FAM_INFO tFamInfo;

/* extern value */
extern int32_t nTimeOnPumpPeriEc;
extern int32_t nTimeOnPumpPeriPh;

SPI_HandleTypeDef *spi;

/* Export Variable -----------------------------------------------------------*/
_ETHW5500 ethCheck = {
	.bState = false,
	.nIpAddress = "0.0.0.0"};
_W5500_DEV g_w5500 = {
	.netInfo = {
		.mac = {0x00, 0x08, 0xdc, 0xab, 0xcd, 0xef}, // Mac address
		.ip = {192, 168, 0, 123},					 // IP address 169.254.130.1
		.sn = {255, 255, 255, 0},					 // Subnet mask
		.gw = {192, 168, 0, 1}},
	.server_info = {.serverip = {18, 138, 185, 90}, .port = 80},
};
uint8_t bufSize[] = {4,4};
int8_t nTimeOut = 0;
uint8_t socket_numer = 0; //Select socket 0
uint16_t received_len = 0, RSR_len = 0;

uint16_t test_lenght_sys;
uint16_t test_lenght_irr;
uint16_t test_lenght_cli;
char test_msg[1200];

/* Private variables-----------*/
EthState_t EthState;
int16_t nTransmitTimerCount = 0;
int16_t nSysInfoTimerCount = 0;
extern uint8_t nReceiveData[3000];
extern char nPayLoad[2500];

char BEGIN_STR[] = "GET ";
char END_STR[] = " HTTP/1.1\r\nHost: 18.138.185.90\r\n\r\n";
char findPayload[] = "{\"success\":";

int8_t nTimeEthernetCheck = 5;

/* Private function prototypes -----------------------------------------------*/
static bool ETH_Check(void);
static bool ETH_Send(uint8_t *buf);
static bool ETH_Close(void);
static bool ETH_StringSysInfo(void);
static bool ETH_StringClimate(void);
static bool ETH_StringIrrigation(void);
static void w5500_cs_select(void);
static void w5500_cs_deselect(void);
static uint8_t w5500_spi_rb(void);
static void w5500_spi_wb(uint8_t _byte);


void ETH_W5500_Init(SPI_HandleTypeDef *handle)
{
	spi = handle;

	reg_wizchip_cs_cbfunc(w5500_cs_select, w5500_cs_deselect);
	reg_wizchip_spi_cbfunc(w5500_spi_rb, w5500_spi_wb);
	wizchip_init(bufSize, bufSize);
	setSn_TXBUF_SIZE(0,4);
	setSn_RXBUF_SIZE(0,4);
	sprintf((char *)ethCheck.nIpAddress, "%3d.%3d.%3d.%3d", g_w5500.netInfo.ip[0], g_w5500.netInfo.ip[1], g_w5500.netInfo.ip[2], g_w5500.netInfo.ip[3]);

	wizchip_setnetinfo(&g_w5500.netInfo);

	ethCheck.bState = ETH_Check();
	EthState = ETHERNET_IDLE;
}


bool ETH_Check(void)
{
	bool ret = false;

	// Init socket
	socket(socket_numer, Sn_MR_TCP, g_w5500.server_info.port, SF_TCP_NODELAY);

	uint8_t connectionState = SOCK_BUSY;

	//Connect to server
	while (nTimeOut <= 5)
	{
		if (connectionState != SOCK_OK)
		{
			connectionState = connect(socket_numer, g_w5500.server_info.serverip, g_w5500.server_info.port);
			nTimeOut++;
		}
		else
		{
			ret = true;
			goto QUIT_CHECK;
		}
	}
	ret = false;

QUIT_CHECK:

		if (SOCK_OK != connectionState) {
			connectionState = close(socket_numer);
	 }
	return ret;
}

bool ETH_Send(uint8_t *buf)
{
	bool ret = false;
	socket(socket_numer, Sn_MR_TCP, g_w5500.server_info.port, SF_TCP_NODELAY);
	uint8_t connectionState = SOCK_BUSY;
	while (nTimeOut <= 5)
	{
		if (connectionState != SOCK_OK)
		{
			connectionState = connect(socket_numer, g_w5500.server_info.serverip, g_w5500.server_info.port);
			nTimeOut++;
		}
		else
		{
			//Send data
			send(socket_numer, buf, strlen((const char *)buf));
			ret = true;
			goto QUIT_SEND;
		}
	}
	ret = false;

QUIT_SEND:
	return ret;
}

bool ETH_Close(void)
{
	uint8_t connectionState = SOCK_BUSY;

	if (SOCK_OK != connectionState)
	{
		connectionState = close(socket_numer);
	}

	return connectionState;
}

//TODO: Ethernet Exclude
bool ETH_Process(void)
{	
	switch (EthState)
	{

	case ETHERNET_IDLE:
		break;
	case ETHERNET_ERROR:
		EthState = ETHERNET_IDLE;
		break;
	case ETHERNET_CHECK:
		ethCheck.bState = ETH_Check();
		if (ethCheck.bState)
		{
			if (nSysInfoTimerCount == 0)
				EthState = ETHERNET_TRANS_SYSINFO;
			else
				EthState = ETHERNET_TRANS_CLI;
		}
		else
		{
			EthState = ETHERNET_ERROR;
		}
		break;
	case ETHERNET_TRANS_SYSINFO:
		nSysInfoTimerCount = 28800; // 8h
		// nSysInfoTimerCount = 300;

		ETH_StringSysInfo();

		nTransmitTimerCount = 5;
		EthState = ETHERNET_TRANS_CLI;
		break;
	case ETHERNET_TRANS_CLI:
		if (nTransmitTimerCount == 0)
		{
			ETH_StringClimate();

			nTransmitTimerCount = 5;
			EthState = ETHERNET_TRANS_IRRI;
		}
		break;
	case ETHERNET_TRANS_IRRI:
		if (nTransmitTimerCount == 0)
		{
			ETH_StringIrrigation();

			nTimeOut = 5;
			EthState = ETHERNET_TRANS_RECEIVE_CHECK;
		}
		break;
	case ETHERNET_TRANS_RECEIVE_CHECK:
		RSR_len = getSn_RX_RSR(socket_numer);
		if (RSR_len > 0)
		{
			EthState = ETHERNET_CHECK_RECEIVE;
		}
		else if (nTimeOut == 0)
		{
			ETH_Close();
			EthState = ETHERNET_IDLE;
		}
		break;
	case ETHERNET_CHECK_RECEIVE:	
		received_len = recv(socket_numer, nReceiveData, RSR_len);
		memset(nPayLoad, '\0', (size_t)strlen((char *)nPayLoad));
	
		sprintf(nPayLoad,"%s",strstr((const char *)nReceiveData,findPayload));
		if (received_len > 1000)
		{
			process_payload(nPayLoad);
			EthState = ETHERNET_PARAM_SAVE;
		}
		
		else
		{
			EthState = ETHERNET_IDLE;
		}

		memset(nReceiveData, '\0', (size_t)strlen((char *)nReceiveData));
		
		ETH_Close();
		break;
	case ETHERNET_PARAM_SAVE:
		MODE_ParamsServerUpdate();
		MODE_ParamsServerSave();
		MODE_ParamsServerLoad();
		ETH_ResetTimerSysInfo();

		EthState = ETHERNET_IDLE;
		break;
	}

	return true;
}

bool ETH_Timer1000ms(void)
{
	if (nTransmitTimerCount > 0)
		nTransmitTimerCount--;
	if (nSysInfoTimerCount > 0)
		nSysInfoTimerCount--;
	if (nTimeEthernetCheck > 0)
		nTimeEthernetCheck--;
	if (nTimeOut > 0)
		nTimeOut--;

	return true;
}



void ETH_ResetTimerSysInfo(void)
{
	nSysInfoTimerCount = 0;
}

//#define TEST_SYSINFO
bool ETH_StringSysInfo(void)
{
	char *msg;
	msg = (char *)malloc(1000 * sizeof(char));
	
	if (msg == NULL)
	{
		return false;
	}
	
	char data[100];

	sprintf(msg, "%s", BEGIN_STR);

	sprintf((char *)data, "/sysinfoIN?FarmCode=%s", tFamInfo.cFarmCode);
	strcat(msg, data);
	sprintf((char *)data, "&T_Update=%04d", tFamInfo.nTimeUpload);
	strcat(msg, data);
	sprintf((char *)data, "&Thre_EC=%04d", tFamInfo.nThrehold_Ec);
	strcat(msg, data);
	sprintf((char *)data, "&ECLo=%04d", tFamInfo.nThrehold_EcLow);
	strcat(msg, data);
	sprintf((char *)data, "&ECHi=%04d", tFamInfo.nThrehold_ECHigh);
	strcat(msg, data);
	sprintf((char *)data, "&ECDual=%04d", tFamInfo.nThrehold_ECDual);
	strcat(msg, data);
	sprintf((char *)data, "&Thre_WT=%04d", tFamInfo.nThrehold_WT);
	strcat(msg, data);
	sprintf((char *)data, "&WTLo=%04d", tFamInfo.nThrehold_WTLow);
	strcat(msg, data);
	sprintf((char *)data, "&WTHi=%04d", tFamInfo.nThrehold_WTHigh);
	strcat(msg, data);
	sprintf((char *)data, "&WTDual=%04d", tFamInfo.nThrehold_WTDual);
	strcat(msg, data);
	sprintf((char *)data, "&Thre_PH=%04d", tFamInfo.nThrehold_PH);
	strcat(msg, data);
	sprintf((char *)data, "&PHLo=%04d", tFamInfo.nThrehold_PHLow);
	strcat(msg, data);
	sprintf((char *)data, "&PHHi=%04d", tFamInfo.nThrehold_PHHigh);
	strcat(msg, data);
	sprintf((char *)data, "&PHDual=%04d", tFamInfo.nThrehold_PHDual);
	strcat(msg, data);
	sprintf((char *)data, "&Thre_Temp=%04d", tFamInfo.nThrehold_Cli_Temp);
	strcat(msg, data);
	sprintf((char *)data, "&TempLo=%04d", tFamInfo.nThrehold_Cli_TempLow);
	strcat(msg, data);
	sprintf((char *)data, "&TempHi=%04d", tFamInfo.nThrehold_Cli_TempHigh);
	strcat(msg, data);
	sprintf((char *)data, "&TempDual=%04d", tFamInfo.nThrehold_Cli_TempDual);
	strcat(msg, data);
	sprintf((char *)data, "&Thre_Humi=%04d", tFamInfo.nThrehold_Cli_Humi);
	strcat(msg, data);
	sprintf((char *)data, "&HumiLo=%04d", tFamInfo.nThrehold_Cli_HumiLow);
	strcat(msg, data);
	sprintf((char *)data, "&HumiHi=%04d", tFamInfo.nThrehold_Cli_HumiHigh);
	strcat(msg, data);
	sprintf((char *)data, "&HumiDual=%04d", tFamInfo.nThrehold_Cli_HumiDual);
	strcat(msg, data);
	sprintf((char *)data, "&Thre_Light=%05d", tFamInfo.nThrehold_Cli_Light);
	strcat(msg, data);
	sprintf((char *)data, "&LightLo=%05d", tFamInfo.nThrehold_Cli_LightLow);
	strcat(msg, data);
	sprintf((char *)data, "&LightHi=%05d", tFamInfo.nThrehold_Cli_LightHigh);
	strcat(msg, data);
	sprintf((char *)data, "&LightDual=%05d", tFamInfo.nThrehold_Cli_LightDual);
	strcat(msg, data);
	sprintf((char *)data, "&Thre_Flow=%04d", tFamInfo.nThrehold_FlowRate);
	strcat(msg, data);
	sprintf((char *)data, "&FlowLo=%04d", tFamInfo.nThrehold_FlowRateLow);
	strcat(msg, data);
	sprintf((char *)data, "&FlowHi=%04d", tFamInfo.nThrehold_FlowRateHigh);
	strcat(msg, data);
	sprintf((char *)data, "&FlowDual=%04d", tFamInfo.nThrehold_FlowRateDual);
	strcat(msg, data);
	sprintf((char *)data, "&TankLv=%04d", tFamInfo.nThrehold_TankLevel);
	strcat(msg, data);
	sprintf((char *)data, "&TankLvLo=%04d", tFamInfo.nThrehold_TankLevelLow);
	strcat(msg, data);
	sprintf((char *)data, "&TankLvHi=%04d", tFamInfo.nThrehold_TankLevelHigh);
	strcat(msg, data);
	sprintf((char *)data, "&TankLvDual=%04d", tFamInfo.nThrehold_TankLevelDual);
	strcat(msg, data);
	sprintf((char *)data, "&T_Peri=%04d", tFamInfo.nPump_TimePeri);
	strcat(msg, data);
	sprintf((char *)data, "&TNozOn=%04d", tFamInfo.nPump_TimeNozzleOn);
	strcat(msg, data);
	sprintf((char *)data, "&TNozOff=%04d", tFamInfo.nPump_TimeNozzleOff);
	strcat(msg, data);
	sprintf((char *)data, "&TPumpMix=%04d", tFamInfo.nPump_TimePumpMix);
	strcat(msg, data);
	sprintf((char *)data, "&EC1A=%04d", tFamInfo.nCalib_Ec1A);
	strcat(msg, data);
	sprintf((char *)data, "&EC1B=%04d", tFamInfo.nCalib_Ec1B);
	strcat(msg, data);
	sprintf((char *)data, "&EC2A=%04d", tFamInfo.nCalib_Ec2A);
	strcat(msg, data);
	sprintf((char *)data, "&EC2B=%04d", tFamInfo.nCalib_Ec2B);
	strcat(msg, data);
	sprintf((char *)data, "&WT1A=%04d", tFamInfo.nCalib_Wt1A);
	strcat(msg, data);
	sprintf((char *)data, "&WT1B=%04d", tFamInfo.nCalib_Wt1B);
	strcat(msg, data);
	sprintf((char *)data, "&WT2A=%04d", tFamInfo.nCalib_Wt2A);
	strcat(msg, data);
	sprintf((char *)data, "&WT2B=%04d", tFamInfo.nCalib_Wt2B);
	strcat(msg, data);
	sprintf((char *)data, "&PH1A=%04d", tFamInfo.nCalib_Ph1A);
	strcat(msg, data);
	sprintf((char *)data, "&PH1B=%04d", tFamInfo.nCalib_Ph1B);
	strcat(msg, data);
	sprintf((char *)data, "&PH2A=%04d", tFamInfo.nCalib_Ph2A);
	strcat(msg, data);
	sprintf((char *)data, "&PH2B=%04d", tFamInfo.nCalib_Ph2B);
	strcat(msg, data);
	sprintf((char *)data, "&O_TE1=%04d", tFamInfo.nOffset_Cli_Temp1);
	strcat(msg, data);
	sprintf((char *)data, "&O_TE2=%04d", tFamInfo.nOffset_Cli_Temp2);
	strcat(msg, data);
	sprintf((char *)data, "&O_HU1=%04d", tFamInfo.nOffset_Cli_Humi1);
	strcat(msg, data);
	sprintf((char *)data, "&O_HU2=%04d", tFamInfo.nOffset_Cli_Humi2);
	strcat(msg, data);
	sprintf((char *)data, "&O_LI1=%04d", tFamInfo.nOffset_Cli_Light1);
	strcat(msg, data);
	sprintf((char *)data, "&O_LI2=%04d", tFamInfo.nOffset_Cli_Light2);
	strcat(msg, data);
	sprintf((char *)data, "&O_US1=%04d", tFamInfo.nOffset_US1);
	strcat(msg, data);
	sprintf((char *)data, "&O_US2=%04d", tFamInfo.nOffset_US2);
	strcat(msg, data);
	sprintf((char *)data, "&O_US3=%04d", tFamInfo.nOffset_US3);
	strcat(msg, data);
	sprintf((char *)data, "&O_US4=%04d", tFamInfo.nOffset_US4);
	strcat(msg, data);
	sprintf((char *)data, "&O_US5=%04d", tFamInfo.nOffset_US5);
	strcat(msg, data);
	sprintf((char *)data, "&O_US6=%04d", tFamInfo.nOffset_US6);
	strcat(msg, data);
	sprintf((char *)data, "&VolN_PH1=%04d", tFamInfo.nVoltage_Ph1_Newtral);
	strcat(msg, data);
	sprintf((char *)data, "&VolN_PH2=%04d", tFamInfo.nVoltage_Ph2_Newtral);
	strcat(msg, data);
	sprintf((char *)data, "&VolA_PH1=%04d", tFamInfo.nVoltage_Ph1_Acid);
	strcat(msg, data);
	sprintf((char *)data, "&VolA_PH2=%04d", tFamInfo.nVoltage_Ph2_Acid);
	strcat(msg, data);
	sprintf((char *)data, "&TNozOn=%04d", tFamInfo.nTime_NozzleOn);
	strcat(msg, data);
	sprintf((char *)data, "&TNozOff=%04d", tFamInfo.nTime_NozzleOff);
	strcat(msg, data);
	sprintf((char *)data, "&TCooOn=%04d", tFamInfo.nTime_CoopadOn);
	strcat(msg, data);
	sprintf((char *)data, "&TCooOff=%04d", tFamInfo.nTime_CoopadOff);
	strcat(msg, data);
	sprintf((char *)data, "&TChiOn=%04d", tFamInfo.nTime_ChillerOn);
	strcat(msg, data);
	sprintf((char *)data, "&TChiOff=%04d", tFamInfo.nTime_ChillerOff);
	strcat(msg, data);
	sprintf((char *)data, "&TNetOn=%04d", tFamInfo.nTime_ShadingNetOn);
	strcat(msg, data);
	sprintf((char *)data, "&TNetOff=%04d", tFamInfo.nTime_ShadingNetOff);
	strcat(msg, data);
	sprintf((char *)data, "&FWVer=%s", tFamInfo.cFwVersion);
	strcat(msg, data);

	strcat(msg, END_STR);

	//test
	
//	sprintf(test_msg,"%s","GET /sysinfoIN?FarmCode=SGN012111IN000222&T_Update=1000&Thre_EC=0160&ECLo=0140&ECHi=0180&ECDual=0020&Thre_WT=0300&WTLo=0260&WTHi=0350&WTDual=0020&Thre_PH=0650&PHLo=0600&PHHi=0700&PHDual=0020&Thre_Temp=0300&TempLo=0300&TempHi=0300&TempDual=0300&Thre_Humi=0300&HumiLo=0300&HumiHi=0300&HumiDual=0300&Thre_Light=12000&LightLo=12000&LightHi=20000&LightDual=12000&Thre_Flow=1200&FlowLo=1200&FlowHi=1200&FlowDual=1200&TankLv=1200&TankLvLo=1200&TankLvHi=1200&TankLvDual=1200&T_Peri=1200&TNozOn=1200&TNozOff=1200&TPumpMix=1200&EC1A=1200&EC1B=1200&EC2A=1200&EC2B=1200&WT1A=1200&WT1B=1200&WT2A=1200&WT2B=1200&PH1A=1200&PH1B=1200&PH2A=1200&PH2B=1200&FWVer=IN01A&O_TE1=1000&O_TE2=1000&O_HU1=1000&O_HU2=1000&O_LI1=1000&O_LI2=1000&O_US1=1000&O_US2=1000&O_US3=1000&O_US4=1000&O_US5=1000&O_US6=1000&VolN_PH1=1000&VolN_PH2=1000&VolA_PH1=1000&VolA_PH2=1000&TNozOn=1000&TNozOff=1000&TCooOn=1000&TCooOff=1000&TChiOn=1000&TChiOff=1000&TNetOn=1000&TNetOff=1000 HTTP/1.1\r\nHost: 18.138.185.90\r\n\r\n");
	
	test_lenght_sys = strlen(msg);
	
	ETH_Send((uint8_t *)msg);
	ETH_Close();
	free(msg);
	
	return true;
}
bool ETH_StringClimate(void)
{
	char *msg;
	msg = (char *)malloc(300 * sizeof(char));
	
	if (msg == NULL)
	{
		return false;
	}
	
	char data[100];

	sprintf(msg, "%s", BEGIN_STR);

	sprintf((char *)data, "/cliCollect?farmCode=%s", tFamInfo.cFarmCode);
	strcat(msg, data);

	switch (tSensorCliValue.tTempCheck)
	{
	case CHECK_TEMP_OK:
		sprintf(data, "&cliTemp1=%2.2f&cliTemp2=%2.2f&cliTempe=%s", tSensorCliValue.fCliTemp1, tSensorCliValue.fCliTemp2, "OK");
		break;
	case CHECK_TEMP_DUAL:
		sprintf(data, "&cliTemp1=%2.2f&cliTemp2=%2.2f&cliTempe=%s", tSensorCliValue.fCliTemp1, tSensorCliValue.fCliTemp2, "DUAL");
		break;
	case CHECK_TEMP_HIGH:
		sprintf(data, "&cliTemp1=%2.2f&cliTemp2=%2.2f&cliTempe=%s", tSensorCliValue.fCliTemp1, tSensorCliValue.fCliTemp2, "HIGH");
		break;
	case CHECK_TEMP_LOW:
		sprintf(data, "&cliTemp1=%2.2f&cliTemp2=%2.2f&cliTempe=%s", tSensorCliValue.fCliTemp1, tSensorCliValue.fCliTemp2, "LOW");
		break;
	}
	strcat(msg, data);

	switch (tSensorCliValue.tHumiCheck)
	{
	case CHECK_HUMI_OK:
		sprintf(data, "&cliHumi1=%2.2f&cliHumi2=%2.2f&cliHumie=%s", tSensorCliValue.fCliHumi1, tSensorCliValue.fCliHumi2, "OK");
		break;
	case CHECK_HUMI_DUAL:
		sprintf(data, "&cliHumi1=%2.2f&cliHumi2=%2.2f&cliHumie=%s", tSensorCliValue.fCliHumi1, tSensorCliValue.fCliHumi2, "DUAL");
		break;
	case CHECK_HUMI_HIGH:
		sprintf(data, "&cliHumi1=%2.2f&cliHumi2=%2.2f&cliHumie=%s", tSensorCliValue.fCliHumi1, tSensorCliValue.fCliHumi2, "HIGH");
		break;
	case CHECK_HUMI_LOW:
		sprintf(data, "&cliHumi1=%2.2f&cliHumi2=%2.2f&cliHumie=%s", tSensorCliValue.fCliHumi1, tSensorCliValue.fCliHumi2, "LOW");
		break;
	}
	strcat(msg, data);

	switch (tSensorCliValue.tLightCheck)
	{
	case CHECK_LIGHT_OK:
		sprintf(data, "&cliLight1=%d&cliLight2=%d&cliLighte=%s", tSensorCliValue.nCliLight1, tSensorCliValue.nCliLight2, "OK");
		break;
	case CHECK_LIGHT_DUAL:
		sprintf(data, "&cliLight1=%d&cliLight2=%d&cliLighte=%s", tSensorCliValue.nCliLight1, tSensorCliValue.nCliLight2, "DUAL");
		break;
	case CHECK_LIGHT_HIGH:
		sprintf(data, "&cliLight1=%d&cliLight2=%d&cliLighte=%s", tSensorCliValue.nCliLight1, tSensorCliValue.nCliLight2, "HIGH");
		break;
	case CHECK_LIGHT_LOW:
		sprintf(data, "&cliLight1=%d&cliLight2=%d&cliLighte=%s", tSensorCliValue.nCliLight1, tSensorCliValue.nCliLight2, "LOW");
		break;
	}
	strcat(msg, data);

	sprintf(data, "&cliSttFan=%s", "UNKNOW");
	strcat(msg, data);

	sprintf(data, "&cliSttCool=%s", "UNKNOW");
	strcat(msg, data);

	sprintf(data, "&cliSttNozzle=%s", "UNKNOW");
	strcat(msg, data);

	sprintf(data, "&cliSttShading=%s", "UNKNOW");
	strcat(msg, data);

	sprintf(data, "&firmVersion=%s", tFamInfo.cFwVersion);
	strcat(msg, data);

	strcat(msg, END_STR);

	// 18.138.185.90/cliCollect?farmCode=SGN022004IN000789&cliTemp1=33.45&cliTemp2=34.33&cliTempe=OK&cliHumi1=50.45&cliHumi2=50.66&cliHumie=LOW&cliLight1=12000&cliLight2=13000&cliLighte=HIGH&cliSttFan=UNKNOW&cliSttCool=UNKNOW&cliSttNozzle=UNKNOW&cliSttShading=UNKNOW&firmVersion=IN01A
	
	
	test_lenght_cli = strlen(msg);
	
	ETH_Send((uint8_t *)msg);
	ETH_Close();
	free(msg);
	
	return true;
}

//#define TEST_IRR

bool ETH_StringIrrigation(void)
{
	char *msg;
	msg = (char *)malloc(470 * sizeof(char));
	
	if (msg == NULL)
	{
		return false;
	}

	char data[100];

	sprintf(msg, "%s", BEGIN_STR);

	sprintf(data, "/irrCollect2?farmCode=%s", tFamInfo.cFarmCode);
	strcat(msg, data);

	switch (tSensorIrriValue.tPhCheck)
	{
	case CHECK_PH_OK:
		sprintf(data, "&irrPH1=%1.2f&irrPH2=%1.2f&irrPHe=%s", tSensorIrriValue.dPh1, tSensorIrriValue.dPh2, "OK");
		break;
	case CHECK_PH_HIGH:
		sprintf(data, "&irrPH1=%1.2f&irrPH2=%1.2f&irrPHe=%s", tSensorIrriValue.dPh1, tSensorIrriValue.dPh2, "HIGH");
		break;
	case CHECK_PH_LOW:
		sprintf(data, "&irrPH1=%1.2f&irrPH2=%1.2f&irrPHe=%s", tSensorIrriValue.dPh1, tSensorIrriValue.dPh2, "LOW");
		break;
	case CHECK_PH_DUAL:
		sprintf(data, "&irrPH1=%1.2f&irrPH2=%1.2f&irrPHe=%s", tSensorIrriValue.dPh1, tSensorIrriValue.dPh2, "DUAL");
		break;
	}
	strcat(msg, data);

	switch (tSensorIrriValue.tEcCheck)
	{
	case CHECK_EC_OK:
		sprintf(data, "&irrEC1=%1.2f&irrEC2=%1.2f&irrECe=%s", tSensorIrriValue.dEc1, tSensorIrriValue.dEc2, "OK");
		break;
	case CHECK_EC_HIGH:
		sprintf(data, "&irrEC1=%1.2f&irrEC2=%1.2f&irrECe=%s", tSensorIrriValue.dEc1, tSensorIrriValue.dEc2, "HIGH");
		break;
	case CHECK_EC_LOW:
		sprintf(data, "&irrEC1=%1.2f&irrEC2=%1.2f&irrECe=%s", tSensorIrriValue.dEc1, tSensorIrriValue.dEc2, "LOW");
		break;
	case CHECK_EC_DUAL:
		sprintf(data, "&irrEC1=%1.2f&irrEC2=%1.2f&irrECe=%s", tSensorIrriValue.dEc1, tSensorIrriValue.dEc2, "DUAL");
		break;
	}
	strcat(msg, data);

	switch (tSensorIrriValue.tWtCheck)
	{
	case CHECK_WT_OK:
		sprintf(data, "&irrWaterTemp1=%2.2f&irrWaterTemp2=%2.2f&irrWaterTempe=%s", tSensorIrriValue.dWaterTemp1, tSensorIrriValue.dWaterTemp2, "OK");
		break;
	case CHECK_WT_HIGH:
		sprintf(data, "&irrWaterTemp1=%2.2f&irrWaterTemp2=%2.2f&irrWaterTempe=%s", tSensorIrriValue.dWaterTemp1, tSensorIrriValue.dWaterTemp2, "HIGH");
		break;
	case CHECK_WT_LOW:
		sprintf(data, "&irrWaterTemp1=%2.2f&irrWaterTemp2=%2.2f&irrWaterTempe=%s", tSensorIrriValue.dWaterTemp1, tSensorIrriValue.dWaterTemp2, "LOW");
		break;
	case CHECK_WT_DUAL:
		sprintf(data, "&irrWaterTemp1=%2.2f&irrWaterTemp2=%2.2f&irrWaterTempe=%s", tSensorIrriValue.dWaterTemp1, tSensorIrriValue.dWaterTemp2, "DUAL");
		break;
	}
	strcat(msg, data);

	switch (tDistance.tTank1Check)
	{
	case CHECK_TANK1_HIGH:
		sprintf(data, "&irrF1A=%03d&irrF1B=%03d&irrF1E=%s", tDistance.dLitUs1, tDistance.dLitUs2, "HIGH");
		break;
	case CHECK_TANK1_LOW:
		sprintf(data, "&irrF1A=%03d&irrF1B=%03d&irrF1E=%s", tDistance.dLitUs1, tDistance.dLitUs2, "LOW");
		break;
	case CHECK_TANK1_DUAL:
		sprintf(data, "&irrF1A=%03d&irrF1B=%03d&irrF1E=%s", tDistance.dLitUs1, tDistance.dLitUs2, "DUAL");
		break;
	case CHECK_TANK1_OK:
		sprintf(data, "&irrF1A=%03d&irrF1B=%03d&irrF1E=%s", tDistance.dLitUs1, tDistance.dLitUs2, "OK");
		break;
	}
	//sprintf(data,"&irrF1A=80&irrF1B=81&irrF2E=OK");
	strcat(msg, data);

	switch (tDistance.tTank2Check)
	{
	case CHECK_TANK2_HIGH:
		sprintf(data, "&irrF2A=%03d&irrF2B=%03d&irrF2E=%s", tDistance.dLitUs3, tDistance.dLitUs4, "HIGH");
		break;
	case CHECK_TANK2_LOW:
		sprintf(data, "&irrF2A=%03d&irrF2B=%03d&irrF2E=%s", tDistance.dLitUs3, tDistance.dLitUs4, "LOW");
		break;
	case CHECK_TANK2_DUAL:
		sprintf(data, "&irrF2A=%03d&irrF2B=%03d&irrF2E=%s", tDistance.dLitUs3, tDistance.dLitUs4, "DUAL");
		break;
	case CHECK_TANK2_OK:
		sprintf(data, "&irrF2A=%03d&irrF2B=%03d&irrF2E=%s", tDistance.dLitUs3, tDistance.dLitUs4, "OK");
		break;
	}
	//sprintf(data,"&irrF2A=137&irrF2B=137&irrF2E=HIGH");
	strcat(msg, data);

	switch (tDistance.tTank3Check)
	{
	case CHECK_TANK3_HIGH:
		sprintf(data, "&irrF3A=%03d&irrF3B=%03d&irrF3E=%s", tDistance.dLitUs5, tDistance.dLitUs6, "HIGH");
		break;
	case CHECK_TANK3_LOW:
		sprintf(data, "&irrF3A=%03d&irrF3B=%03d&irrF3E=%s", tDistance.dLitUs5, tDistance.dLitUs6, "LOW");
		break;
	case CHECK_TANK3_DUAL:
		sprintf(data, "&irrF3A=%03d&irrF3B=%03d&irrF3E=%s", tDistance.dLitUs5, tDistance.dLitUs6, "DUAL");
		break;
	case CHECK_TANK3_OK:
		sprintf(data, "&irrF3A=%03d&irrF3B=%03d&irrF3E=%s", tDistance.dLitUs5, tDistance.dLitUs6, "OK");
		break;
	}
	//sprintf(data,"&irrF3A=137&irrF3B=137&irrF2E=HIGH");
	strcat(msg, data);

	sprintf(data, "&irrFlow1=%d&irrFlow2=%d&irrFlowe=%s", 0, 0, "UNKNOW");
	strcat(msg, data);
	
	if (nTimeOnPumpPeriPh != 0)
	{
		sprintf(data, "&irrSttPumpPh=%s", "OPEN");
	}
	else
	{
		sprintf(data, "&irrSttPumpPh=%s", "CLOSE");
	}
	strcat(msg, data);

	if (nTimeOnPumpPeriEc != 0)
	{
		sprintf(data, "&irrSttPumpEc=%s", "OPEN");
	}
	else
	{
		sprintf(data, "&irrSttPumpEc=%s", "CLOSE");
	}
	strcat(msg, data);

	sprintf(data, "&irrSttNutriPump=%s", "UNKNOW");
	strcat(msg, data);

	sprintf(data, "&irrSttChillerPump=%s", "UNKNOW");
	strcat(msg, data);

	sprintf(data, "&irStSol=%s", "00");
	strcat(msg, data);

	sprintf(data, "&firmVersion=%s", tFamInfo.cFwVersion);
	strcat(msg, data);

	//sprintf((char *)nMsg, "GET /irrCollect2?farmCode=SGN012111IN000222&irrPH1=6.86&irrPH2=8.87&irrPHe=OK&irrEC1=1.61&irrEC2=1.66&irrECe=OK&irrWaterTemp1=28.66&irrWaterTemp2=28.98&irrWaterTempe=OK&irrFlow1=3120&irrFlow2=2140&irrFlowe=OK&irrSttPumpPh=OPEN&irrSttPumpEc=CLOSE&irrSttNutriPump=OPEN&irrSttChillerPump=OPEN&irStSol=00&firmVersion=TEST HTTP/1.1\r\nHost: 18.138.185.90\r\n\r\n");

	strcat(msg, END_STR);
//#else
//	sprintf((char *)nMsg, "GET /irrCollect2?farmCode=SGN022004IN000789&irrPH1=6.82&irrPH2=6.83&irrPHe=OK&irrEC1=1.65&irrEC2=1.66&irrECe=OK&irrWaterTemp1=29.33&irrWaterTemp2=29.35&irrWaterTempe=OK&irrF1A=80&irrF1B=81&irrF1E=OK&irrF2A=137&irrF2B=137&irrF2E=HIGH&irrF3A=137&irrF3B=137&irrF3E=HIGH&irrFlow1=0&irrFlow2=0&irrFlowe=LOW&irrSttPumpPh=UNKNOW&irrSttPumpEc=CLOSE&irrSttNutriPump=OPEN&irrSttChillerPump=UNKNOW&irStSol=00&firmVersion=IN01A HTTP/1.1\r\nHost: 18.138.185.90\r\n\r\n");
//#endif

//	sprintf((char *)nMsg, "GET /irrCollect2?farmCode=SGN012111IN000222&irrPH1=6.86&irrPH2=8.87&irrPHe=OK&irrEC1=1.61&irrEC2=1.66&irrECe=OK&irrWaterTemp1=28.66&irrWaterTemp2=28.98&irrWaterTempe=OK&irrFlow1=3120&irrFlow2=2140&irrFlowe=OK&irrSttPumpPh=OPEN&irrSttPumpEc=CLOSE&irrSttNutriPump=OPEN&irrSttChillerPump=OPEN&irStSol=00&firmVersion=TEST");


	//18.138.185.90/irrCollect2?farmCode=SGN022004IN000789&irrPH1=6.82&irrPH2=6.83&irrPHe=OK&irrEC1=1.65&irrEC2=1.66&irrECe=OK&irrWaterTemp1=29.33&irrWaterTemp2=29.35&irrWaterTempe=OK&irrF1A=80&irrF1B=81&irrF1E=OK&irrF2A=137&irrF2B=137&irrF2E=HIGH&irrF3A=137&irrF3B=137&irrF3E=HIGH&irrFlow1=00.00&irrFlow2=00.00&irrFlowe=LOW&irrSttPumpPh=UNKNOW&irrSttPumpEc=CLOSE&irrSttNutriPump=OPEN&irrSttChillerPump=UNKNOW&irStSol=00&firmVersion=IN01A

	
	
	test_lenght_irr = strlen(msg);
	sprintf(test_msg,"%s",msg);
	
	ETH_Send((uint8_t *)msg);
	free(msg);

	return true;
}
void w5500_cs_select(void)
{
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET); //CS LOW
}
void w5500_cs_deselect(void)
{
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET); //CS HIGH
}
uint8_t w5500_spi_rb(void)
{
	uint8_t rbuf;
	//	HAL_SPI_Receive(spi, &rbuf, 1, 0xFFFFFFFF);
	if (HAL_SPI_Receive(spi, &rbuf, 1, 1000) != HAL_OK)
	{
		EthState = ETHERNET_ERROR;
	}
	return rbuf;
}

void w5500_spi_wb(uint8_t _byte)
{
	//	HAL_SPI_Transmit(spi, &_byte, 1, 0xFFFFFFFF);
	if (HAL_SPI_Transmit(spi, &_byte, 1, 1000) != HAL_OK)
	{
		EthState = ETHERNET_ERROR;
	}
}
