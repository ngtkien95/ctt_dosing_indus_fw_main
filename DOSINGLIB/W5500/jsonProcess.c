/*
 * jsonProcess.c
 *
 *  Created on: Dec 26, 2020
 *      Author: Zeder
 */

#include "../include.h"

#include "jsmn.h"
#define JSMN_HEADER



Value_t m_value[100] = {
		{"farmCode",""},
		{"farmName",""},
		{"firmVersion",""},
		{"dateTime",""},
		{"timeUpload",""},
		{"Threhold_EC",""},
		{"Threhold_ECLow",""},
		{"Threhold_ECHigh",""},
		{"Threhold_ECDual",""},
		{"Threhold_WaterTemp",""},
		{"Threhold_WaterTempLow",""},
		{"Threhold_WaterTempHigh",""},
		{"Threhold_WaterTempDual",""},
		{"Threhold_PH",""},
		{"Threhold_PHLow",""},
		{"Threhold_PHHigh",""},
		{"Threhold_PHDual",""},
		{"Threhold_ClimateTemp",""},
		{"Threhold_ClimateTempLow",""},
		{"Threhold_ClimateTempHigh",""},
		{"Threhold_ClimateTempDual",""},
		{"Threhold_ClimateHumidity",""},
		{"Threhold_ClimateHumidityLow",""},
		{"Threhold_ClimateHumidityHigh",""},
		{"Threhold_ClimateHumidityDual",""},
		{"Threhold_ClimateLight",""},
		{"Threhold_ClimateLightLow",""},
		{"Threhold_ClimateLightHigh",""},
		{"Threhold_ClimateLightDual",""},
		{"Threhold_FlowRate",""},
		{"Threhold_FlowRateLow",""},
		{"Threhold_FlowRateHigh",""},
		{"Threhold_FlowRateDual",""},
		{"Threhold_TankLevel",""},
		{"Threhold_TankLevelLow",""},
		{"Threhold_TankLevelHigh",""},
		{"Threhold_TankLevelDual",""},
		{"Pump_TimePeri",""},
		{"Pump_TimeNozzleOn",""},
		{"Pump_TimeNozzleOff",""},
		{"Pump_TimePumpMix",""},
		{"Calib_EC1A",""},
		{"Calib_EC1B",""},
		{"Calib_EC2A",""},
		{"Calib_EC2B",""},
		{"Calib_WT1A",""},
		{"Calib_WT1B",""},
		{"Calib_WT2A",""},
		{"Calib_WT2B",""},
		{"Calib_PH1A",""},
		{"Calib_PH1B",""},
		{"Calib_PH2A",""},
		{"Calib_PH2B",""},
		{"OFFSET_CLI_TEMP1",""},
		{"OFFSET_CLI_TEMP2",""},
		{"OFFSET_CLI_HUMI1",""},
		{"OFFSET_CLI_HUMI2",""},
		{"OFFSET_CLI_LIGHT1",""},
		{"OFFSET_CLI_LIGHT2",""},
		{"OFFSET_US_1",""},
		{"OFFSET_US_2",""},
		{"OFFSET_US_3",""},
		{"OFFSET_US_4",""},
		{"OFFSET_US_5",""},
		{"OFFSET_US_6",""},
		{"Neutral_Voltage_Ph1",""},
		{"Neutral_Voltage_Ph2",""},
		{"Acid_Voltage_Ph1",""},
		{"Acid_Voltage_Ph2",""},
		{"Nozzle_Time_On",""},
		{"Nozzle_Time_Off",""},
		{"Cooling_Pad_Time_On",""},
		{"Cooling_Pad_Time_Off",""},
		{"Chiller_Time_On",""},
		{"Chiller_Time_Off",""},
		{"Shading_Net_Time_On",""},
		{"Shading_Net_Time_Off",""},
		{"statusUpdate","0"}
};

//
//
//Examles
static const char *JsonResult = "{\"success\":true,\"data\":{\"Threhold_ECLow\":100,\"Threhold_ECHigh\":100,\"Threhold_ECDual\":100}}";

static int jsoneq(const char *json, jsmntok_t *tok, const char *s)
{
	if (tok->type == JSMN_STRING && (int)strlen(s) == tok->end - tok->start && strncmp(json + tok->start, s, tok->end - tok->start) == 0)
	{
		return 0;
	}
	return -1;
}

bool process_payload(char* m_payLoad)
{
	int i;
	int r;
	
	jsmn_parser jsParser;
	jsmntok_t t[200]; /* We expect no more than 150 tokens */
	jsmn_init(&jsParser); 
	
	r = jsmn_parse(&jsParser, (const char*)m_payLoad, strlen(m_payLoad), t, sizeof(t) / sizeof(t[0]));
	
	if (r < 0)
	{
		//		  printMsg("Failed to parse JSON: %d\n", r);
		return false;
	}

	/* Assume the top-level element is an object */
	if (r < 1 || t[0].type != JSMN_OBJECT)
	{
		//		  printMsg("Object expected\n");
		return false;
	}
	for (i = 1; i < r; i++)
	{
		for (int j = 0; j < sizeof(m_value) / sizeof(m_value[0]); j++)
		{
			if (jsoneq(m_payLoad, &t[i], m_value[j].name) == 0)
			{
				//sprintf((char *)m_value[j].value, "%.*s\n", t[i + 1].end - t[i + 1].start, m_payLoad + t[i + 1].start);
				sprintf((char *)m_value[j].value, "%.*s", t[i + 1].end - t[i + 1].start, m_payLoad + t[i + 1].start);
			}
		}
		i++;
	}
	
	return true;
}
