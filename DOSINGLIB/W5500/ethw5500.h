/*
 * ethw5500.h
 *
 *  Created on: 2020. 8. 31.
 *      Author: 
 */
#ifndef __ETHW5500_H
#define __ETHW5500_H

#include "stm32f4xx_hal.h"
#include <stdlib.h>
#include <stdbool.h>
//#include "w5500.h"
//#include "wizchip_conf.h"
//#include "socket.h"


typedef enum{
	ETHERNET_IDLE = 10,
	ETHERNET_ERROR,
	ETHERNET_CHECK,
	ETHERNET_TRANS_SYSINFO,
	ETHERNET_TRANS_CLI,
	ETHERNET_TRANS_IRRI,
	ETHERNET_TRANS_RECEIVE_CHECK,
	ETHERNET_CHECK_RECEIVE,
	ETHERNET_PARAM_SAVE
} EthState_t;

typedef struct ServerInformation{
	uint8_t serverip[4];
	uint8_t port;
} _SERVER_INFO;

typedef struct
{
	uint8_t nIpAddress[15];
	bool bState;
} _ETHW5500;

typedef struct W5500X
{
	wiz_NetInfo 	netInfo;
	_SERVER_INFO	server_info;
} _W5500_DEV;


//-----Function prototype
void ETH_W5500_Init(SPI_HandleTypeDef *handle);
bool ETH_Process(void);
void ETH_ResetTimerSysInfo(void);
bool ETH_Timer1000ms(void);

#endif
