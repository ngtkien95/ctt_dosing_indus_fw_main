/*
 * jsonProcess.h
 *
 *  Created on: Dec 26, 2020
 *      Author: Zeder
 */

#ifndef JSONPROCESS_JSONPROCESS_H_
#define JSONPROCESS_JSONPROCESS_H_

#include "../include.h"


typedef struct {
	const char* name;
	char value[100];
}Value_t;

bool process_payload(char* m_payLoad);

#endif /* JSONPROCESS_JSONPROCESS_H_ */
