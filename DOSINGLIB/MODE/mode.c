/*
 * mode.c
 *
 *  Created on: 2020. 8. 16.
 *      Author: Cuong Le Hung
 */
#include "include.h"
#include "FLASH/flash/FlashConfig.h"
#define TIMERDEBOUNCEMAX 100 //Chong rung phim
#define TIMERCHECKLONGCLICK 1000
#define TIMERHANDLELONGCLICK 50

extern _DS3231 tRtc;
extern _HCSR04 tDistance;
//extern _ETHW5500 ethCheck;
extern _SENSOR_IRRI tSensorIrriValue;
extern _SENSOR_CLI tSensorCliValue;
//extern Value_t m_value[100];
Value_t m_value[100];
_FAM_INFO tFamInfo;

int16_t nTimeRefreshLcd;
uint16_t nTimerCheckDebounce;
double fWaterTempStandard;
double fClimateTempStandard;
double fClimateHumiStandard;
uint16_t nClimateLightStandard;
uint16_t nTankLevelStandard1;
uint16_t nTankLevelStandard2;
uint16_t nTankLevelStandard3;
bool refreshLcd;
bool success;
bool bZigbeeState = true;
char ac_BuffLcd[30];

flagMode_t flagMode;
modeDisplay_t modeDisplay;
modeSetting_t modeSetting;
settingEthernet_t settingEthernet;
settingThreshold_t settingThreshold;
settingRtc_t settingRtc;
settingPump_t settingPump;
settingCalibEC_t settingCalibEC;
settingCalibWT_t settingCalibWT;
settingCalibPH_t settingCalibPH;
settingCalibTE_t settingCalibTE;
settingCalibHU_t settingCalibHU;
settingCalibLI_t settingCalibLI;
settingCalibFS_t settingCalibFS;
settingCalibTankLevel1_t settingCalibTankLevel1;
settingCalibTankLevel2_t settingCalibTankLevel2;
settingCalibTankLevel3_t settingCalibTankLevel3;
settingExIO_t settingExIO;
settingEnable_t settingEnable;
settingInfo_t settingInfo;

static bool HandleButtonMode(void);
static bool HandleButtonSet(void);
static bool HandleButtonUp(void);
static bool HandleButtonDown(void);
static void MODE_Disp_Success(void);

void MODE_Init(void)
{
	flagMode = MODE_DISPLAY;
	modeDisplay = DISPLAY_EC_PH_WT;
	settingEthernet = SETTING_ETHERNET_NONE;
	settingRtc = SETTING_RTC_NONE;
	settingThreshold = SETTING_THRESHOLD_NONE;
	settingPump = SETTING_PUMP_NONE;
	settingCalibEC = SETTING_CALIB_EC_NONE;
	settingCalibWT = SETTING_CALIB_WT_NONE;
	settingExIO = SETTING_EXIO_NONE;
	settingEnable = SETTING_ENABLE_NONE;
	settingInfo = SETTING_INFO_NONE;
	LCD4_Clear();
}

void MODE_Disp_Success(void)
{
	flagMode = MODE_CONFIG_SUCCESS;
	success = true;
}

bool MODE_Lcd(void)
{
	//	char *ac_BuffLcd;
	//	ac_BuffLcd = (char *)malloc(20 * sizeof(char));

	switch (flagMode)
	{
	case MODE_DISPLAY:
		switch (modeDisplay)
		{
		case DISPLAY_EC_PH_WT:
			if (refreshLcd == true)
			{
				LCD4_Clear();
				refreshLcd = false;
			}

			// RTC and farmName
			sprintf(ac_BuffLcd, "%s  ", tFamInfo.cFarmName);
			LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
			sprintf(ac_BuffLcd, "%02d:%02d:%02d", tRtc.Hour, tRtc.Min, tRtc.Sec);
			LCD4_WriteCursor(0, 12, (int8_t *)ac_BuffLcd);

			sprintf(ac_BuffLcd, "PH: %01.02f  %01.02f", tSensorIrriValue.dPh1, tSensorIrriValue.dPh2);
			LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
			sprintf(ac_BuffLcd, "EC: %01.02f  %01.02f", tSensorIrriValue.dEc1, tSensorIrriValue.dEc2);
			LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
			sprintf(ac_BuffLcd, "WT: %02.02f %02.02f", tSensorIrriValue.dWaterTemp1, tSensorIrriValue.dWaterTemp2);
			LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);

			if (nTimeRefreshLcd > 600)
			{
				switch (tSensorIrriValue.tEcCheck)
				{
				case CHECK_EC_HIGH:
					LCD4_WriteCursor(2, 16, (int8_t *)"HIGH");
					break;
				case CHECK_EC_LOW:
					LCD4_WriteCursor(2, 16, (int8_t *)"LOW");
					break;
				case CHECK_EC_DUAL:
					LCD4_WriteCursor(2, 16, (int8_t *)"DUAL");
					break;
				default:
					break;
				}
				switch (tSensorIrriValue.tWtCheck)
				{
				case CHECK_WT_HIGH:
					LCD4_WriteCursor(3, 16, (int8_t *)"HIGH");
					break;
				case CHECK_WT_LOW:
					LCD4_WriteCursor(3, 16, (int8_t *)"LOW");
					break;
				case CHECK_WT_DUAL:
					LCD4_WriteCursor(3, 16, (int8_t *)"DUAL");
					break;
				default:
					break;
				}
				switch (tSensorIrriValue.tPhCheck)
				{
				case CHECK_PH_HIGH:
					LCD4_WriteCursor(1, 16, (int8_t *)"HIGH");
					break;
				case CHECK_PH_LOW:
					LCD4_WriteCursor(1, 16, (int8_t *)"LOW");
					break;
				case CHECK_PH_DUAL:
					LCD4_WriteCursor(1, 16, (int8_t *)"DUAL");
					break;
				default:
					break;
				}
			}
			else
			{
				LCD4_WriteCursor(1, 16, (int8_t *)"    ");
				LCD4_WriteCursor(2, 16, (int8_t *)"    ");
				LCD4_WriteCursor(3, 16, (int8_t *)"    ");
			}
			break;
		case DISPLAY_TE_HU_LI:
			if (refreshLcd == true)
			{
				LCD4_Clear();
				refreshLcd = false;
			}

			// RTC and farmName
			sprintf(ac_BuffLcd, "%s  ", tFamInfo.cFarmName);
			LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
			sprintf(ac_BuffLcd, "%02d:%02d:%02d", tRtc.Hour, tRtc.Min, tRtc.Sec);
			LCD4_WriteCursor(0, 12, (int8_t *)ac_BuffLcd);

			sprintf(ac_BuffLcd, "Te: %02.02f %02.02f", tSensorCliValue.fCliTemp1, tSensorCliValue.fCliTemp2);
			LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
			sprintf(ac_BuffLcd, "Hu: %02.02f %02.02f", tSensorCliValue.fCliHumi1, tSensorCliValue.fCliHumi2);
			LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
			sprintf(ac_BuffLcd, "Li: %05d %05d", tSensorCliValue.nCliLight1, tSensorCliValue.nCliLight2);
			LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);

			if (nTimeRefreshLcd > 600)
			{
				switch (tSensorCliValue.tTempCheck)
				{
				case CHECK_TEMP_HIGH:
					LCD4_WriteCursor(1, 16, (int8_t *)"HIGH");
					break;
				case CHECK_TEMP_LOW:
					LCD4_WriteCursor(1, 16, (int8_t *)"LOW");
					break;
				case CHECK_TEMP_DUAL:
					LCD4_WriteCursor(1, 16, (int8_t *)"DUAL");
					break;
				default:
					break;
				}
				switch (tSensorCliValue.tHumiCheck)
				{
				case CHECK_HUMI_HIGH:
					LCD4_WriteCursor(2, 16, (int8_t *)"HIGH");
					break;
				case CHECK_HUMI_LOW:
					LCD4_WriteCursor(2, 16, (int8_t *)"LOW");
					break;
				case CHECK_HUMI_DUAL:
					LCD4_WriteCursor(2, 16, (int8_t *)"DUAL");
					break;
				default:
					break;
				}
				switch (tSensorCliValue.tLightCheck)
				{
				case CHECK_LIGHT_HIGH:
					LCD4_WriteCursor(3, 16, (int8_t *)"HIGH");
					break;
				case CHECK_LIGHT_LOW:
					LCD4_WriteCursor(3, 16, (int8_t *)"LOW");
					break;
				case CHECK_LIGHT_DUAL:
					LCD4_WriteCursor(3, 16, (int8_t *)"DUAL");
					break;
				default:
					break;
				}
			}
			else
			{
				LCD4_WriteCursor(1, 16, (int8_t *)"    ");
				LCD4_WriteCursor(2, 16, (int8_t *)"    ");
				LCD4_WriteCursor(3, 16, (int8_t *)"    ");
			}
			break;
		case DISPLAY_FS_US:
			if (refreshLcd == true)
			{
				LCD4_Clear();
				refreshLcd = false;
			}

			sprintf(ac_BuffLcd, "Flow : xxx mL/m");
			LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
			sprintf(ac_BuffLcd, "Tank1: %3d %3d", tDistance.dLitUs1, tDistance.dLitUs2);
			LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
			sprintf(ac_BuffLcd, "Tank2: %3d %3d", tDistance.dLitUs3, tDistance.dLitUs4);
			LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
			sprintf(ac_BuffLcd, "Tank3: %3d %3d", tDistance.dLitUs5, tDistance.dLitUs6);
			LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);

			if (nTimeRefreshLcd > 600)
			{
				switch (tDistance.tTank1Check)
				{
				case CHECK_TANK1_HIGH:
					LCD4_WriteCursor(1, 16, (int8_t *)"HIGH");
					break;
				case CHECK_TANK1_LOW:
					LCD4_WriteCursor(1, 16, (int8_t *)"LOW");
					break;
				case CHECK_TANK1_DUAL:
					LCD4_WriteCursor(1, 16, (int8_t *)"DUAL");
					break;
				default:
					break;
				}
				switch (tDistance.tTank2Check)
				{
				case CHECK_TANK2_HIGH:
					LCD4_WriteCursor(2, 16, (int8_t *)"HIGH");
					break;
				case CHECK_TANK2_LOW:
					LCD4_WriteCursor(2, 16, (int8_t *)"LOW");
					break;
				case CHECK_TANK2_DUAL:
					LCD4_WriteCursor(2, 16, (int8_t *)"DUAL");
					break;
				default:
					break;
				}
				switch (tDistance.tTank3Check)
				{
				case CHECK_TANK3_HIGH:
					LCD4_WriteCursor(3, 16, (int8_t *)"HIGH");
					break;
				case CHECK_TANK3_LOW:
					LCD4_WriteCursor(3, 16, (int8_t *)"LOW");
					break;
				case CHECK_TANK3_DUAL:
					LCD4_WriteCursor(3, 16, (int8_t *)"DUAL");
					break;
				default:
					break;
				}
			}
			else
			{
				LCD4_WriteCursor(1, 16, (int8_t *)"    ");
				LCD4_WriteCursor(2, 16, (int8_t *)"    ");
				LCD4_WriteCursor(3, 16, (int8_t *)"    ");
			}
			break;
		}
		break;
	case MODE_SETTING:
		switch (modeSetting)
		{
//TODO: Ethernet Exclude
		case SETTING_INTERNET:
			switch (settingEthernet)
			{
			case SETTING_ETHERNET_NONE:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}

				LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
				sprintf(ac_BuffLcd, "    %01d", modeSetting);
				LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(1, 0, (int8_t *)"1. Internet");
				LCD4_WriteCursor(2, 0, (int8_t *)"2. Threshold");
				LCD4_WriteCursor(3, 0, (int8_t *)"3. Pump");
				LCD4_WriteCursor(1, 19, (int8_t *)"*");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			default:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
//				sprintf(ac_BuffLcd, "IP: %15s ", ethCheck.nIpAddress);
//				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);

//				if (ethCheck.bState)
//					sprintf(ac_BuffLcd, "<<  Connected   >>");
//				else
//					sprintf(ac_BuffLcd, "<< Disconnected >>");
//				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);

				if (bZigbeeState == true)
					sprintf(ac_BuffLcd, "Zigbee State:   ON");
				else
					sprintf(ac_BuffLcd, "Zigbee State:  OFF");
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);

				sprintf(ac_BuffLcd, "T Update: %3d (s)", tFamInfo.nTimeUpload);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			}
			switch (settingEthernet)
			{
				case SETTING_ZIGBEE_STATE:
					LCD4_WriteCursor(2, 19, (int8_t *)"*");
					LCD4_WriteCursor(3, 19, (int8_t *)" ");
					break;
				case SETTING_ETHERNET_UPDATE:
					LCD4_WriteCursor(2, 19, (int8_t *)" ");
					LCD4_WriteCursor(3, 19, (int8_t *)"*");
					break;
				default:
					break;
			}
			break;
		case SETTING_THRESHOLD:
			switch (settingThreshold)
			{
			case SETTING_THRESHOLD_NONE:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
				sprintf(ac_BuffLcd, "    %01d", modeSetting);
				LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(1, 0, (int8_t *)"1. Internet");
				LCD4_WriteCursor(2, 0, (int8_t *)"2. Threshold");
				LCD4_WriteCursor(3, 0, (int8_t *)"3. Pump");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)"*");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_THRESHOLD_EC:
			case SETTING_THRESHOLD_EC_LOW:
			case SETTING_THRESHOLD_EC_HIGH:
			case SETTING_THRESHOLD_EC_DUAL:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				sprintf(ac_BuffLcd, "EC:      %4.2f", (float)tFamInfo.nThrehold_Ec / 100);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "EC LOW:  %4.2f", (float)tFamInfo.nThrehold_EcLow / 100);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "EC HIGH: %4.2f", (float)tFamInfo.nThrehold_ECHigh / 100);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "EC DUAL: %4.2f", (float)tFamInfo.nThrehold_ECDual / 100);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			case SETTING_THRESHOLD_WATERTEMP:
			case SETTING_THRESHOLD_WATERTEMP_LOW:
			case SETTING_THRESHOLD_WATERTEMP_HIGH:
			case SETTING_THRESHOLD_WATERTEMP_DUAL:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				sprintf(ac_BuffLcd, "WT:      %2d", tFamInfo.nThrehold_WT / 10);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "WT LOW:  %2d", tFamInfo.nThrehold_WTLow / 10);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "WT HIGH: %2d", tFamInfo.nThrehold_WTHigh / 10);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "WT DUAL: %2d", tFamInfo.nThrehold_WTDual / 10);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			case SETTING_THRESHOLD_PH:
			case SETTING_THRESHOLD_PH_LOW:
			case SETTING_THRESHOLD_PH_HIGH:
			case SETTING_THRESHOLD_PH_DUAL:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				sprintf(ac_BuffLcd, "PH:       %4.2f", (float)tFamInfo.nThrehold_PH / 100);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "PH LOW:   %4.2f", (float)tFamInfo.nThrehold_PHLow / 100);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "PH HIGH:  %4.2f", (float)tFamInfo.nThrehold_PHHigh / 100);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "PH DUAL:  %4.2f", (float)tFamInfo.nThrehold_PHDual / 100);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			case SETTING_THRESHOLD_TE:
			case SETTING_THRESHOLD_TE_LOW:
			case SETTING_THRESHOLD_TE_HIGH:
			case SETTING_THRESHOLD_TE_DUAL:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				sprintf(ac_BuffLcd, "TE:      %2d", tFamInfo.nThrehold_Cli_Temp / 10);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "TE LOW:  %2d", tFamInfo.nThrehold_Cli_TempLow / 10);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "TE HIGH: %2d", tFamInfo.nThrehold_Cli_TempHigh / 10);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "TE DUAL: %2d", tFamInfo.nThrehold_Cli_TempDual / 10);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			case SETTING_THRESHOLD_HU:
			case SETTING_THRESHOLD_HU_LOW:
			case SETTING_THRESHOLD_HU_HIGH:
			case SETTING_THRESHOLD_HU_DUAL:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				sprintf(ac_BuffLcd, "HU:      %2d", tFamInfo.nThrehold_Cli_Humi / 10);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "HU LOW:  %2d", tFamInfo.nThrehold_Cli_HumiLow / 10);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "HU HIGH: %2d", tFamInfo.nThrehold_Cli_HumiHigh / 10);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "HU DUAL: %2d", tFamInfo.nThrehold_Cli_HumiDual / 10);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			case SETTING_THRESHOLD_LI:
			case SETTING_THRESHOLD_LI_LOW:
			case SETTING_THRESHOLD_LI_HIGH:
			case SETTING_THRESHOLD_LI_DUAL:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				sprintf(ac_BuffLcd, "LI:      %5d", tFamInfo.nThrehold_Cli_Light);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "LI LOW:  %5d", tFamInfo.nThrehold_Cli_LightLow);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "LI HIGH: %5d", tFamInfo.nThrehold_Cli_LightHigh);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "LI DUAL: %5d", tFamInfo.nThrehold_Cli_LightDual);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			case SETTING_THRESHOLD_FS:
			case SETTING_THRESHOLD_FS_LOW:
			case SETTING_THRESHOLD_FS_HIGH:
			case SETTING_THRESHOLD_FS_DUAL:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				sprintf(ac_BuffLcd, "FS:      %2d", tFamInfo.nThrehold_FlowRate);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "FS LOW:  %2d", tFamInfo.nThrehold_FlowRateLow);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "FS HIGH: %2d", tFamInfo.nThrehold_FlowRateHigh);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "FS DUAL: %2d", tFamInfo.nThrehold_FlowRateDual);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			case SETTING_THRESHOLD_TANKLEVEL:
			case SETTING_THRESHOLD_TANKLEVEL_LOW:
			case SETTING_THRESHOLD_TANKLEVEL_HIGH:
			case SETTING_THRESHOLD_TANKLEVEL_DUAL:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				sprintf(ac_BuffLcd, "LEVEL T:      %3d", tFamInfo.nThrehold_TankLevel / 10);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "LEVEL T LOW:  %2d", tFamInfo.nThrehold_TankLevelLow / 10);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "LEVEL T HIGH: %2d", tFamInfo.nThrehold_TankLevelHigh / 10);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "LEVEL T DUAL: %2d", tFamInfo.nThrehold_TankLevelDual / 10);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			}
			switch (settingThreshold)
			{
			case SETTING_THRESHOLD_EC:
			case SETTING_THRESHOLD_WATERTEMP:
			case SETTING_THRESHOLD_PH:
			case SETTING_THRESHOLD_TE:
			case SETTING_THRESHOLD_HU:
			case SETTING_THRESHOLD_LI:
			case SETTING_THRESHOLD_FS:
			case SETTING_THRESHOLD_TANKLEVEL:
				LCD4_WriteCursor(0, 19, (int8_t *)"*");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_THRESHOLD_EC_LOW:
			case SETTING_THRESHOLD_WATERTEMP_LOW:
			case SETTING_THRESHOLD_PH_LOW:
			case SETTING_THRESHOLD_TE_LOW:
			case SETTING_THRESHOLD_HU_LOW:
			case SETTING_THRESHOLD_LI_LOW:
			case SETTING_THRESHOLD_FS_LOW:
			case SETTING_THRESHOLD_TANKLEVEL_LOW:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)"*");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_THRESHOLD_EC_HIGH:
			case SETTING_THRESHOLD_WATERTEMP_HIGH:
			case SETTING_THRESHOLD_PH_HIGH:
			case SETTING_THRESHOLD_TE_HIGH:
			case SETTING_THRESHOLD_HU_HIGH:
			case SETTING_THRESHOLD_LI_HIGH:
			case SETTING_THRESHOLD_FS_HIGH:
			case SETTING_THRESHOLD_TANKLEVEL_HIGH:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)"*");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_THRESHOLD_EC_DUAL:
			case SETTING_THRESHOLD_WATERTEMP_DUAL:
			case SETTING_THRESHOLD_PH_DUAL:
			case SETTING_THRESHOLD_TE_DUAL:
			case SETTING_THRESHOLD_HU_DUAL:
			case SETTING_THRESHOLD_LI_DUAL:
			case SETTING_THRESHOLD_FS_DUAL:
			case SETTING_THRESHOLD_TANKLEVEL_DUAL:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)"*");
				break;
			}
			break;
		case SETTING_PUMP:
			switch (settingPump)
			{
			case SETTING_PUMP_NONE:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
				sprintf(ac_BuffLcd, "    %01d", modeSetting);
				LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(1, 0, (int8_t *)"1. Internet");
				LCD4_WriteCursor(2, 0, (int8_t *)"2. Threshold");
				LCD4_WriteCursor(3, 0, (int8_t *)"3. Pump");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)"*");
				break;
			default:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				sprintf(ac_BuffLcd, "T Pump Peri :%3d(s)", tFamInfo.nPump_TimePeri);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "T Pump Mix  :%3d(s)", tFamInfo.nPump_TimePumpMix);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "T Nozzle    :%3d(s)", tFamInfo.nPump_TimeNozzleOn);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "T Noz Period:%3d(s)", tFamInfo.nPump_TimeNozzleOff);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			}
			switch (settingPump)
			{
			case SETTING_TIME_PUMP_PERI:
				LCD4_WriteCursor(0, 19, (int8_t *)"*");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_TIME_PUMP_MIX:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)"*");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_TIME_PUMP_NOZZLE:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)"*");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_TIME_PUMP_NOZZLE_WAIT:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)"*");
				break;
			default:
				break;
			}
			break;
		case SETTING_CALIB_EC:
			switch (settingCalibEC)
			{
			case SETTING_CALIB_EC_NONE:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
				sprintf(ac_BuffLcd, "    %01d", modeSetting);
				LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(1, 0, (int8_t *)"4. Calib EC");
				LCD4_WriteCursor(2, 0, (int8_t *)"5. Calib wTemp");
				LCD4_WriteCursor(3, 0, (int8_t *)"6. Calib PH");
				LCD4_WriteCursor(1, 19, (int8_t *)"*");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_EC_STANDARD:
			case SETTING_CLICK_CALIB_EC:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Put the probe into");
				LCD4_WriteCursor(1, 0, (int8_t *)"the EC solution");
				sprintf(ac_BuffLcd, "EC sol: %4d mS/cm", tFamInfo.nStandard_Ec);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(3, 0, (int8_t *)"<< CALIBRATION >>");
				break;
			case SETTING_CALIB_EC1_A:
			case SETTING_CALIB_EC1_B:
			case SETTING_CALIB_EC2_A:
			case SETTING_CALIB_EC2_B:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				sprintf(ac_BuffLcd, "Ec1: %2.2f-> A:%4d", tSensorIrriValue.dEc1, tFamInfo.nCalib_Ec1A);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "%4.0f mV   > B:%4d", tSensorIrriValue.dEc1mv, tFamInfo.nCalib_Ec1B);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "Ec2: %2.2f-> A:%4d", tSensorIrriValue.dEc2, tFamInfo.nCalib_Ec2A);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "%4.0f mV   > B:%4d", tSensorIrriValue.dEc2mv, tFamInfo.nCalib_Ec2B);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			default:
				break;
			}
			switch (settingCalibEC)
			{
			case SETTING_EC_STANDARD:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)"*");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_CLICK_CALIB_EC:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)"*");
				break;
			case SETTING_CALIB_EC1_A:
				LCD4_WriteCursor(0, 19, (int8_t *)"*");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_CALIB_EC1_B:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)"*");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_CALIB_EC2_A:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)"*");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_CALIB_EC2_B:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)"*");
				break;
			default:
				break;
			}
			break;
		case SETTING_CALIB_WT:
			switch (settingCalibWT)
			{
			case SETTING_CALIB_WT_NONE:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
				sprintf(ac_BuffLcd, "    %01d", modeSetting);
				LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(1, 0, (int8_t *)"4. Calib EC");
				LCD4_WriteCursor(2, 0, (int8_t *)"5. Calib wTemp");
				LCD4_WriteCursor(3, 0, (int8_t *)"6. Calib PH");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)"*");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_WT_STANDARD:
			case SETTING_CLICK_CALIB_WT:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Put the probe into");
				LCD4_WriteCursor(1, 0, (int8_t *)"the Water Temp Sol");
				sprintf(ac_BuffLcd, "WT Stand: %2.2f ", fWaterTempStandard);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				LCD4_WriteCommand(1, 223);
				LCD4_WriteCommand(1, 67);
				LCD4_WriteCursor(3, 0, (int8_t *)"<< CALIBRATION >>");
				break;
			default:
				break;
			}
			switch (settingCalibWT)
			{
			case SETTING_WT_STANDARD:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)"*");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_CLICK_CALIB_WT:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)"*");
				break;
			default:
				break;
			}
			break;
		case SETTING_CALIB_PH:
			switch (settingCalibPH)
			{
			case SETTING_CALIB_PH_NONE:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
				sprintf(ac_BuffLcd, "    %01d", modeSetting);
				LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(1, 0, (int8_t *)"4. Calib EC");
				LCD4_WriteCursor(2, 0, (int8_t *)"5. Calib wTemp");
				LCD4_WriteCursor(3, 0, (int8_t *)"6. Calib PH");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)"*");
				break;
			case SETTING_PH_STANDARD:
			case SETTING_CLICK_CALIB_PH:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Put the probe into");
				LCD4_WriteCursor(1, 0, (int8_t *)"the PH solution");
				sprintf(ac_BuffLcd, "PH solution: %4.2f  ", (double)tFamInfo.nStandard_Ph / 100);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(3, 0, (int8_t *)"<< CALIBRATION >>");
				break;
			case SETTING_CALIB_PH1_A:
			case SETTING_CALIB_PH1_B:
			case SETTING_CALIB_PH2_A:
			case SETTING_CALIB_PH2_B:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				sprintf(ac_BuffLcd, "Ph1: %3.2f-> A:%4d", tSensorIrriValue.dPh1, tFamInfo.nCalib_Ph1A);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "%4.0f mV   > B:%4d", tSensorIrriValue.dPh1mv, tFamInfo.nCalib_Ph1B);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "Ph2: %3.2f-> A:%4d", tSensorIrriValue.dPh2, tFamInfo.nCalib_Ph2A);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "%4.0f mV   > B:%4d", tSensorIrriValue.dPh2mv, tFamInfo.nCalib_Ph2B);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			default:
				break;
			}
			switch (settingCalibPH)
			{
			case SETTING_PH_STANDARD:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)"*");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_CLICK_CALIB_PH:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)"*");
				break;
			case SETTING_CALIB_PH1_A:
				LCD4_WriteCursor(0, 19, (int8_t *)"*");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_CALIB_PH1_B:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)"*");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_CALIB_PH2_A:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)"*");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_CALIB_PH2_B:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)"*");
				break;
			default:
				break;
			}
			break;
		case SETTING_CALIB_TE:
			switch (settingCalibTE)
			{
			case SETTING_CALIB_TE_NONE:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
				sprintf(ac_BuffLcd, "    %01d", modeSetting);
				LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(1, 0, (int8_t *)"7. Calib Cli Temp");
				LCD4_WriteCursor(2, 0, (int8_t *)"8. Calib Cli Humi");
				LCD4_WriteCursor(3, 0, (int8_t *)"9. Calib Cli Light");
				LCD4_WriteCursor(1, 19, (int8_t *)"*");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_CALIB_TE_STAND:
				//			case SETTING_CALIB_TE_CLICK:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Temperature Standard");
				sprintf(ac_BuffLcd, "%2.2f ", fClimateTempStandard);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				LCD4_WriteCommand(1, 223);
				LCD4_WriteCommand(1, 67);
				//				LCD4_WriteCursor(2, 0, (int8_t *)"<< CALIBRATION >>");
				break;
			default:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				break;
			}
			//			switch (settingCalibTE)
			//			{
			//			case SETTING_CALIB_TE_STAND:
			//				LCD4_WriteCursor(1, 19, (int8_t *)"*");
			//				LCD4_WriteCursor(2, 19, (int8_t *)" ");
			//				break;
			//			case SETTING_CALIB_TE_CLICK:
			//				LCD4_WriteCursor(1, 19, (int8_t *)" ");
			//				LCD4_WriteCursor(2, 19, (int8_t *)"*");
			//				break;
			//			default:
			//				break;
			//			}
			break;
		case SETTING_CALIB_HU:
			switch (settingCalibHU)
			{
			case SETTING_CALIB_HU_NONE:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
				sprintf(ac_BuffLcd, "    %01d", modeSetting);
				LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(1, 0, (int8_t *)"7. Calib Cli Temp");
				LCD4_WriteCursor(2, 0, (int8_t *)"8. Calib Cli Humi");
				LCD4_WriteCursor(3, 0, (int8_t *)"9. Calib Cli Light");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)"*");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_CALIB_HU_STAND:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Humidity Standard");
				sprintf(ac_BuffLcd, "%2.2f ", fClimateHumiStandard);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				LCD4_WriteCommand(1, 37);
				break;
			default:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				break;
			}
			break;
		case SETTING_CALIB_LI:
			switch (settingCalibLI)
			{
			case SETTING_CALIB_LI_NONE:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
				sprintf(ac_BuffLcd, "    %01d", modeSetting);
				LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(1, 0, (int8_t *)"7. Calib Cli Temp");
				LCD4_WriteCursor(2, 0, (int8_t *)"8. Calib Cli Humi");
				LCD4_WriteCursor(3, 0, (int8_t *)"9. Calib Cli Light");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)"*");
				break;
			case SETTING_CALIB_LI_STAND:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Light Standard");
				sprintf(ac_BuffLcd, "%d Lux", nClimateLightStandard);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				break;
			default:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				break;
			}
			break;
		case SETTING_CALIB_FS:
			switch (settingCalibFS)
			{
			case SETTING_CALIB_FS_NONE:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
				sprintf(ac_BuffLcd, "   %01d", modeSetting);
				LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(1, 0, (int8_t *)"10. Calib Flow");
				LCD4_WriteCursor(2, 0, (int8_t *)"11. Calib US Tank1");
				LCD4_WriteCursor(3, 0, (int8_t *)"12. Calib US Tank2");
				LCD4_WriteCursor(1, 19, (int8_t *)"*");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			default:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_1:
			switch (settingCalibTankLevel1)
			{
			case SETTING_CALIB_TANK_LEVEL_1_NONE:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
				sprintf(ac_BuffLcd, "   %01d", modeSetting);
				LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(1, 0, (int8_t *)"10. Calib Flow");
				LCD4_WriteCursor(2, 0, (int8_t *)"11. Calib US Tank1");
				LCD4_WriteCursor(3, 0, (int8_t *)"12. Calib US Tank2");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)"*");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_CALIB_TANK_LEVEL_1_STAND:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Tank 1 Level");
				sprintf(ac_BuffLcd, "%d Liter", nTankLevelStandard1);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				break;
			default:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_2:
			switch (settingCalibTankLevel2)
			{
			case SETTING_CALIB_TANK_LEVEL_2_NONE:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
				sprintf(ac_BuffLcd, "   %01d", modeSetting);
				LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(1, 0, (int8_t *)"10. Calib Flow");
				LCD4_WriteCursor(2, 0, (int8_t *)"11. Calib US Tank1");
				LCD4_WriteCursor(3, 0, (int8_t *)"12. Calib US Tank2");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)"*");
				break;
			case SETTING_CALIB_TANK_LEVEL_2_STAND:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Tank 2 Level");
				sprintf(ac_BuffLcd, "%d Liter", nTankLevelStandard2);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				break;
			default:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_3:
			switch (settingCalibTankLevel3)
			{
			case SETTING_CALIB_TANK_LEVEL_3_NONE:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
				sprintf(ac_BuffLcd, "   %01d", modeSetting);
				LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(1, 0, (int8_t *)"13. Calib US Tank3");
				LCD4_WriteCursor(2, 0, (int8_t *)"14. EX IO");
				LCD4_WriteCursor(3, 0, (int8_t *)"15. Clock");
				LCD4_WriteCursor(1, 19, (int8_t *)"*");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_CALIB_TANK_LEVEL_3_STAND:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Tank 3 Level");
				sprintf(ac_BuffLcd, "%d Liter", nTankLevelStandard3);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				break;
			default:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				break;
			}
			break;
		case SETTING_EXIO:
			switch (settingExIO)
			{
			case SETTING_EXIO_NONE:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
				sprintf(ac_BuffLcd, "   %01d", modeSetting);
				LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(1, 0, (int8_t *)"13. Calib US Tank3");
				LCD4_WriteCursor(2, 0, (int8_t *)"14. EX IO");
				LCD4_WriteCursor(3, 0, (int8_t *)"15. Clock");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)"*");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			default:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				sprintf(ac_BuffLcd, "T Nozzle On: %02d(h)", tFamInfo.nTime_NozzleOn);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "T Nozzle Off:%02d(h)", tFamInfo.nTime_NozzleOff);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "T CooPad On: %02d(h)", tFamInfo.nTime_CoopadOn);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "T CooPad Off:%02d(h)", tFamInfo.nTime_CoopadOff);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			}
			switch (settingExIO)
			{
			case SETTING_EXIO_NOZZLE_TIME_ON:
				LCD4_WriteCursor(0, 19, (int8_t *)"*");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_EXIO_NOZZLE_TIME_OFF:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)"*");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_EXIO_COOPAD_TIME_ON:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)"*");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_EXIO_COOPAD_TIME_OFF:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)"*");
				break;
			default:
				break;
			}
			break;
		case SETTING_RTC:
			switch (settingRtc)
			{
			case SETTING_RTC_NONE:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
				sprintf(ac_BuffLcd, "   %01d", modeSetting);
				LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(1, 0, (int8_t *)"13. Calib US Tank3");
				LCD4_WriteCursor(2, 0, (int8_t *)"14. EX IO");
				LCD4_WriteCursor(3, 0, (int8_t *)"15. Clock");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)"*");
				break;
			default:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Setting Clock");
				sprintf(ac_BuffLcd, "Hour:    %02d", tRtc.Hour);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "Minute:  %02d", tRtc.Min);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				break;
			}
			switch (settingRtc)
			{
			case SETTING_RTC_HOUR:
				LCD4_WriteCursor(1, 19, (int8_t *)"*");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				break;
			case SETTING_RTC_MINUTE:
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)"*");
				break;
			default:
				break;
			}
			break;
		case SETTING_ENABLE:
			switch (settingEnable)
			{
			case SETTING_ENABLE_NONE:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
				sprintf(ac_BuffLcd, "   %01d", modeSetting);
				LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(1, 0, (int8_t *)"16. Devices Enable");
				LCD4_WriteCursor(2, 0, (int8_t *)"17. Default");
				LCD4_WriteCursor(3, 0, (int8_t *)"18. Info");
				LCD4_WriteCursor(1, 19, (int8_t *)"*");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_ENABLE_EC:
			case SETTING_ENABLE_PH:
			case SETTING_ENABLE_O1:
			case SETTING_ENABLE_O2:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				if ((OP_EC_GPIO_Port->ODR & OP_EC_Pin) != 0x00u)
					LCD4_WriteCursor(0, 0, (int8_t *)"Pump EC        ON ");
				else
					LCD4_WriteCursor(0, 0, (int8_t *)"Pump EC        OFF");

				if ((OP_PH_GPIO_Port->ODR & OP_PH_Pin) != 0x00u)
					LCD4_WriteCursor(1, 0, (int8_t *)"Pump PH        ON ");
				else
					LCD4_WriteCursor(1, 0, (int8_t *)"Pump PH        OFF");

				if ((OP_O1_GPIO_Port->ODR & OP_O1_Pin) != 0x00u)
					LCD4_WriteCursor(2, 0, (int8_t *)"Nozzle         ON ");
				else
					LCD4_WriteCursor(2, 0, (int8_t *)"Nozzle         OFF");

				if ((OP_O2_GPIO_Port->ODR & OP_O2_Pin) != 0x00u)
					LCD4_WriteCursor(3, 0, (int8_t *)"Chiller        ON ");
				else
					LCD4_WriteCursor(3, 0, (int8_t *)"Chiller        OFF");
				break;
			case SETTING_ENABLE_O3:
			case SETTING_ENABLE_O4:
			case SETTING_ENABLE_O5:
			case SETTING_ENABLE_O6:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				if ((OP_O3_GPIO_Port->ODR & OP_O3_Pin) != 0x00u)
					LCD4_WriteCursor(0, 0, (int8_t *)"Cooling Pad    ON ");
				else
					LCD4_WriteCursor(0, 0, (int8_t *)"Cooling Pad    OFF");

				if ((OP_O4_GPIO_Port->ODR & OP_O4_Pin) != 0x00u)
					LCD4_WriteCursor(1, 0, (int8_t *)"Motor Mix      ON ");
				else
					LCD4_WriteCursor(1, 0, (int8_t *)"Motor Mix      OFF");

				if ((OP_O5_GPIO_Port->ODR & OP_O5_Pin) != 0x00u)
					LCD4_WriteCursor(2, 0, (int8_t *)"O5             ON ");
				else
					LCD4_WriteCursor(2, 0, (int8_t *)"O5             OFF");

				if ((OP_O6_GPIO_Port->ODR & OP_O6_Pin) != 0x00u)
					LCD4_WriteCursor(3, 0, (int8_t *)"O6             ON ");
				else
					LCD4_WriteCursor(3, 0, (int8_t *)"O6             OFF");
				break;
			case SETTING_ENABLE_O7:
			case SETTING_ENABLE_O8:
			case SETTING_ENABLE_O9:
			case SETTING_ENABLE_O10:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				if ((OP_O7_GPIO_Port->ODR & OP_O7_Pin) != 0x00u)
					LCD4_WriteCursor(0, 0, (int8_t *)"O7             ON ");
				else
					LCD4_WriteCursor(0, 0, (int8_t *)"O7             OFF");

				if ((OP_O8_GPIO_Port->ODR & OP_O8_Pin) != 0x00u)
					LCD4_WriteCursor(1, 0, (int8_t *)"O8             ON ");
				else
					LCD4_WriteCursor(1, 0, (int8_t *)"O8             OFF");

				if ((OP_O9_GPIO_Port->ODR & OP_O9_Pin) != 0x00u)
					LCD4_WriteCursor(2, 0, (int8_t *)"O9             ON ");
				else
					LCD4_WriteCursor(2, 0, (int8_t *)"O9             OFF");

				if ((OP_O10_GPIO_Port->ODR & OP_O10_Pin) != 0x00u)
					LCD4_WriteCursor(3, 0, (int8_t *)"O10            ON ");
				else
					LCD4_WriteCursor(3, 0, (int8_t *)"O10            OFF");
				break;
			default:
				break;
			}
			switch (settingEnable)
			{
			case SETTING_ENABLE_EC:
			case SETTING_ENABLE_O3:
			case SETTING_ENABLE_O7:
				LCD4_WriteCursor(0, 19, (int8_t *)"*");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_ENABLE_PH:
			case SETTING_ENABLE_O4:
			case SETTING_ENABLE_O8:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)"*");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_ENABLE_O1:
			case SETTING_ENABLE_O5:
			case SETTING_ENABLE_O9:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)"*");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_ENABLE_O2:
			case SETTING_ENABLE_O6:
			case SETTING_ENABLE_O10:
				LCD4_WriteCursor(0, 19, (int8_t *)" ");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)"*");
				break;
			default:
				break;
			}
			break;
		case SETTING_DEFAULT:
			if (refreshLcd == true)
			{
				LCD4_Clear();
				refreshLcd = false;
			}
			LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
			sprintf(ac_BuffLcd, "   %01d", modeSetting);
			LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
			LCD4_WriteCursor(1, 0, (int8_t *)"16. Devices Enable");
			LCD4_WriteCursor(2, 0, (int8_t *)"17. Default");
			LCD4_WriteCursor(3, 0, (int8_t *)"18. Info");
			LCD4_WriteCursor(1, 19, (int8_t *)" ");
			LCD4_WriteCursor(2, 19, (int8_t *)"*");
			LCD4_WriteCursor(3, 19, (int8_t *)" ");
			break;
		case SETTING_INFO:
			switch (settingInfo)
			{
			case SETTING_INFO_NONE:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
				sprintf(ac_BuffLcd, "   %01d", modeSetting);
				LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(1, 0, (int8_t *)"16. Devices Enable");
				LCD4_WriteCursor(2, 0, (int8_t *)"17. Default");
				LCD4_WriteCursor(3, 0, (int8_t *)"18. Info");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)"*");
				break;
			case SETTING_INFO_FWVERSION:
			case SETTING_INFO_FARMCODE:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}

				LCD4_WriteCursor(0, 0, (int8_t *)"FW Version");
				sprintf(ac_BuffLcd, " %s", tFamInfo.cFwVersion);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(2, 0, (int8_t *)"Farm Code");
				sprintf(ac_BuffLcd, " %s", tFamInfo.cFarmCode);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			}
			break;
		default:
			break;
		}
		break;
	case MODE_CONFIG_SUCCESS:
		LCD4_Clear();
		HAL_Delay(1);
		if (success == true)
			LCD4_WriteCursor(2, 7, (int8_t *)"Saved!");
		else
			LCD4_WriteCursor(2, 7, (int8_t *)"Failed!");
		HAL_Delay(1000);

		LCD4_Clear();
		HAL_Delay(1);

		flagMode = MODE_SETTING;
		settingEthernet = SETTING_ETHERNET_NONE;
		settingThreshold = SETTING_THRESHOLD_NONE;
		settingPump = SETTING_PUMP_NONE;
		settingCalibEC = SETTING_CALIB_EC_NONE;
		settingCalibWT = SETTING_CALIB_WT_NONE;
		settingCalibPH = SETTING_CALIB_PH_NONE;
		settingCalibTE = SETTING_CALIB_TE_NONE;
		settingCalibHU = SETTING_CALIB_HU_NONE;
		settingCalibLI = SETTING_CALIB_LI_NONE;
		settingCalibFS = SETTING_CALIB_FS_NONE;
		settingCalibTankLevel1 = SETTING_CALIB_TANK_LEVEL_1_NONE;
		settingCalibTankLevel2 = SETTING_CALIB_TANK_LEVEL_2_NONE;
		settingCalibTankLevel3 = SETTING_CALIB_TANK_LEVEL_3_NONE;
		settingExIO = SETTING_EXIO_NONE;
		settingEnable = SETTING_ENABLE_NONE;
		settingInfo = SETTING_INFO_NONE;
		break;
	}

	//	free(ac_BuffLcd);

	return true;
}

bool MODE_Timer(void)
{
	nTimeRefreshLcd++;
	if (nTimeRefreshLcd > 1200)
		nTimeRefreshLcd = 0;

	switch (ReadButtonMode())
	{
	case BUTTON_MODE_CLICK:
		HandleButtonMode();
		nTimerCheckDebounce = 0;
		break;
	case BUTTON_MODE_LONGCLICK:
		break;
	default:
		break;
	}
	switch (ReadButtonSet())
	{
	case BUTTON_SET_CLICK:
		HandleButtonSet();
		nTimerCheckDebounce = 0;
		break;
	case BUTTON_SET_LONGCLICK:
		nTimerCheckDebounce++;
		break;
	default:
		break;
	}
	switch (ReadButtonUp())
	{
	case BUTTON_UP_CLICK:
		HandleButtonUp();
		nTimerCheckDebounce = 0;
		break;
	case BUTTON_UP_LONGCLICK:
		if (nTimerCheckDebounce > TIMERHANDLELONGCLICK)
		{
			HandleButtonUp();
			nTimerCheckDebounce = 0;
		}
		else
		{
			nTimerCheckDebounce++;
		}
		break;
	default:
		break;
	}
	switch (ReadButtonDown())
	{
	case BUTTON_DOWN_CLICK:
		HandleButtonDown();
		nTimerCheckDebounce = 0;
		break;
	case BUTTON_DOWN_LONGCLICK:
		if (nTimerCheckDebounce > TIMERHANDLELONGCLICK)
		{
			HandleButtonDown();
			nTimerCheckDebounce = 0;
		}
		else
		{
			nTimerCheckDebounce++;
		}
		break;
	default:
		break;
	}

	return true;
}

bool HandleButtonMode(void)
{
	switch (flagMode)
	{
	case MODE_DISPLAY:
		flagMode = MODE_SETTING;
		modeSetting = SETTING_INTERNET;
		settingEthernet = SETTING_ETHERNET_NONE;
		settingThreshold = SETTING_THRESHOLD_NONE;
		settingPump = SETTING_PUMP_NONE;
		settingCalibEC = SETTING_CALIB_EC_NONE;
		settingCalibWT = SETTING_CALIB_WT_NONE;
		settingCalibPH = SETTING_CALIB_PH_NONE;
		settingCalibTE = SETTING_CALIB_TE_NONE;
		settingCalibHU = SETTING_CALIB_HU_NONE;
		settingCalibLI = SETTING_CALIB_LI_NONE;
		settingCalibFS = SETTING_CALIB_FS_NONE;
		settingCalibTankLevel1 = SETTING_CALIB_TANK_LEVEL_1_NONE;
		settingCalibTankLevel2 = SETTING_CALIB_TANK_LEVEL_2_NONE;
		settingCalibTankLevel3 = SETTING_CALIB_TANK_LEVEL_3_NONE;
		settingExIO = SETTING_EXIO_NONE;
		settingRtc = SETTING_RTC_NONE;
		settingEnable = SETTING_ENABLE_NONE;
		settingInfo = SETTING_INFO_NONE;
		refreshLcd = true;
		break;
	case MODE_SETTING:
		if ((settingEthernet == SETTING_ETHERNET_NONE) && (settingThreshold == SETTING_THRESHOLD_NONE) &&
			(settingPump == SETTING_PUMP_NONE) && (settingCalibEC == SETTING_CALIB_EC_NONE) &&
			(settingCalibWT == SETTING_CALIB_WT_NONE) && (settingCalibPH == SETTING_CALIB_PH_NONE) &&
			(settingCalibTE == SETTING_CALIB_TE_NONE) && (settingCalibHU == SETTING_CALIB_HU_NONE) &&
			(settingCalibLI == SETTING_CALIB_LI_NONE) && (settingCalibFS == SETTING_CALIB_FS_NONE) &&
			(settingCalibTankLevel1 == SETTING_CALIB_TANK_LEVEL_1_NONE) && (settingCalibTankLevel2 == SETTING_CALIB_TANK_LEVEL_2_NONE) &&
			(settingCalibTankLevel3 == SETTING_CALIB_TANK_LEVEL_3_NONE) && (settingExIO == SETTING_EXIO_NONE) &&
			(settingRtc == SETTING_RTC_NONE) && (settingEnable == SETTING_ENABLE_NONE) &&
			(settingInfo == SETTING_INFO_NONE))
		{
			modeSetting = SETTING_INTERNET;
			flagMode = MODE_DISPLAY;
		}
		else if (settingRtc != SETTING_RTC_NONE)
		{
			tRtc.State = DS3231_SET_TIME;
			settingRtc = SETTING_RTC_NONE;
			MODE_Disp_Success();
		}
		else if (settingInfo != SETTING_INFO_NONE)
		{
			settingInfo = SETTING_INFO_NONE;
		}
		else if (settingEnable != SETTING_ENABLE_NONE)
		{
			settingEnable = SETTING_ENABLE_NONE;
		}
		else
		{
			MODE_ParamsServerSave();
			MODE_Disp_Success();
			//ETH_ResetTimerSysInfo();
		}
		refreshLcd = true;
		break;
	case MODE_CONFIG_SUCCESS:
		break;
	}

	return true;
}
bool HandleButtonSet(void)
{
	switch (flagMode)
	{
	case MODE_DISPLAY:
		break;
	case MODE_SETTING:
		switch (modeSetting)
		{
		case SETTING_INTERNET:
			switch (settingEthernet)
			{
			case SETTING_ETHERNET_NONE:
				settingEthernet = SETTING_ZIGBEE_STATE;
				refreshLcd = true;
				break;
			case SETTING_ZIGBEE_STATE:
				settingEthernet = SETTING_ETHERNET_UPDATE;
				break;
			case SETTING_ETHERNET_UPDATE:
				settingEthernet = SETTING_ZIGBEE_STATE;
				break;
			}
			break;
		case SETTING_RTC:
			switch (settingRtc)
			{
			case SETTING_RTC_NONE:
				settingRtc = SETTING_RTC_HOUR;
				refreshLcd = true;
				break;
			case SETTING_RTC_HOUR:
				settingRtc = SETTING_RTC_MINUTE;
				break;
			case SETTING_RTC_MINUTE:
				settingRtc = SETTING_RTC_HOUR;
				break;
			default:
				break;
			}
			break;
		case SETTING_THRESHOLD:
			switch (settingThreshold)
			{
			case SETTING_THRESHOLD_NONE:
				settingThreshold = SETTING_THRESHOLD_EC;
				refreshLcd = true;
				break;
			case SETTING_THRESHOLD_EC:
				settingThreshold = SETTING_THRESHOLD_EC_LOW;
				break;
			case SETTING_THRESHOLD_EC_LOW:
				settingThreshold = SETTING_THRESHOLD_EC_HIGH;
				break;
			case SETTING_THRESHOLD_EC_HIGH:
				settingThreshold = SETTING_THRESHOLD_EC_DUAL;
				break;
			case SETTING_THRESHOLD_EC_DUAL:
				settingThreshold = SETTING_THRESHOLD_WATERTEMP;
				refreshLcd = true;
				break;
			case SETTING_THRESHOLD_WATERTEMP:
				settingThreshold = SETTING_THRESHOLD_WATERTEMP_LOW;
				break;
			case SETTING_THRESHOLD_WATERTEMP_LOW:
				settingThreshold = SETTING_THRESHOLD_WATERTEMP_HIGH;
				break;
			case SETTING_THRESHOLD_WATERTEMP_HIGH:
				settingThreshold = SETTING_THRESHOLD_WATERTEMP_DUAL;
				break;
			case SETTING_THRESHOLD_WATERTEMP_DUAL:
				settingThreshold = SETTING_THRESHOLD_PH;
				refreshLcd = true;
				break;
			case SETTING_THRESHOLD_PH:
				settingThreshold = SETTING_THRESHOLD_PH_LOW;
				break;
			case SETTING_THRESHOLD_PH_LOW:
				settingThreshold = SETTING_THRESHOLD_PH_HIGH;
				break;
			case SETTING_THRESHOLD_PH_HIGH:
				settingThreshold = SETTING_THRESHOLD_PH_DUAL;
				break;
			case SETTING_THRESHOLD_PH_DUAL:
				settingThreshold = SETTING_THRESHOLD_TE;
				refreshLcd = true;
				break;
			case SETTING_THRESHOLD_TE:
				settingThreshold = SETTING_THRESHOLD_TE_LOW;
				break;
			case SETTING_THRESHOLD_TE_LOW:
				settingThreshold = SETTING_THRESHOLD_TE_HIGH;
				break;
			case SETTING_THRESHOLD_TE_HIGH:
				settingThreshold = SETTING_THRESHOLD_TE_DUAL;
				break;
			case SETTING_THRESHOLD_TE_DUAL:
				settingThreshold = SETTING_THRESHOLD_HU;
				refreshLcd = true;
				break;
			case SETTING_THRESHOLD_HU:
				settingThreshold = SETTING_THRESHOLD_HU_LOW;
				break;
			case SETTING_THRESHOLD_HU_LOW:
				settingThreshold = SETTING_THRESHOLD_HU_HIGH;
				break;
			case SETTING_THRESHOLD_HU_HIGH:
				settingThreshold = SETTING_THRESHOLD_HU_DUAL;
				break;
			case SETTING_THRESHOLD_HU_DUAL:
				settingThreshold = SETTING_THRESHOLD_LI;
				refreshLcd = true;
				break;
			case SETTING_THRESHOLD_LI:
				settingThreshold = SETTING_THRESHOLD_LI_LOW;
				break;
			case SETTING_THRESHOLD_LI_LOW:
				settingThreshold = SETTING_THRESHOLD_LI_HIGH;
				break;
			case SETTING_THRESHOLD_LI_HIGH:
				settingThreshold = SETTING_THRESHOLD_LI_DUAL;
				break;
			case SETTING_THRESHOLD_LI_DUAL:
				settingThreshold = SETTING_THRESHOLD_FS;
				refreshLcd = true;
				break;
			case SETTING_THRESHOLD_FS:
				settingThreshold = SETTING_THRESHOLD_FS_LOW;
				break;
			case SETTING_THRESHOLD_FS_LOW:
				settingThreshold = SETTING_THRESHOLD_FS_HIGH;
				break;
			case SETTING_THRESHOLD_FS_HIGH:
				settingThreshold = SETTING_THRESHOLD_FS_DUAL;
				break;
			case SETTING_THRESHOLD_FS_DUAL:
				settingThreshold = SETTING_THRESHOLD_TANKLEVEL;
				refreshLcd = true;
				break;
			case SETTING_THRESHOLD_TANKLEVEL:
				settingThreshold = SETTING_THRESHOLD_TANKLEVEL_LOW;
				break;
			case SETTING_THRESHOLD_TANKLEVEL_LOW:
				settingThreshold = SETTING_THRESHOLD_TANKLEVEL_HIGH;
				break;
			case SETTING_THRESHOLD_TANKLEVEL_HIGH:
				settingThreshold = SETTING_THRESHOLD_TANKLEVEL_DUAL;
				break;
			case SETTING_THRESHOLD_TANKLEVEL_DUAL:
				settingThreshold = SETTING_THRESHOLD_EC;
				refreshLcd = true;
				break;
			}
			break;
		case SETTING_PUMP:
			switch (settingPump)
			{
			case SETTING_PUMP_NONE:
				settingPump = SETTING_TIME_PUMP_PERI;
				refreshLcd = true;
				break;
			case SETTING_TIME_PUMP_PERI:
				settingPump = SETTING_TIME_PUMP_MIX;
				break;
			case SETTING_TIME_PUMP_MIX:
				settingPump = SETTING_TIME_PUMP_NOZZLE;
				break;
			case SETTING_TIME_PUMP_NOZZLE:
				settingPump = SETTING_TIME_PUMP_NOZZLE_WAIT;
				break;
			case SETTING_TIME_PUMP_NOZZLE_WAIT:
				settingPump = SETTING_TIME_PUMP_PERI;
				refreshLcd = true;
				break;
			}
			break;
		case SETTING_CALIB_EC:
			switch (settingCalibEC)
			{
			case SETTING_CALIB_EC_NONE:
				settingCalibEC = SETTING_EC_STANDARD;
				refreshLcd = true;
				break;
			case SETTING_EC_STANDARD:
				settingCalibEC = SETTING_CLICK_CALIB_EC;
				break;
			case SETTING_CLICK_CALIB_EC:
				settingCalibEC = SETTING_CALIB_EC1_A;
				refreshLcd = true;
				break;
			case SETTING_CALIB_EC1_A:
				settingCalibEC = SETTING_CALIB_EC1_B;
				break;
			case SETTING_CALIB_EC1_B:
				settingCalibEC = SETTING_CALIB_EC2_A;
				break;
			case SETTING_CALIB_EC2_A:
				settingCalibEC = SETTING_CALIB_EC2_B;
				break;
			case SETTING_CALIB_EC2_B:
				settingCalibEC = SETTING_EC_STANDARD;
				break;
			}
			break;
		case SETTING_CALIB_WT:
			switch (settingCalibWT)
			{
			case SETTING_CALIB_WT_NONE:
				fWaterTempStandard = tSensorIrriValue.dWtAvg;
				settingCalibWT = SETTING_WT_STANDARD;
				refreshLcd = true;
				break;
			case SETTING_WT_STANDARD:
				settingCalibWT = SETTING_CLICK_CALIB_WT;
				break;
			case SETTING_CLICK_CALIB_WT:
				settingCalibWT = SETTING_WT_STANDARD;
				refreshLcd = true;
				break;
			}
			break;
		case SETTING_CALIB_PH:
			switch (settingCalibPH)
			{
			case SETTING_CALIB_PH_NONE:
				settingCalibPH = SETTING_PH_STANDARD;
				refreshLcd = true;
				break;
			case SETTING_PH_STANDARD:
				settingCalibPH = SETTING_CLICK_CALIB_PH;
				break;
			case SETTING_CLICK_CALIB_PH:
				settingCalibPH = SETTING_CALIB_PH1_A;
				refreshLcd = true;
				break;
			case SETTING_CALIB_PH1_A:
				settingCalibPH = SETTING_CALIB_PH1_B;
				break;
			case SETTING_CALIB_PH1_B:
				settingCalibPH = SETTING_CALIB_PH2_A;
				break;
			case SETTING_CALIB_PH2_A:
				settingCalibPH = SETTING_CALIB_PH2_B;
				break;
			case SETTING_CALIB_PH2_B:
				settingCalibPH = SETTING_PH_STANDARD;
				break;
			}
			break;
		case SETTING_CALIB_TE:
			switch (settingCalibTE)
			{
			case SETTING_CALIB_TE_NONE:
				fClimateTempStandard = tSensorCliValue.fCliTempAvg;
				settingCalibTE = SETTING_CALIB_TE_STAND;
				refreshLcd = true;
				break;
			case SETTING_CALIB_TE_STAND:
				break;
			}
			break;
		case SETTING_CALIB_HU:
			switch (settingCalibHU)
			{
			case SETTING_CALIB_HU_NONE:
				fClimateHumiStandard = tSensorCliValue.fCliHumiAvg;
				settingCalibHU = SETTING_CALIB_HU_STAND;
				refreshLcd = true;
				break;
			case SETTING_CALIB_HU_STAND:
				break;
			}
			break;
		case SETTING_CALIB_LI:
			switch (settingCalibLI)
			{
			case SETTING_CALIB_LI_NONE:
				nClimateLightStandard = tSensorCliValue.nCliLightAvg;
				settingCalibLI = SETTING_CALIB_LI_STAND;
				refreshLcd = true;
				break;
			case SETTING_CALIB_LI_STAND:
				break;
			}
			break;
		case SETTING_CALIB_FS:
			switch (settingCalibFS)
			{
			case SETTING_CALIB_FS_NONE:
				refreshLcd = true;
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_1:
			switch (settingCalibTankLevel1)
			{
			case SETTING_CALIB_TANK_LEVEL_1_NONE:
				nTankLevelStandard1 = tDistance.dLitTank1;
				settingCalibTankLevel1 = SETTING_CALIB_TANK_LEVEL_1_STAND;
				refreshLcd = true;
				break;
			case SETTING_CALIB_TANK_LEVEL_1_STAND:
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_2:
			switch (settingCalibTankLevel2)
			{
			case SETTING_CALIB_TANK_LEVEL_2_NONE:
				nTankLevelStandard2 = tDistance.dLitTank2;
				settingCalibTankLevel2 = SETTING_CALIB_TANK_LEVEL_2_STAND;
				refreshLcd = true;
				break;
			case SETTING_CALIB_TANK_LEVEL_2_STAND:
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_3:
			switch (settingCalibTankLevel3)
			{
			case SETTING_CALIB_TANK_LEVEL_3_NONE:
				nTankLevelStandard3 = tDistance.dLitTank3;
				settingCalibTankLevel3 = SETTING_CALIB_TANK_LEVEL_3_STAND;
				refreshLcd = true;
				break;
			case SETTING_CALIB_TANK_LEVEL_3_STAND:
				break;
			}
			break;
		case SETTING_EXIO:
			switch (settingExIO)
			{
			case SETTING_EXIO_NONE:
				settingExIO = SETTING_EXIO_NOZZLE_TIME_ON;
				refreshLcd = true;
				break;
			case SETTING_EXIO_NOZZLE_TIME_ON:
				settingExIO = SETTING_EXIO_NOZZLE_TIME_OFF;
				break;
			case SETTING_EXIO_NOZZLE_TIME_OFF:
				settingExIO = SETTING_EXIO_COOPAD_TIME_ON;
				break;
			case SETTING_EXIO_COOPAD_TIME_ON:
				settingExIO = SETTING_EXIO_COOPAD_TIME_OFF;
				break;
			case SETTING_EXIO_COOPAD_TIME_OFF:
				settingExIO = SETTING_EXIO_NOZZLE_TIME_ON;
				break;
			}
			break;
		case SETTING_ENABLE:
			switch (settingEnable)
			{
			case SETTING_ENABLE_NONE:
				settingEnable = SETTING_ENABLE_EC;
				refreshLcd = true;
				break;
			case SETTING_ENABLE_EC:
				settingEnable = SETTING_ENABLE_PH;
				break;
			case SETTING_ENABLE_PH:
				settingEnable = SETTING_ENABLE_O1;
				break;
			case SETTING_ENABLE_O1:
				settingEnable = SETTING_ENABLE_O2;
				break;
			case SETTING_ENABLE_O2:
				settingEnable = SETTING_ENABLE_O3;
				refreshLcd = true;
				break;
			case SETTING_ENABLE_O3:
				settingEnable = SETTING_ENABLE_O4;
				break;
			case SETTING_ENABLE_O4:
				settingEnable = SETTING_ENABLE_O5;
				break;
			case SETTING_ENABLE_O5:
				settingEnable = SETTING_ENABLE_O6;
				break;
			case SETTING_ENABLE_O6:
				settingEnable = SETTING_ENABLE_O7;
				refreshLcd = true;
				break;
			case SETTING_ENABLE_O7:
				settingEnable = SETTING_ENABLE_O8;
				break;
			case SETTING_ENABLE_O8:
				settingEnable = SETTING_ENABLE_O9;
				break;
			case SETTING_ENABLE_O9:
				settingEnable = SETTING_ENABLE_O10;
				break;
			case SETTING_ENABLE_O10:
				settingEnable = SETTING_ENABLE_EC;
				refreshLcd = true;
				break;
			default:
				break;
			}
			break;
		case SETTING_DEFAULT:
			if (nTimerCheckDebounce > 10000)
			{
				/* Info */
				// sprintf(tFamInfo.cFarmCode, "%s", "SGN022011IN000111");
				// sprintf(tFamInfo.cFarmName, "%s", "VP280");
				sprintf(tFamInfo.cFarmCode, "%s", "SGNCG2105IN000333");
				sprintf(tFamInfo.cFarmName, "%s", "YENFARM CG");
//				sprintf(tFamInfo.cFarmCode, "%s", "SGN022004IN000120");
//				sprintf(tFamInfo.cFarmName, "%s", "SMARTGREEN");
//				sprintf(tFamInfo.cFarmCode, "%s", "SGN022004IN000789");
//				sprintf(tFamInfo.cFarmName, "%s", "YENFARM LA");
				sprintf(tFamInfo.cFwVersion, "%s", "IN01C");

				/* Mode Internet */
				tFamInfo.nTimeUpload = 600;

				/* Mode Threhold */
				tFamInfo.nThrehold_Ec = 163;
				tFamInfo.nThrehold_EcLow = 160;
				tFamInfo.nThrehold_ECHigh = 165;
				tFamInfo.nThrehold_ECDual = 20;

				tFamInfo.nThrehold_WT = 250;
				tFamInfo.nThrehold_WTLow = 200;
				tFamInfo.nThrehold_WTHigh = 280;
				tFamInfo.nThrehold_WTDual = 20;

				tFamInfo.nThrehold_PH = 610;
				tFamInfo.nThrehold_PHLow = 590;
				tFamInfo.nThrehold_PHHigh = 630;
				tFamInfo.nThrehold_PHDual = 20;

				tFamInfo.nThrehold_Cli_Temp = 280;
				tFamInfo.nThrehold_Cli_TempLow = 200;
				tFamInfo.nThrehold_Cli_TempHigh = 320;
				tFamInfo.nThrehold_Cli_TempDual = 20;

				tFamInfo.nThrehold_Cli_Humi = 700;
				tFamInfo.nThrehold_Cli_HumiLow = 600;
				tFamInfo.nThrehold_Cli_HumiHigh = 800;
				tFamInfo.nThrehold_Cli_HumiDual = 50;

				tFamInfo.nThrehold_Cli_Light = 45000;
				tFamInfo.nThrehold_Cli_LightLow = 43000;
				tFamInfo.nThrehold_Cli_LightHigh = 48000;
				tFamInfo.nThrehold_Cli_LightDual = 2000;

				tFamInfo.nThrehold_FlowRate = 40;
				tFamInfo.nThrehold_FlowRateLow = 40;
				tFamInfo.nThrehold_FlowRateHigh = 40;
				tFamInfo.nThrehold_FlowRateDual = 40;

				tFamInfo.nThrehold_TankLevel = 500;
				tFamInfo.nThrehold_TankLevelLow = 200;
				tFamInfo.nThrehold_TankLevelHigh = 1200;
				tFamInfo.nThrehold_TankLevelDual = 50;

				/* Mode Pump */
				tFamInfo.nPump_TimePeri = 80;
				tFamInfo.nPump_TimeNozzleOn = 60;
				tFamInfo.nPump_TimeNozzleOff = 300;
				tFamInfo.nPump_TimePumpMix = 10;

				/* Mode Calib Ec */
				tFamInfo.nCalib_Ec1A = 560;
				tFamInfo.nCalib_Ec1B = 0;
				tFamInfo.nCalib_Ec2A = 543;
				tFamInfo.nCalib_Ec2B = 0;
				tFamInfo.nStandard_Ec = 1600;

				/* Mode Calib Water temperature*/
				tFamInfo.nCalib_Wt1A = 138;
				tFamInfo.nCalib_Wt1B = 0;
				tFamInfo.nCalib_Wt2A = 138;
				tFamInfo.nCalib_Wt2B = 0;

				/* Mode Calib Ph */
				tFamInfo.nStandard_Ph = 701;
				tFamInfo.nCalib_Ph1A = 108;
				tFamInfo.nCalib_Ph1B = 346;
				tFamInfo.nCalib_Ph2A = 108;
				tFamInfo.nCalib_Ph2B = 346;
				tFamInfo.nVoltage_Ph1_Newtral = 2000;
				tFamInfo.nVoltage_Ph2_Newtral = 2000;
				tFamInfo.nVoltage_Ph1_Acid = 500;
				tFamInfo.nVoltage_Ph2_Acid = 500;

				/* Mode Calib Climate Temp/Humi/Light */
				tFamInfo.nOffset_Cli_Temp1 = 0;
				tFamInfo.nOffset_Cli_Temp2 = 0;
				tFamInfo.nOffset_Cli_Humi1 = 0;
				tFamInfo.nOffset_Cli_Humi2 = 0;
				tFamInfo.nOffset_Cli_Light1 = 0;
				tFamInfo.nOffset_Cli_Light2 = 0;

				/* Mode Calib Tank Level */
				tFamInfo.nOffset_US1 = 0;
				tFamInfo.nOffset_US2 = 0;
				tFamInfo.nOffset_US3 = 0;
				tFamInfo.nOffset_US4 = 0;
				tFamInfo.nOffset_US5 = 0;
				tFamInfo.nOffset_US6 = 0;

				/* Mode ExIO */
				tFamInfo.nTime_NozzleOn = 8;
				tFamInfo.nTime_NozzleOff = 16;
				tFamInfo.nTime_CoopadOn = 9;
				tFamInfo.nTime_CoopadOff = 17;
				tFamInfo.nTime_ChillerOn = 9;
				tFamInfo.nTime_ChillerOff = 17;

				//			MODE_ParamSave();
				MODE_ParamsServerSave();
				//ETH_ResetTimerSysInfo();
				flagMode = MODE_DISPLAY;
				refreshLcd = true;
			}
			else
			{
				/* Mode Internet */
				tFamInfo.nTimeUpload = 600;

				/* Mode Threhold */
				tFamInfo.nThrehold_Ec = 163;
				tFamInfo.nThrehold_EcLow = 160;
				tFamInfo.nThrehold_ECHigh = 165;
				tFamInfo.nThrehold_ECDual = 20;

				tFamInfo.nThrehold_WT = 250;
				tFamInfo.nThrehold_WTLow = 200;
				tFamInfo.nThrehold_WTHigh = 280;
				tFamInfo.nThrehold_WTDual = 20;

				tFamInfo.nThrehold_PH = 610;
				tFamInfo.nThrehold_PHLow = 590;
				tFamInfo.nThrehold_PHHigh = 630;
				tFamInfo.nThrehold_PHDual = 20;

				tFamInfo.nThrehold_Cli_Temp = 280;
				tFamInfo.nThrehold_Cli_TempLow = 200;
				tFamInfo.nThrehold_Cli_TempHigh = 320;
				tFamInfo.nThrehold_Cli_TempDual = 20;

				tFamInfo.nThrehold_Cli_Humi = 700;
				tFamInfo.nThrehold_Cli_HumiLow = 600;
				tFamInfo.nThrehold_Cli_HumiHigh = 800;
				tFamInfo.nThrehold_Cli_HumiDual = 50;

				tFamInfo.nThrehold_Cli_Light = 45000;
				tFamInfo.nThrehold_Cli_LightLow = 43000;
				tFamInfo.nThrehold_Cli_LightHigh = 48000;
				tFamInfo.nThrehold_Cli_LightDual = 2000;

				tFamInfo.nThrehold_FlowRate = 40;
				tFamInfo.nThrehold_FlowRateLow = 40;
				tFamInfo.nThrehold_FlowRateHigh = 40;
				tFamInfo.nThrehold_FlowRateDual = 40;

				tFamInfo.nThrehold_TankLevel = 500;
				tFamInfo.nThrehold_TankLevelLow = 200;
				tFamInfo.nThrehold_TankLevelHigh = 1200;
				tFamInfo.nThrehold_TankLevelDual = 50;

				/* Mode Pump */
				tFamInfo.nPump_TimePeri = 80;
				tFamInfo.nPump_TimeNozzleOn = 60;
				tFamInfo.nPump_TimeNozzleOff = 300;
				tFamInfo.nPump_TimePumpMix = 10;

				/* Mode Calib Ec */
				tFamInfo.nCalib_Ec1A = 560;
				tFamInfo.nCalib_Ec1B = 0;
				tFamInfo.nCalib_Ec2A = 543;
				tFamInfo.nCalib_Ec2B = 0;
				tFamInfo.nStandard_Ec = 1600;

				/* Mode Calib Water temperature*/
				tFamInfo.nCalib_Wt1A = 138;
				tFamInfo.nCalib_Wt1B = 0;
				tFamInfo.nCalib_Wt2A = 138;
				tFamInfo.nCalib_Wt2B = 0;

				/* Mode Calib Ph */
				tFamInfo.nStandard_Ph = 701;
				tFamInfo.nCalib_Ph1A = 108;
				tFamInfo.nCalib_Ph1B = 346;
				tFamInfo.nCalib_Ph2A = 108;
				tFamInfo.nCalib_Ph2B = 346;
				tFamInfo.nVoltage_Ph1_Newtral = 2000;
				tFamInfo.nVoltage_Ph2_Newtral = 2000;
				tFamInfo.nVoltage_Ph1_Acid = 500;
				tFamInfo.nVoltage_Ph2_Acid = 500;

				/* Mode Calib Climate Temp/Humi/Light */
				tFamInfo.nOffset_Cli_Temp1 = 0;
				tFamInfo.nOffset_Cli_Temp2 = 0;
				tFamInfo.nOffset_Cli_Humi1 = 0;
				tFamInfo.nOffset_Cli_Humi2 = 0;
				tFamInfo.nOffset_Cli_Light1 = 0;
				tFamInfo.nOffset_Cli_Light2 = 0;

				/* Mode Calib Tank Level */
				tFamInfo.nOffset_US1 = 0;
				tFamInfo.nOffset_US2 = 0;
				tFamInfo.nOffset_US3 = 0;
				tFamInfo.nOffset_US4 = 0;
				tFamInfo.nOffset_US5 = 0;
				tFamInfo.nOffset_US6 = 0;

				/* Mode ExIO */
				tFamInfo.nTime_NozzleOn = 8;
				tFamInfo.nTime_NozzleOff = 16;
				tFamInfo.nTime_CoopadOn = 9;
				tFamInfo.nTime_CoopadOff = 17;
				tFamInfo.nTime_ChillerOn = 9;
				tFamInfo.nTime_ChillerOff = 17;

				//			MODE_ParamSave();
				MODE_ParamsServerSave();
				//ETH_ResetTimerSysInfo();
				flagMode = MODE_DISPLAY;
				refreshLcd = true;
			}
			break;
		case SETTING_INFO:
			switch (settingInfo)
			{
			case SETTING_INFO_NONE:
				settingInfo = SETTING_INFO_FWVERSION;
				refreshLcd = true;
				break;
			case SETTING_INFO_FWVERSION:
				break;
			case SETTING_INFO_FARMCODE:
				break;
			}
			break;
		default:
			break;
		}
		break;
	case MODE_CONFIG_SUCCESS:
		break;
	}

	return true;
}
bool HandleButtonUp(void)
{
	switch (flagMode)
	{
	case MODE_DISPLAY:
		switch (modeDisplay)
		{
		case DISPLAY_EC_PH_WT:
			modeDisplay = DISPLAY_TE_HU_LI;
			refreshLcd = true;
			break;
		case DISPLAY_TE_HU_LI:
			modeDisplay = DISPLAY_FS_US;
			refreshLcd = true;
			break;
		case DISPLAY_FS_US:
			modeDisplay = DISPLAY_EC_PH_WT;
			refreshLcd = true;
			break;
		}
		break;
	case MODE_SETTING:
		switch (modeSetting)
		{
		case SETTING_INTERNET:
			switch (settingEthernet)
			{
			case SETTING_ETHERNET_NONE:
				modeSetting = SETTING_INFO;
				refreshLcd = true;
				break;
			case SETTING_ZIGBEE_STATE:
				if (bZigbeeState == true)
				{
					bZigbeeState = false;
				}
				else
				{
					bZigbeeState = true;
				}
				break;
			case SETTING_ETHERNET_UPDATE:
				tFamInfo.nTimeUpload++;
				break;
			}
			break;
		case SETTING_THRESHOLD:
			switch (settingThreshold)
			{
			case SETTING_THRESHOLD_NONE:
				modeSetting = SETTING_INTERNET;
				break;
			case SETTING_THRESHOLD_EC:
				tFamInfo.nThrehold_Ec++;
				if (tFamInfo.nThrehold_Ec <= tFamInfo.nThrehold_EcLow){
					tFamInfo.nThrehold_Ec = tFamInfo.nThrehold_EcLow;
				}else if (tFamInfo.nThrehold_Ec >= tFamInfo.nThrehold_ECHigh){
					tFamInfo.nThrehold_Ec = tFamInfo.nThrehold_ECHigh;
				}
				break;
			case SETTING_THRESHOLD_EC_LOW:
				if (tFamInfo.nThrehold_EcLow < tFamInfo.nThrehold_ECHigh)
					tFamInfo.nThrehold_EcLow++;
				break;
			case SETTING_THRESHOLD_EC_HIGH:
				tFamInfo.nThrehold_ECHigh++;
				break;
			case SETTING_THRESHOLD_EC_DUAL:
				tFamInfo.nThrehold_ECDual++;
				break;
			case SETTING_THRESHOLD_WATERTEMP:
				tFamInfo.nThrehold_WT += 10;
				if (tFamInfo.nThrehold_WT <= tFamInfo.nThrehold_WTLow){
					tFamInfo.nThrehold_WT = tFamInfo.nThrehold_WTLow;
				}else if (tFamInfo.nThrehold_WT <= tFamInfo.nThrehold_WTHigh){
					tFamInfo.nThrehold_WT = tFamInfo.nThrehold_WTHigh;
				}
				break;
			case SETTING_THRESHOLD_WATERTEMP_LOW:
				if (tFamInfo.nThrehold_WTLow < tFamInfo.nThrehold_WTHigh)
					tFamInfo.nThrehold_WTLow += 10;
				break;
			case SETTING_THRESHOLD_WATERTEMP_HIGH:
				tFamInfo.nThrehold_WTHigh += 10;
				break;
			case SETTING_THRESHOLD_WATERTEMP_DUAL:
				tFamInfo.nThrehold_WTDual += 10;
				break;
			case SETTING_THRESHOLD_PH:
				tFamInfo.nThrehold_PH++;
				if (tFamInfo.nThrehold_PH <= tFamInfo.nThrehold_PHLow){
					tFamInfo.nThrehold_PH = tFamInfo.nThrehold_PHLow;
				}else if (tFamInfo.nThrehold_PH >= tFamInfo.nThrehold_PHHigh){
					tFamInfo.nThrehold_PH = tFamInfo.nThrehold_PHHigh;
				}
				break;
			case SETTING_THRESHOLD_PH_LOW:
				if (tFamInfo.nThrehold_PHLow < tFamInfo.nThrehold_PHHigh)
					tFamInfo.nThrehold_PHLow++;
				break;
			case SETTING_THRESHOLD_PH_HIGH:
				tFamInfo.nThrehold_PHHigh++;
				break;
			case SETTING_THRESHOLD_PH_DUAL:
				tFamInfo.nThrehold_PHDual++;
				break;
			case SETTING_THRESHOLD_TE:
				tFamInfo.nThrehold_Cli_Temp += 10;
				if (tFamInfo.nThrehold_Cli_Temp <= tFamInfo.nThrehold_Cli_TempLow){
					tFamInfo.nThrehold_Cli_Temp = tFamInfo.nThrehold_Cli_TempLow;
        }else if (tFamInfo.nThrehold_Cli_Temp >= tFamInfo.nThrehold_Cli_TempHigh){
					tFamInfo.nThrehold_Cli_Temp = tFamInfo.nThrehold_Cli_TempHigh;
				}
				break;
			case SETTING_THRESHOLD_TE_LOW:
				if (tFamInfo.nThrehold_Cli_TempLow < tFamInfo.nThrehold_Cli_TempHigh)
					tFamInfo.nThrehold_Cli_TempLow += 10;
				break;
			case SETTING_THRESHOLD_TE_HIGH:
				tFamInfo.nThrehold_Cli_TempHigh += 10;
				break;
			case SETTING_THRESHOLD_TE_DUAL:
				tFamInfo.nThrehold_Cli_TempDual += 10;
				break;
			case SETTING_THRESHOLD_HU:
				tFamInfo.nThrehold_Cli_Humi += 10;
				if (tFamInfo.nThrehold_Cli_Humi <= tFamInfo.nThrehold_Cli_HumiLow){
					tFamInfo.nThrehold_Cli_Humi = tFamInfo.nThrehold_Cli_HumiLow;
				}else if (tFamInfo.nThrehold_Cli_Humi >= tFamInfo.nThrehold_Cli_HumiHigh){
					tFamInfo.nThrehold_Cli_Humi = tFamInfo.nThrehold_Cli_HumiHigh;
				}
				break;
			case SETTING_THRESHOLD_HU_LOW:
				if (tFamInfo.nThrehold_Cli_HumiLow < tFamInfo.nThrehold_Cli_HumiHigh)
					tFamInfo.nThrehold_Cli_HumiLow += 10;
				break;
			case SETTING_THRESHOLD_HU_HIGH:
				tFamInfo.nThrehold_Cli_HumiHigh += 10;
				break;
			case SETTING_THRESHOLD_HU_DUAL:
				tFamInfo.nThrehold_Cli_HumiDual += 10;
				break;
			case SETTING_THRESHOLD_LI:
				tFamInfo.nThrehold_Cli_Light += 1000;
				if (tFamInfo.nThrehold_Cli_Light <= tFamInfo.nThrehold_Cli_LightLow){
					tFamInfo.nThrehold_Cli_Light = tFamInfo.nThrehold_Cli_LightLow;
				}else if (tFamInfo.nThrehold_Cli_Light >= tFamInfo.nThrehold_Cli_LightHigh){
					tFamInfo.nThrehold_Cli_Light = tFamInfo.nThrehold_Cli_LightHigh;
				}
				break;
			case SETTING_THRESHOLD_LI_LOW:
				if (tFamInfo.nThrehold_Cli_LightLow < tFamInfo.nThrehold_Cli_LightHigh)
					tFamInfo.nThrehold_Cli_LightLow += 1000;
				break;
			case SETTING_THRESHOLD_LI_HIGH:
				tFamInfo.nThrehold_Cli_LightHigh += 1000;
				break;
			case SETTING_THRESHOLD_LI_DUAL:
				tFamInfo.nThrehold_Cli_LightDual += 1000;
				break;
			case SETTING_THRESHOLD_FS:
				tFamInfo.nThrehold_FlowRate++;
				break;
			case SETTING_THRESHOLD_FS_LOW:
				tFamInfo.nThrehold_FlowRateLow++;
				break;
			case SETTING_THRESHOLD_FS_HIGH:
				tFamInfo.nThrehold_FlowRateHigh++;
				break;
			case SETTING_THRESHOLD_FS_DUAL:
				tFamInfo.nThrehold_FlowRateDual++;
				break;
			case SETTING_THRESHOLD_TANKLEVEL:
				tFamInfo.nThrehold_TankLevel += 10;
				break;
			case SETTING_THRESHOLD_TANKLEVEL_LOW:
				if (tFamInfo.nThrehold_TankLevelLow < tFamInfo.nThrehold_TankLevel)
					tFamInfo.nThrehold_TankLevelLow += 10;
				break;
			case SETTING_THRESHOLD_TANKLEVEL_HIGH:
				tFamInfo.nThrehold_TankLevelHigh += 10;
				break;
			case SETTING_THRESHOLD_TANKLEVEL_DUAL:
				tFamInfo.nThrehold_TankLevelDual += 10;
				break;
			}
			break;
		case SETTING_PUMP:
			switch (settingPump)
			{
			case SETTING_PUMP_NONE:
				modeSetting = SETTING_THRESHOLD;
				break;
			case SETTING_TIME_PUMP_PERI:
				tFamInfo.nPump_TimePeri++;
				break;
			case SETTING_TIME_PUMP_MIX:
				tFamInfo.nPump_TimePumpMix++;
				break;
			case SETTING_TIME_PUMP_NOZZLE:
				tFamInfo.nPump_TimeNozzleOn++;
				break;
			case SETTING_TIME_PUMP_NOZZLE_WAIT:
				tFamInfo.nPump_TimeNozzleOff++;
				break;
			}
			break;
		case SETTING_CALIB_EC:
			switch (settingCalibEC)
			{
			case SETTING_CALIB_EC_NONE:
				modeSetting = SETTING_PUMP;
				refreshLcd = true;
				break;
			case SETTING_EC_STANDARD:
				tFamInfo.nStandard_Ec++;
				if (tFamInfo.nStandard_Ec <= 0){
					tFamInfo.nStandard_Ec = 0;
				}else if (tFamInfo.nStandard_Ec >= 5000){
					tFamInfo.nStandard_Ec = 5000;
				}
//				switch (tFamInfo.nStandard_Ec)
//				{
//				case 1000:
//					tFamInfo.nStandard_Ec = 1413;
//					break;
//				case 1413:
//					tFamInfo.nStandard_Ec = 2000;
//					break;
//				case 2000:
//					tFamInfo.nStandard_Ec = 5000;
//					break;
//				case 5000:
//					tFamInfo.nStandard_Ec = 1000;
//					break;
//				default:
//					break;
//				}
				break;
			case SETTING_CLICK_CALIB_EC:
				SENSOR_CalibEc(tFamInfo.nStandard_Ec, &tFamInfo.nCalib_Ec1A, &tFamInfo.nCalib_Ec2A);
				break;
			case SETTING_CALIB_EC1_A:
				tFamInfo.nCalib_Ec1A++;
				break;
			case SETTING_CALIB_EC1_B:
				tFamInfo.nCalib_Ec1B++;
				break;
			case SETTING_CALIB_EC2_A:
				tFamInfo.nCalib_Ec2A++;
				break;
			case SETTING_CALIB_EC2_B:
				tFamInfo.nCalib_Ec2B++;
				break;
			}
			break;
		case SETTING_CALIB_WT:
			switch (settingCalibWT)
			{
			case SETTING_CALIB_WT_NONE:
				modeSetting = SETTING_CALIB_EC;
				break;
			case SETTING_WT_STANDARD:
				fWaterTempStandard += 0.1;
				break;
			case SETTING_CLICK_CALIB_WT:
				SENSOR_CalibWt(fWaterTempStandard);
				break;
			}
			break;
		case SETTING_CALIB_PH:
			switch (settingCalibPH)
			{
			case SETTING_CALIB_PH_NONE:
				modeSetting = SETTING_CALIB_WT;
				break;
			case SETTING_PH_STANDARD:
				switch (tFamInfo.nStandard_Ph)
				{
				case 401:
					tFamInfo.nStandard_Ph = 701;
					break;
				case 701:
					tFamInfo.nStandard_Ph = 401;
					break;
				default:
					break;
				}
				break;
			case SETTING_CLICK_CALIB_PH:
				SENSOR_CalibPh(tFamInfo.nStandard_Ph, &tFamInfo.nCalib_Ph1A, &tFamInfo.nCalib_Ph1B, &tFamInfo.nCalib_Ph2A, &tFamInfo.nCalib_Ph2B);
				break;
			case SETTING_CALIB_PH1_A:
				tFamInfo.nCalib_Ph1A++;
				break;
			case SETTING_CALIB_PH1_B:
				tFamInfo.nCalib_Ph1B++;
				break;
			case SETTING_CALIB_PH2_A:
				tFamInfo.nCalib_Ph2A++;
				break;
			case SETTING_CALIB_PH2_B:
				tFamInfo.nCalib_Ph2B++;
				break;
			}
			break;
		case SETTING_CALIB_TE:
			switch (settingCalibTE)
			{
			case SETTING_CALIB_TE_NONE:
				modeSetting = SETTING_CALIB_PH;
				refreshLcd = true;
				break;
			case SETTING_CALIB_TE_STAND:
				fClimateTempStandard += 0.1;
				SENSOR_CalibTemp(fClimateTempStandard);
				break;
			}
			break;
		case SETTING_CALIB_HU:
			switch (settingCalibHU)
			{
			case SETTING_CALIB_HU_NONE:
				modeSetting = SETTING_CALIB_TE;
				break;
			case SETTING_CALIB_HU_STAND:
				fClimateHumiStandard += 1;
				SENSOR_CalibHumi(fClimateHumiStandard);
				break;
			}
			break;
		case SETTING_CALIB_LI:
			switch (settingCalibLI)
			{
			case SETTING_CALIB_LI_NONE:
				modeSetting = SETTING_CALIB_HU;
				break;
			case SETTING_CALIB_LI_STAND:
				nClimateLightStandard += 100;
				SENSOR_CalibLight(nClimateLightStandard);
				break;
			}
			break;
		case SETTING_CALIB_FS:
			switch (settingCalibFS)
			{
			case SETTING_CALIB_FS_NONE:
				modeSetting = SETTING_CALIB_LI;
				refreshLcd = true;
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_1:
			switch (settingCalibTankLevel1)
			{
			case SETTING_CALIB_TANK_LEVEL_1_NONE:
				modeSetting = SETTING_CALIB_FS;
				break;
			case SETTING_CALIB_TANK_LEVEL_1_STAND:
				nTankLevelStandard1 += 1;
				HCSR04_calibTank1(nTankLevelStandard1);
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_2:
			switch (settingCalibTankLevel2)
			{
			case SETTING_CALIB_TANK_LEVEL_2_NONE:
				modeSetting = SETTING_CALIB_TANK_LEVEL_1;
				break;
			case SETTING_CALIB_TANK_LEVEL_2_STAND:
				nTankLevelStandard2 += 1;
				HCSR04_calibTank2(nTankLevelStandard2);
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_3:
			switch (settingCalibTankLevel3)
			{
			case SETTING_CALIB_TANK_LEVEL_3_NONE:
				modeSetting = SETTING_CALIB_TANK_LEVEL_2;
				refreshLcd = true;
				break;
			case SETTING_CALIB_TANK_LEVEL_3_STAND:
				nTankLevelStandard3 += 1;
				HCSR04_calibTank3(nTankLevelStandard3);
				break;
			}
			break;
		case SETTING_EXIO:
			switch (settingExIO)
			{
			case SETTING_EXIO_NONE:
				modeSetting = SETTING_CALIB_TANK_LEVEL_3;
				break;
			case SETTING_EXIO_NOZZLE_TIME_ON:
				tFamInfo.nTime_NozzleOn++;
				if (tFamInfo.nTime_NozzleOn >= 12)
					tFamInfo.nTime_NozzleOn = 12;
				else if (tFamInfo.nTime_NozzleOn <= 0)
					tFamInfo.nTime_NozzleOn = 0;
				break;
			case SETTING_EXIO_NOZZLE_TIME_OFF:
				tFamInfo.nTime_NozzleOff++;
				if (tFamInfo.nTime_NozzleOff >= 23)
					tFamInfo.nTime_NozzleOff = 23;
				else if (tFamInfo.nTime_NozzleOff <= 12)
					tFamInfo.nTime_NozzleOff = 12;
				break;
			case SETTING_EXIO_COOPAD_TIME_ON:
				tFamInfo.nTime_CoopadOn++;
				if (tFamInfo.nTime_CoopadOn >= 12)
					tFamInfo.nTime_CoopadOn = 12;
				else if (tFamInfo.nTime_CoopadOn <= 0)
					tFamInfo.nTime_CoopadOn = 0;
				break;
			case SETTING_EXIO_COOPAD_TIME_OFF:
				tFamInfo.nTime_CoopadOff++;
				if (tFamInfo.nTime_CoopadOff >= 23)
					tFamInfo.nTime_CoopadOff = 23;
				else if (tFamInfo.nTime_CoopadOff <= 12)
					tFamInfo.nTime_CoopadOff = 12;
				break;
			}
			break;
		case SETTING_RTC:
			switch (settingRtc)
			{
			case SETTING_RTC_NONE:
				modeSetting = SETTING_EXIO;
				break;
			case SETTING_RTC_HOUR:
				//Increase Hour
				tRtc.Hour++;
				if (tRtc.Hour > 23)
					tRtc.Hour = 0;
				else if (tRtc.Hour < 0)
					tRtc.Hour = 23;
				break;
			case SETTING_RTC_MINUTE:
				//Increase Minute
				tRtc.Min++;
				if (tRtc.Min > 59)
					tRtc.Min = 0;
				else if (tRtc.Min < 0)
					tRtc.Min = 59;
				break;
			}
			break;
		case SETTING_ENABLE:
			/*
					O1 - PD0 - OP_SOLENOID_NUTRI
					O2 - PD1 - OP_SOLENOID_WATER
					O3 - PD2 - OP_PMIX
					O4 - PD3 - OP_NOZZLE
					O5 - PD4 - OP_COOLINGPAD
					O6 - PD5 - OP_FAN
					O7 - PB9 - ACT4
					O8 - PE1 - ACT3
					O9 - PE0 - ACT2
					O10 - PD6 - ACT1
				*/
			switch (settingEnable)
			{
			case SETTING_ENABLE_NONE:
				modeSetting = SETTING_RTC;
				refreshLcd = true;
				break;
			case SETTING_ENABLE_EC:
				HAL_GPIO_TogglePin(OP_EC_GPIO_Port, OP_EC_Pin);
				break;
			case SETTING_ENABLE_PH:
				HAL_GPIO_TogglePin(OP_PH_GPIO_Port, OP_PH_Pin);
				break;
			case SETTING_ENABLE_O1:
				HAL_GPIO_TogglePin(OP_O1_GPIO_Port, OP_O1_Pin);
				break;
			case SETTING_ENABLE_O2:
				HAL_GPIO_TogglePin(OP_O2_GPIO_Port, OP_O2_Pin);
				break;
			case SETTING_ENABLE_O3:
				HAL_GPIO_TogglePin(OP_O3_GPIO_Port, OP_O3_Pin);
				break;
			case SETTING_ENABLE_O4:
				HAL_GPIO_TogglePin(OP_O4_GPIO_Port, OP_O4_Pin);
				break;
			case SETTING_ENABLE_O5:
				HAL_GPIO_TogglePin(OP_O5_GPIO_Port, OP_O5_Pin);
				break;
			case SETTING_ENABLE_O6:
				HAL_GPIO_TogglePin(OP_O6_GPIO_Port, OP_O6_Pin);
				break;
			case SETTING_ENABLE_O7:
				HAL_GPIO_TogglePin(OP_O7_GPIO_Port, OP_O7_Pin);
				break;
			case SETTING_ENABLE_O8:
				HAL_GPIO_TogglePin(OP_O8_GPIO_Port, OP_O8_Pin);
				break;
			case SETTING_ENABLE_O9:
				HAL_GPIO_TogglePin(OP_O9_GPIO_Port, OP_O9_Pin);
				break;
			case SETTING_ENABLE_O10:
				HAL_GPIO_TogglePin(OP_O10_GPIO_Port, OP_O10_Pin);
				break;
			default:
				break;
			}
			break;
		case SETTING_DEFAULT:
			modeSetting = SETTING_ENABLE;
			break;
		case SETTING_INFO:
			switch (settingInfo)
			{
			case SETTING_INFO_NONE:
				modeSetting = SETTING_DEFAULT;
				break;
			case SETTING_INFO_FWVERSION:
				break;
			case SETTING_INFO_FARMCODE:
				break;
			}
			break;
		}
		break;
	case MODE_CONFIG_SUCCESS:
		break;
	}

	return true;
}
bool HandleButtonDown(void)
{
	switch (flagMode)
	{
	case MODE_DISPLAY:
		switch (modeDisplay)
		{
		case DISPLAY_EC_PH_WT:
			modeDisplay = DISPLAY_FS_US;
			refreshLcd = true;
			break;
		case DISPLAY_TE_HU_LI:
			modeDisplay = DISPLAY_EC_PH_WT;
			refreshLcd = true;
			break;
		case DISPLAY_FS_US:
			modeDisplay = DISPLAY_TE_HU_LI;
			refreshLcd = true;
			break;
		}
		break;
	case MODE_SETTING:
		switch (modeSetting)
		{
		case SETTING_INTERNET:
			switch (settingEthernet)
			{
			case SETTING_ETHERNET_NONE:
				modeSetting = SETTING_THRESHOLD;
				break;
			case SETTING_ZIGBEE_STATE:
				if (bZigbeeState == true)
				{
					bZigbeeState = false;
				}
				else
				{
					bZigbeeState = true;
				}
				break;
			case SETTING_ETHERNET_UPDATE:
				if (tFamInfo.nTimeUpload > 1)
					tFamInfo.nTimeUpload--;
				break;
			}
			break;
		case SETTING_THRESHOLD:
			switch (settingThreshold)
			{
			case SETTING_THRESHOLD_NONE:
				modeSetting = SETTING_PUMP;
				break;
			case SETTING_THRESHOLD_EC:
				tFamInfo.nThrehold_Ec--;
				if (tFamInfo.nThrehold_Ec <= tFamInfo.nThrehold_EcLow){
					tFamInfo.nThrehold_Ec = tFamInfo.nThrehold_EcLow;
				}else if (tFamInfo.nThrehold_Ec >= tFamInfo.nThrehold_ECHigh){
					tFamInfo.nThrehold_Ec = tFamInfo.nThrehold_ECHigh;
				}
				break;
			case SETTING_THRESHOLD_EC_LOW:
				if (tFamInfo.nThrehold_EcLow > 1)
					tFamInfo.nThrehold_EcLow--;
				break;
			case SETTING_THRESHOLD_EC_HIGH:
				if (tFamInfo.nThrehold_ECHigh > tFamInfo.nThrehold_EcLow)
					tFamInfo.nThrehold_ECHigh--;
				break;
			case SETTING_THRESHOLD_EC_DUAL:
				if (tFamInfo.nThrehold_ECDual > 1)
					tFamInfo.nThrehold_ECDual--;
				break;
			case SETTING_THRESHOLD_WATERTEMP:
				tFamInfo.nThrehold_WT -= 10;
				if (tFamInfo.nThrehold_WT <= tFamInfo.nThrehold_WTLow){
					tFamInfo.nThrehold_WT = tFamInfo.nThrehold_WTLow;
				}else if (tFamInfo.nThrehold_WT >= tFamInfo.nThrehold_WTHigh){
					tFamInfo.nThrehold_WT = tFamInfo.nThrehold_WTHigh;
				}
				break;
			case SETTING_THRESHOLD_WATERTEMP_LOW:
				if (tFamInfo.nThrehold_WTLow > 1)
					tFamInfo.nThrehold_WTLow -= 10;
				break;
			case SETTING_THRESHOLD_WATERTEMP_HIGH:
				if (tFamInfo.nThrehold_WTHigh > tFamInfo.nThrehold_WTLow)
					tFamInfo.nThrehold_WTHigh -= 10;
				break;
			case SETTING_THRESHOLD_WATERTEMP_DUAL:
				if (tFamInfo.nThrehold_WTDual > 1)
					tFamInfo.nThrehold_WTDual -= 10;
				break;
			case SETTING_THRESHOLD_PH:
				tFamInfo.nThrehold_PH--;
				if (tFamInfo.nThrehold_PH <= tFamInfo.nThrehold_PHLow){
					tFamInfo.nThrehold_PH = tFamInfo.nThrehold_PHLow;
				}else if (tFamInfo.nThrehold_PH >= tFamInfo.nThrehold_PHHigh){
					tFamInfo.nThrehold_PH = tFamInfo.nThrehold_PHHigh;
				}
				break;
			case SETTING_THRESHOLD_PH_LOW:
				if (tFamInfo.nThrehold_PHLow > 1)
					tFamInfo.nThrehold_PHLow--;
				break;
			case SETTING_THRESHOLD_PH_HIGH:
				if (tFamInfo.nThrehold_PHHigh > tFamInfo.nThrehold_PHLow)
					tFamInfo.nThrehold_PHHigh--;
				break;
			case SETTING_THRESHOLD_PH_DUAL:
				if (tFamInfo.nThrehold_PHDual > 1)
					tFamInfo.nThrehold_PHDual--;
				break;
			case SETTING_THRESHOLD_TE:
				tFamInfo.nThrehold_Cli_Temp -= 10;
				if (tFamInfo.nThrehold_Cli_Temp <= tFamInfo.nThrehold_Cli_TempLow){
					tFamInfo.nThrehold_Cli_Temp = tFamInfo.nThrehold_Cli_TempLow;
				}else if (tFamInfo.nThrehold_Cli_Temp >= tFamInfo.nThrehold_Cli_TempHigh){
					tFamInfo.nThrehold_Cli_Temp = tFamInfo.nThrehold_Cli_TempHigh;
				}
				break;
			case SETTING_THRESHOLD_TE_LOW:
				if (tFamInfo.nThrehold_Cli_TempLow > 1)
					tFamInfo.nThrehold_Cli_TempLow -= 10;
				break;
			case SETTING_THRESHOLD_TE_HIGH:
				if (tFamInfo.nThrehold_Cli_TempHigh > tFamInfo.nThrehold_Cli_TempLow)
					tFamInfo.nThrehold_Cli_TempHigh -= 10;
				break;
			case SETTING_THRESHOLD_TE_DUAL:
				if (tFamInfo.nThrehold_Cli_TempDual > 1)
					tFamInfo.nThrehold_Cli_TempDual -= 10;
				break;
			case SETTING_THRESHOLD_HU:
				tFamInfo.nThrehold_Cli_Humi -= 10;
				if (tFamInfo.nThrehold_Cli_Humi <= tFamInfo.nThrehold_Cli_HumiLow){
					tFamInfo.nThrehold_Cli_Humi = tFamInfo.nThrehold_Cli_HumiLow;
				}else if (tFamInfo.nThrehold_Cli_Humi >= tFamInfo.nThrehold_Cli_HumiHigh){
					tFamInfo.nThrehold_Cli_Humi = tFamInfo.nThrehold_Cli_HumiHigh;
				}
				break;
			case SETTING_THRESHOLD_HU_LOW:
				if (tFamInfo.nThrehold_Cli_HumiLow > 1)
					tFamInfo.nThrehold_Cli_HumiLow -= 10;
				break;
			case SETTING_THRESHOLD_HU_HIGH:
				if (tFamInfo.nThrehold_Cli_HumiHigh > tFamInfo.nThrehold_Cli_HumiLow)
					tFamInfo.nThrehold_Cli_HumiHigh -= 10;
				break;
			case SETTING_THRESHOLD_HU_DUAL:
				if (tFamInfo.nThrehold_Cli_HumiDual > 1)
					tFamInfo.nThrehold_Cli_HumiDual -= 10;
				break;
			case SETTING_THRESHOLD_LI:
				tFamInfo.nThrehold_Cli_Light -= 1000;
				if (tFamInfo.nThrehold_Cli_Light <= tFamInfo.nThrehold_Cli_LightLow){
					tFamInfo.nThrehold_Cli_Light = tFamInfo.nThrehold_Cli_LightLow;
				}else if (tFamInfo.nThrehold_Cli_Light >= tFamInfo.nThrehold_Cli_LightHigh){
					tFamInfo.nThrehold_Cli_Light = tFamInfo.nThrehold_Cli_LightHigh;
				}
				break;
			case SETTING_THRESHOLD_LI_LOW:
				if (tFamInfo.nThrehold_Cli_LightLow > 1)
					tFamInfo.nThrehold_Cli_LightLow -= 1000;
				break;
			case SETTING_THRESHOLD_LI_HIGH:
				if (tFamInfo.nThrehold_Cli_LightHigh > tFamInfo.nThrehold_Cli_LightLow)
					tFamInfo.nThrehold_Cli_LightHigh -= 1000;
				break;
			case SETTING_THRESHOLD_LI_DUAL:
				if (tFamInfo.nThrehold_Cli_LightDual > 1)
					tFamInfo.nThrehold_Cli_LightDual -= 1000;
				break;
			case SETTING_THRESHOLD_FS:
				if (tFamInfo.nThrehold_FlowRate > 1)
					tFamInfo.nThrehold_FlowRate--;
				break;
			case SETTING_THRESHOLD_FS_LOW:
				if (tFamInfo.nThrehold_FlowRateLow > 1)
					tFamInfo.nThrehold_FlowRateLow--;
				break;
			case SETTING_THRESHOLD_FS_HIGH:
				if (tFamInfo.nThrehold_FlowRateHigh > 1)
					tFamInfo.nThrehold_FlowRateHigh--;
				break;
			case SETTING_THRESHOLD_FS_DUAL:
				if (tFamInfo.nThrehold_FlowRateDual > 1)
					tFamInfo.nThrehold_FlowRateDual--;
				break;
			case SETTING_THRESHOLD_TANKLEVEL:
				if (tFamInfo.nThrehold_TankLevel > 1)
					tFamInfo.nThrehold_TankLevel -= 10;
				break;
			case SETTING_THRESHOLD_TANKLEVEL_LOW:
				if (tFamInfo.nThrehold_TankLevelLow > 1)
					tFamInfo.nThrehold_TankLevelLow -= 10;
				break;
			case SETTING_THRESHOLD_TANKLEVEL_HIGH:
				if (tFamInfo.nThrehold_TankLevelHigh > 1)
					tFamInfo.nThrehold_TankLevelHigh -= 10;
				break;
			case SETTING_THRESHOLD_TANKLEVEL_DUAL:
				if (tFamInfo.nThrehold_TankLevelDual > 1)
					tFamInfo.nThrehold_TankLevelDual -= 10;
				break;
			}
			break;
		case SETTING_PUMP:
			switch (settingPump)
			{
			case SETTING_PUMP_NONE:
				modeSetting = SETTING_CALIB_EC;
				break;
			case SETTING_TIME_PUMP_PERI:
				if (tFamInfo.nPump_TimePeri > 1)
					tFamInfo.nPump_TimePeri--;
				break;
			case SETTING_TIME_PUMP_MIX:
				if (tFamInfo.nPump_TimePumpMix > 1)
					tFamInfo.nPump_TimePumpMix--;
				break;
			case SETTING_TIME_PUMP_NOZZLE_WAIT:
				if (tFamInfo.nPump_TimeNozzleOff > 1)
					tFamInfo.nPump_TimeNozzleOff--;
				break;
			case SETTING_TIME_PUMP_NOZZLE:
				if (tFamInfo.nPump_TimeNozzleOn > 1)
					tFamInfo.nPump_TimeNozzleOn--;
				break;
			}
			break;
		case SETTING_CALIB_EC:
			switch (settingCalibEC)
			{
			case SETTING_CALIB_EC_NONE:
				modeSetting = SETTING_CALIB_WT;
				break;
			case SETTING_EC_STANDARD:
				tFamInfo.nStandard_Ec--;
				if (tFamInfo.nStandard_Ec <= 0){
					tFamInfo.nStandard_Ec = 0;
				}else if (tFamInfo.nStandard_Ec >= 5000){
					tFamInfo.nStandard_Ec = 5000;
				}
//				switch (tFamInfo.nStandard_Ec)
//				{
//				case 1000:
//					tFamInfo.nStandard_Ec = 5000;
//					break;
//				case 1413:
//					tFamInfo.nStandard_Ec = 1000;
//					break;
//				case 2000:
//					tFamInfo.nStandard_Ec = 1413;
//					break;
//				case 5000:
//					tFamInfo.nStandard_Ec = 2000;
//					break;
//				default:
//					break;
//				}
				break;
			case SETTING_CLICK_CALIB_EC:
				SENSOR_CalibEc(tFamInfo.nStandard_Ec, &tFamInfo.nCalib_Ec1A, &tFamInfo.nCalib_Ec2A);
				break;
			case SETTING_CALIB_EC1_A:
				if (tFamInfo.nCalib_Ec1A > 1)
					tFamInfo.nCalib_Ec1A--;
				break;
			case SETTING_CALIB_EC1_B:
				if (tFamInfo.nCalib_Ec1B > 1)
					tFamInfo.nCalib_Ec1B--;
				break;
			case SETTING_CALIB_EC2_A:
				if (tFamInfo.nCalib_Ec2A > 1)
					tFamInfo.nCalib_Ec2A--;
				break;
			case SETTING_CALIB_EC2_B:
				if (tFamInfo.nCalib_Ec2B > 1)
					tFamInfo.nCalib_Ec2B--;
				break;
			}
			break;
		case SETTING_CALIB_WT:
			switch (settingCalibWT)
			{
			case SETTING_CALIB_WT_NONE:
				modeSetting = SETTING_CALIB_PH;
				break;
			case SETTING_WT_STANDARD:
				fWaterTempStandard -= 0.1;
				break;
			case SETTING_CLICK_CALIB_WT:
				SENSOR_CalibWt(fWaterTempStandard);
				break;
			}
			break;
		case SETTING_CALIB_PH:
			switch (settingCalibPH)
			{
			case SETTING_CALIB_PH_NONE:
				modeSetting = SETTING_CALIB_TE;
				refreshLcd = true;
				break;
			case SETTING_PH_STANDARD:
				switch (tFamInfo.nStandard_Ph)
				{
				case 401:
					tFamInfo.nStandard_Ph = 701;
					break;
				case 701:
					tFamInfo.nStandard_Ph = 401;
					break;
				default:
					break;
				}
				break;
			case SETTING_CLICK_CALIB_PH:
				SENSOR_CalibPh(tFamInfo.nStandard_Ph, &tFamInfo.nCalib_Ph1A, &tFamInfo.nCalib_Ph1B, &tFamInfo.nCalib_Ph2A, &tFamInfo.nCalib_Ph2B);
				break;
			case SETTING_CALIB_PH1_A:
				if (tFamInfo.nCalib_Ph1A > 1)
					tFamInfo.nCalib_Ph1A--;
				break;
			case SETTING_CALIB_PH1_B:
				if (tFamInfo.nCalib_Ph1B > 1)
					tFamInfo.nCalib_Ph1B--;
				break;
			case SETTING_CALIB_PH2_A:
				if (tFamInfo.nCalib_Ph2A > 1)
					tFamInfo.nCalib_Ph2A--;
				break;
			case SETTING_CALIB_PH2_B:
				if (tFamInfo.nCalib_Ph2B < 1)
					tFamInfo.nCalib_Ph2B--;
			default:
				break;
			}
			break;
		case SETTING_CALIB_TE:
			switch (settingCalibTE)
			{
			case SETTING_CALIB_TE_NONE:
				modeSetting = SETTING_CALIB_HU;
				break;
			case SETTING_CALIB_TE_STAND:
				fClimateTempStandard -= 0.1;
				SENSOR_CalibTemp(fClimateTempStandard);
				break;
			}
			break;
		case SETTING_CALIB_HU:
			switch (settingCalibHU)
			{
			case SETTING_CALIB_HU_NONE:
				modeSetting = SETTING_CALIB_LI;
				break;
			case SETTING_CALIB_HU_STAND:
				fClimateHumiStandard -= 1;
				SENSOR_CalibHumi(fClimateHumiStandard);
				break;
			}
			break;
		case SETTING_CALIB_LI:
			switch (settingCalibLI)
			{
			case SETTING_CALIB_LI_NONE:
				modeSetting = SETTING_CALIB_FS;
				refreshLcd = true;
				break;
			case SETTING_CALIB_LI_STAND:
				nClimateLightStandard -= 100;
				SENSOR_CalibLight(nClimateLightStandard);
				break;
			}
			break;
		case SETTING_CALIB_FS:
			switch (settingCalibFS)
			{
			case SETTING_CALIB_FS_NONE:
				modeSetting = SETTING_CALIB_TANK_LEVEL_1;
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_1:
			switch (settingCalibTankLevel1)
			{
			case SETTING_CALIB_TANK_LEVEL_1_NONE:
				modeSetting = SETTING_CALIB_TANK_LEVEL_2;
				break;
			case SETTING_CALIB_TANK_LEVEL_1_STAND:
				nTankLevelStandard1 -= 1;
				HCSR04_calibTank1(nTankLevelStandard1);
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_2:
			switch (settingCalibTankLevel2)
			{
			case SETTING_CALIB_TANK_LEVEL_2_NONE:
				modeSetting = SETTING_CALIB_TANK_LEVEL_3;
				refreshLcd = true;
				break;
			case SETTING_CALIB_TANK_LEVEL_2_STAND:
				nTankLevelStandard2 -= 1;
				HCSR04_calibTank2(nTankLevelStandard2);
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_3:
			switch (settingCalibTankLevel3)
			{
			case SETTING_CALIB_TANK_LEVEL_3_NONE:
				modeSetting = SETTING_EXIO;
				break;
			case SETTING_CALIB_TANK_LEVEL_3_STAND:
				nTankLevelStandard3 -= 1;
				HCSR04_calibTank3(nTankLevelStandard3);
				break;
			}
			break;
		case SETTING_EXIO:
			switch (settingExIO)
			{
			case SETTING_EXIO_NONE:
				modeSetting = SETTING_RTC;
				break;
			case SETTING_EXIO_NOZZLE_TIME_ON:
				tFamInfo.nTime_NozzleOn--;
				if (tFamInfo.nTime_NozzleOn >= 12)
					tFamInfo.nTime_NozzleOn = 12;
				else if (tFamInfo.nTime_NozzleOn <= 0)
					tFamInfo.nTime_NozzleOn = 0;
				break;
			case SETTING_EXIO_NOZZLE_TIME_OFF:
				tFamInfo.nTime_NozzleOff--;
				if (tFamInfo.nTime_NozzleOff >= 23)
					tFamInfo.nTime_NozzleOff = 23;
				else if (tFamInfo.nTime_NozzleOff <= 12)
					tFamInfo.nTime_NozzleOff = 12;
				break;
			case SETTING_EXIO_COOPAD_TIME_ON:
				tFamInfo.nTime_CoopadOn--;
				if (tFamInfo.nTime_CoopadOn >= 12)
					tFamInfo.nTime_CoopadOn = 12;
				else if (tFamInfo.nTime_CoopadOn <= 0)
					tFamInfo.nTime_CoopadOn = 0;
				break;
			case SETTING_EXIO_COOPAD_TIME_OFF:
				tFamInfo.nTime_CoopadOff--;
				if (tFamInfo.nTime_CoopadOff >= 23)
					tFamInfo.nTime_CoopadOff = 23;
				else if (tFamInfo.nTime_CoopadOff <= 12)
					tFamInfo.nTime_CoopadOff = 12;
				break;
			}
			break;
		case SETTING_RTC:
			switch (settingRtc)
			{
			case SETTING_RTC_NONE:
				modeSetting = SETTING_ENABLE;
				refreshLcd = true;
				break;
			case SETTING_RTC_HOUR:
				//Increase Hour
				tRtc.Hour--;
				if (tRtc.Hour > 23)
					tRtc.Hour = 0;
				else if (tRtc.Hour < 0)
					tRtc.Hour = 23;
				break;
			case SETTING_RTC_MINUTE:
				//Increase Minute
				tRtc.Min--;
				if (tRtc.Min > 59)
					tRtc.Min = 0;
				else if (tRtc.Min < 0)
					tRtc.Min = 59;
				break;
			}
			break;
		case SETTING_ENABLE:
			switch (settingEnable)
			{
			case SETTING_ENABLE_NONE:
				modeSetting = SETTING_DEFAULT;
				break;
			case SETTING_ENABLE_EC:
				HAL_GPIO_TogglePin(OP_EC_GPIO_Port, OP_EC_Pin);
				break;
			case SETTING_ENABLE_PH:
				HAL_GPIO_TogglePin(OP_PH_GPIO_Port, OP_PH_Pin);
				break;
			case SETTING_ENABLE_O1:
				HAL_GPIO_TogglePin(OP_O1_GPIO_Port, OP_O1_Pin);
				break;
			case SETTING_ENABLE_O2:
				HAL_GPIO_TogglePin(OP_O2_GPIO_Port, OP_O2_Pin);
				break;
			case SETTING_ENABLE_O3:
				HAL_GPIO_TogglePin(OP_O3_GPIO_Port, OP_O3_Pin);
				break;
			case SETTING_ENABLE_O4:
				HAL_GPIO_TogglePin(OP_O4_GPIO_Port, OP_O4_Pin);
				break;
			case SETTING_ENABLE_O5:
				HAL_GPIO_TogglePin(OP_O5_GPIO_Port, OP_O5_Pin);
				break;
			case SETTING_ENABLE_O6:
				HAL_GPIO_TogglePin(OP_O6_GPIO_Port, OP_O6_Pin);
				break;
			case SETTING_ENABLE_O7:
				HAL_GPIO_TogglePin(OP_O7_GPIO_Port, OP_O7_Pin);
				break;
			case SETTING_ENABLE_O8:
				HAL_GPIO_TogglePin(OP_O8_GPIO_Port, OP_O8_Pin);
				break;
			case SETTING_ENABLE_O9:
				HAL_GPIO_TogglePin(OP_O9_GPIO_Port, OP_O9_Pin);
				break;
			case SETTING_ENABLE_O10:
				HAL_GPIO_TogglePin(OP_O10_GPIO_Port, OP_O10_Pin);
				break;
			default:
				break;
			}
			break;
		case SETTING_DEFAULT:
			modeSetting = SETTING_INFO;
			break;
		case SETTING_INFO:
			switch (settingInfo)
			{
			case SETTING_INFO_NONE:
				modeSetting = SETTING_INTERNET;
				refreshLcd = true;
				break;
			case SETTING_INFO_FWVERSION:
				break;
			case SETTING_INFO_FARMCODE:
				break;
			}
			break;
		}
		break;
	case MODE_CONFIG_SUCCESS:

		break;
	}

	return true;
}

/* load params */
bool MODE_ParamsServerLoad(void)
{
	int index = 0;

	HAL_FLASH_Unlock();

	FLASH_ReadString(EEPROM_START_ADDRESS + (uint32_t)(index), (__IO uint32_t *)tFamInfo.cFarmCode);
	index += 20;
	FLASH_ReadString(EEPROM_START_ADDRESS + (uint32_t)(index), (__IO uint32_t *)tFamInfo.cFarmName);
	index += 12;
	FLASH_ReadString(EEPROM_START_ADDRESS + (uint32_t)(index), (__IO uint32_t *)tFamInfo.cFwVersion);
	index += 12;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nDateTime_Hour);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nDateTime_Minute);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nTimeUpload);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Ec);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_EcLow);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_ECHigh);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_ECDual);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_WT);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_WTLow);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_WTHigh);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_WTDual);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_PH);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_PHLow);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_PHHigh);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_PHDual);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_Temp);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_TempLow);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_TempHigh);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_TempDual);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_Humi);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_HumiLow);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_HumiHigh);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_HumiDual);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_Light);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_LightLow);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_LightHigh);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_LightDual);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_FlowRate);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_FlowRateLow);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_FlowRateHigh);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_FlowRateDual);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_TankLevel);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_TankLevelLow);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_TankLevelHigh);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_TankLevelDual);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nPump_TimePeri);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nPump_TimeNozzleOn);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nPump_TimeNozzleOff);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nPump_TimePumpMix);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Ec1A);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Ec1B);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Ec2A);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Ec2B);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Wt1A);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Wt1B);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Wt2A);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Wt2B);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Ph1A);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Ph1B);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Ph2A);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Ph2B);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_Cli_Temp1);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_Cli_Temp2);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_Cli_Humi1);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_Cli_Humi2);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_Cli_Light1);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_Cli_Light2);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_US1);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_US2);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_US3);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_US4);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_US5);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_US6);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nVoltage_Ph1_Newtral);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nVoltage_Ph1_Acid);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nVoltage_Ph2_Newtral);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nVoltage_Ph2_Acid);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nStandard_Ec);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nStandard_Ph);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nTime_NozzleOn);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nTime_NozzleOff);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nTime_CoopadOn);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nTime_CoopadOff);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nTime_ChillerOn);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nTime_ChillerOff);

	HAL_FLASH_Lock();

	return true;
}

bool MODE_ParamsServerUpdate(void)
{
	sprintf(tFamInfo.cFarmCode, "%s", m_value[0].value);
	// sprintf(tFamInfo.cFarmName, "%s", m_value[1].value);
	// sprintf(tFamInfo.cFwVersion, "%s", m_value[2].value);
	strncpy(tFamInfo.cFarmName, m_value[1].value, 10);
	strncpy(tFamInfo.cFwVersion, m_value[2].value, 5);
	sprintf(tFamInfo.cDateTime, "%s", m_value[3].value);
	// implement parse datetime to hour, minute

	tFamInfo.nTimeUpload = atoi(m_value[4].value);
	tFamInfo.nThrehold_Ec = atoi(m_value[5].value);
	tFamInfo.nThrehold_EcLow = atoi(m_value[6].value);
	tFamInfo.nThrehold_ECHigh = atoi(m_value[7].value);
	tFamInfo.nThrehold_ECDual = atoi(m_value[8].value);
	tFamInfo.nThrehold_WT = atoi(m_value[9].value);
	tFamInfo.nThrehold_WTLow = atoi(m_value[10].value);
	tFamInfo.nThrehold_WTHigh = atoi(m_value[11].value);
	tFamInfo.nThrehold_WTDual = atoi(m_value[12].value);
	tFamInfo.nThrehold_PH = atoi(m_value[13].value);
	tFamInfo.nThrehold_PHLow = atoi(m_value[14].value);
	tFamInfo.nThrehold_PHHigh = atoi(m_value[15].value);
	tFamInfo.nThrehold_PHDual = atoi(m_value[16].value);
	tFamInfo.nThrehold_Cli_Temp = atoi(m_value[17].value);
	tFamInfo.nThrehold_Cli_TempLow = atoi(m_value[18].value);
	tFamInfo.nThrehold_Cli_TempHigh = atoi(m_value[19].value);
	tFamInfo.nThrehold_Cli_TempDual = atoi(m_value[20].value);
	tFamInfo.nThrehold_Cli_Humi = atoi(m_value[21].value);
	tFamInfo.nThrehold_Cli_HumiLow = atoi(m_value[22].value);
	tFamInfo.nThrehold_Cli_HumiHigh = atoi(m_value[23].value);
	tFamInfo.nThrehold_Cli_HumiDual = atoi(m_value[24].value);
	tFamInfo.nThrehold_Cli_Light = atoi(m_value[25].value);
	tFamInfo.nThrehold_Cli_LightLow = atoi(m_value[26].value);
	tFamInfo.nThrehold_Cli_LightHigh = atoi(m_value[27].value);
	tFamInfo.nThrehold_Cli_LightDual = atoi(m_value[28].value);
	tFamInfo.nThrehold_FlowRate = atoi(m_value[29].value);
	tFamInfo.nThrehold_FlowRateLow = atoi(m_value[30].value);
	tFamInfo.nThrehold_FlowRateHigh = atoi(m_value[31].value);
	tFamInfo.nThrehold_FlowRateDual = atoi(m_value[32].value);
	tFamInfo.nThrehold_TankLevel = atoi(m_value[33].value);
	tFamInfo.nThrehold_TankLevelLow = atoi(m_value[34].value);
	tFamInfo.nThrehold_TankLevelHigh = atoi(m_value[35].value);
	tFamInfo.nThrehold_TankLevelDual = atoi(m_value[36].value);
	tFamInfo.nPump_TimePeri = atoi(m_value[37].value);
	tFamInfo.nPump_TimeNozzleOn = atoi(m_value[38].value);
	tFamInfo.nPump_TimeNozzleOff = atoi(m_value[39].value);
	tFamInfo.nPump_TimePumpMix = atoi(m_value[40].value);
	tFamInfo.nCalib_Ec1A = atoi(m_value[41].value);
	tFamInfo.nCalib_Ec1B = atoi(m_value[42].value);
	tFamInfo.nCalib_Ec2A = atoi(m_value[43].value);
	tFamInfo.nCalib_Ec2B = atoi(m_value[44].value);
	tFamInfo.nCalib_Wt1A = atoi(m_value[45].value);
	tFamInfo.nCalib_Wt1B = atoi(m_value[46].value);
	tFamInfo.nCalib_Wt2A = atoi(m_value[47].value);
	tFamInfo.nCalib_Wt2B = atoi(m_value[48].value);
	tFamInfo.nCalib_Ph1A = atoi(m_value[49].value);
	tFamInfo.nCalib_Ph1B = atoi(m_value[50].value);
	tFamInfo.nCalib_Ph2A = atoi(m_value[51].value);
	tFamInfo.nCalib_Ph2B = atoi(m_value[52].value);
	tFamInfo.nOffset_Cli_Temp1 = atoi(m_value[53].value);
	tFamInfo.nOffset_Cli_Temp2 = atoi(m_value[54].value);
	tFamInfo.nOffset_Cli_Humi1 = atoi(m_value[55].value);
	tFamInfo.nOffset_Cli_Humi2 = atoi(m_value[56].value);
	tFamInfo.nOffset_Cli_Light1 = atoi(m_value[57].value);
	tFamInfo.nOffset_Cli_Light2 = atoi(m_value[58].value);
	tFamInfo.nOffset_US1 = atoi(m_value[59].value);
	tFamInfo.nOffset_US2 = atoi(m_value[60].value);
	tFamInfo.nOffset_US3 = atoi(m_value[61].value);
	tFamInfo.nOffset_US4 = atoi(m_value[62].value);
	tFamInfo.nOffset_US5 = atoi(m_value[63].value);
	tFamInfo.nOffset_US6 = atoi(m_value[64].value);
	tFamInfo.nVoltage_Ph1_Newtral = atoi(m_value[65].value);
	tFamInfo.nVoltage_Ph2_Newtral = atoi(m_value[66].value);
	tFamInfo.nVoltage_Ph1_Acid = atoi(m_value[67].value);
	tFamInfo.nVoltage_Ph2_Acid = atoi(m_value[68].value);
	tFamInfo.nTime_NozzleOn = atoi(m_value[69].value);
	tFamInfo.nTime_NozzleOff = atoi(m_value[70].value);
	tFamInfo.nTime_CoopadOn = atoi(m_value[71].value);
	tFamInfo.nTime_CoopadOff = atoi(m_value[72].value);
	tFamInfo.nTime_ChillerOn = atoi(m_value[73].value);
	tFamInfo.nTime_ChillerOff = atoi(m_value[74].value);
	tFamInfo.nTime_ShadingNetOn = atoi(m_value[75].value);
	tFamInfo.nTime_ShadingNetOff = atoi(m_value[76].value);

	return true;
}

bool MODE_ParamsServerSave(void)
{
	int index = 0;

	HAL_FLASH_Unlock();
	FlashEraseSector(ADDR_FLASH_SECTOR_8,ADDR_FLASH_SECTOR_9  + 128 -1);

	FlashWriteString(EEPROM_START_ADDRESS + (uint32_t)(index), (uint32_t *)tFamInfo.cFarmCode);
	index += 20;
	FlashWriteString(EEPROM_START_ADDRESS + (uint32_t)(index), (uint32_t *)tFamInfo.cFarmName);
	index += 12;
	FlashWriteString(EEPROM_START_ADDRESS + (uint32_t)(index), (uint32_t *)tFamInfo.cFwVersion);
	index += 12;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nDateTime_Hour);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nDateTime_Minute);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nTimeUpload);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Ec);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_EcLow);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_ECHigh);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_ECDual);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_WT);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_WTLow);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_WTHigh);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_WTDual);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_PH);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_PHLow);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_PHHigh);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_PHDual);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_Temp);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_TempLow);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_TempHigh);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_TempDual);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_Humi);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_HumiLow);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_HumiHigh);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_HumiDual);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_Light);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_LightLow);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_LightHigh);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_LightDual);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_FlowRate);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_FlowRateLow);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_FlowRateHigh);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_FlowRateDual);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_TankLevel);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_TankLevelLow);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_TankLevelHigh);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_TankLevelDual);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nPump_TimePeri);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nPump_TimeNozzleOn);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nPump_TimeNozzleOff);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nPump_TimePumpMix);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Ec1A);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Ec1B);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Ec2A);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Ec2B);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Wt1A);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Wt1B);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Wt2A);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Wt2B);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Ph1A);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Ph1B);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Ph2A);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Ph2B);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_Cli_Temp1);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_Cli_Temp2);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_Cli_Humi1);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_Cli_Humi2);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_Cli_Light1);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_Cli_Light2);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_US1);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_US2);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_US3);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_US4);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_US5);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_US6);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nVoltage_Ph1_Newtral);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nVoltage_Ph1_Acid);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nVoltage_Ph2_Newtral);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nVoltage_Ph2_Acid);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nStandard_Ec);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nStandard_Ph);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nTime_NozzleOn);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nTime_NozzleOff);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nTime_CoopadOn);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nTime_CoopadOff);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nTime_ChillerOn);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nTime_ChillerOff);

	HAL_FLASH_Lock();

	return true;
}
