/*
 * button.c
 *
 *  Created on: 2020. 8. 16.
 *      Author: Cuong Le Hung
 */
#include "../include.h"
//#include "button.h"

#define TIMERDEBOUNCEMAX 100		//Chong rung phim
#define TIMERCHECKLONGCLICK 1000	
#define TIMERHANDLELONGCLICK	50

int16_t nTimeStampButtonMode = 0;
int16_t nTimeStampButtonUp = 0;
int16_t nTimeStampButtonDown = 0;
int16_t nTimeStampButtonSet = 0;
uint8_t nTimeCheckDebounce = 0;

/*
	Read Button Mode
*/
buttonMode_t DetectButtonMode(void){
	uint8_t i=0;
	uint8_t previousVal, currentVal;
	previousVal=HAL_GPIO_ReadPin(BTN_MODE_GPIO_Port, BTN_MODE_Pin);
	while(i<TIMERDEBOUNCEMAX){
		currentVal=HAL_GPIO_ReadPin(BTN_MODE_GPIO_Port, BTN_MODE_Pin);
		if(currentVal!=previousVal){
			previousVal=currentVal;
			i=0;
		}else i++;
	}
	if(!currentVal)	
		return BUTTON_MODE_DETECTED;
	else return BUTTON_MODE_NOTDETECTED;
}
buttonMode_t ReadButtonMode(void){
	buttonMode_t ret = BUTTON_MODE_NOTDETECTED;
	if(DetectButtonMode() == BUTTON_MODE_DETECTED){
		nTimeStampButtonMode++;
	}else{
		if(nTimeStampButtonMode > 100){
			ret = BUTTON_MODE_CLICK;
			nTimeStampButtonMode = 0;
		}else{
			ret = BUTTON_MODE_NOTDETECTED;
		}
	}
	return ret;
}

/*
	Read Button Up
*/
buttonUp_t DetectButtonUp(void){
	uint8_t i=0;
	uint8_t previousVal, currentVal;
	previousVal=HAL_GPIO_ReadPin(BTN_UP_GPIO_Port, BTN_UP_Pin);
	while(i<TIMERDEBOUNCEMAX){
		currentVal=HAL_GPIO_ReadPin(BTN_UP_GPIO_Port, BTN_UP_Pin);
		if(currentVal!=previousVal){
			previousVal=currentVal;
			i=0;
		}else i++;
	}
	if(!currentVal)
		return BUTTON_UP_DETECTED;
	else return BUTTON_UP_NOTDETECTED;
}
buttonUp_t ReadButtonUp(void){
	buttonUp_t ret = BUTTON_UP_NOTDETECTED;
	if(DetectButtonUp() == BUTTON_UP_DETECTED){
		nTimeStampButtonUp++;
		if(nTimeStampButtonUp > TIMERCHECKLONGCLICK){
			ret = BUTTON_UP_LONGCLICK;
		}
	}else{
		if(nTimeStampButtonUp > 0){
			ret = BUTTON_UP_CLICK;
			nTimeStampButtonUp = 0;
		}else{
			ret = BUTTON_UP_NOTDETECTED;
		}
	}
	return ret;
}

/*
	Read Button Down
*/
buttonDown_t DetectButtonDown(void){
	uint8_t i=0;
	uint8_t previousVal, currentVal;
	previousVal=HAL_GPIO_ReadPin(BTN_DOWN_GPIO_Port, BTN_DOWN_Pin);
	while(i<TIMERDEBOUNCEMAX){
		currentVal=HAL_GPIO_ReadPin(BTN_DOWN_GPIO_Port, BTN_DOWN_Pin);
		if(currentVal!=previousVal){
			previousVal=currentVal;
			i=0;
		}else i++;
	}
	if(!currentVal)
		return BUTTON_DOWN_DETECTED;
	else return BUTTON_DOWN_NOTDETECTED;
}
buttonDown_t ReadButtonDown(void){
	buttonDown_t ret = BUTTON_DOWN_NOTDETECTED;
	if(DetectButtonDown() == BUTTON_DOWN_DETECTED){
		nTimeStampButtonDown++;
		if(nTimeStampButtonDown > TIMERCHECKLONGCLICK){
			ret = BUTTON_DOWN_LONGCLICK;
		}
	}else{
		if(nTimeStampButtonDown > 0){
			ret = BUTTON_DOWN_CLICK;
			nTimeStampButtonDown = 0;
		}else{
			ret = BUTTON_DOWN_NOTDETECTED;
		}
	}
	return ret;
}

/*
	Read Button Set
*/
buttonSet_t DetectButtonSet(void){
	uint8_t i=0;
	uint8_t previousVal, currentVal;
	previousVal=HAL_GPIO_ReadPin(BTN_SET_GPIO_Port, BTN_SET_Pin);
	while(i<TIMERDEBOUNCEMAX){
		currentVal=HAL_GPIO_ReadPin(BTN_SET_GPIO_Port, BTN_SET_Pin);
		if(currentVal!=previousVal){
			previousVal=currentVal;
			i=0;
		}else i++;
	}
	if(!currentVal)
		return BUTTON_SET_DETECTED;
	else return BUTTON_SET_NOTDETECTED;
}
buttonSet_t ReadButtonSet(void){
	buttonSet_t ret = BUTTON_SET_NOTDETECTED;
	if(DetectButtonSet() == BUTTON_SET_DETECTED){
		nTimeStampButtonSet++;
		if (nTimeStampButtonSet > TIMERCHECKLONGCLICK){
			ret = BUTTON_SET_LONGCLICK;
		}
	}else{
		if(nTimeStampButtonSet > 0){
			ret = BUTTON_SET_CLICK;
			nTimeStampButtonSet = 0;
		}else{
			ret = BUTTON_SET_NOTDETECTED;
		}
	}
	return ret;
}
