/*
 * button.h
 *
 *  Created on: 2020. 8. 16.
 *      Author: Cuong Le Hung
 */
#ifndef __BUTTON_H
#define __BUTTON_H

#include "stm32f4xx_hal.h"
#include <stdlib.h>
#include <stdbool.h>

typedef enum{
	BUTTON_MODE_CLICK = 1,
	BUTTON_MODE_NOTDETECTED = 2,
	BUTTON_MODE_LONGCLICK = 3,
	BUTTON_MODE_DETECTED = 4
} buttonMode_t;
typedef enum{
	BUTTON_UP_CLICK = 10,
	BUTTON_UP_NOTDETECTED = 11,
	BUTTON_UP_LONGCLICK = 12,
	BUTTON_UP_DETECTED = 13
} buttonUp_t;
typedef enum{
	BUTTON_DOWN_CLICK = 20,
	BUTTON_DOWN_NOTDETECTED = 21,
	BUTTON_DOWN_LONGCLICK = 22,
	BUTTON_DOWN_DETECTED = 23
} buttonDown_t;
typedef enum{
	BUTTON_SET_CLICK = 30,
	BUTTON_SET_NOTDETECTED = 31,
	BUTTON_SET_LONGCLICK = 32,
	BUTTON_SET_DETECTED = 33
} buttonSet_t;

typedef struct
{
	int16_t timeStampButtonMode;
	int16_t timeStampButtonUp;
	int16_t timeStampButtonDown;
	int16_t timeStampButtonSet;
	uint8_t timerCheckDebounce;
} _BUTTON;

buttonMode_t ReadButtonMode(void);
buttonUp_t ReadButtonUp(void);
buttonDown_t ReadButtonDown(void);
buttonSet_t ReadButtonSet(void);

#endif
