/*
 * gpio.h
 *
 *  Created on: 2020. 8. 16.
 *      Author: Cuong Le Hung
 */
#ifndef __GPIO_H
#define __GPIO_H
 
#include "stm32f4xx_hal.h"
#include <stdlib.h>
#include <stdbool.h>
 
#define SET_NOZZLE(x)	HAL_GPIO_WritePin(OP_O1_GPIO_Port, OP_O1_Pin, x);
#define SET_CHILLER(x)	HAL_GPIO_WritePin(OP_O2_GPIO_Port, OP_O2_Pin, x);

typedef enum
{
	CTL_NOZZLE_AUTO = 1,
	CTL_NOZZLE_OFF
} CtlNozzle_t;
typedef enum
{
	CTL_CHILLER_AUTO = 5,
	CTL_CHILLER_OFF
} CtlChiller_t;

typedef struct
{
	int16_t nTimerNozzleOn;
	int16_t nTimerNozzleOnWait;
} _GPIO;
 
 void GPIO_Init(void);
 bool GPIO_Process(void);
 bool GPIO_Timer1s(void);
 
#endif

