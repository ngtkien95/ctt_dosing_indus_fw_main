/*
 * gpio.c
 *
 *  Created on: 2020. 8. 16.
 *      Author: Cuong Le Hung
 * O1 - PD0 - NOZZLE
 * O2 - PD1 - CHILLER
 * O3 - PD2 - COOPAD
 * O4 - PD3 - PMIX
 * O5 - PD4
 * O6 - PD5
 * O7 - PB9
 * O8 - PE1
 * O9 - PE0
 * O10 - PD6
 */
#include "../include.h"

extern _DS3231 tRtc;
extern _SENSOR_CLI tSensorCliValue;
extern _SENSOR_IRRI tSensorIrriValue;
extern modeSetting_t modeSetting;
extern _FAM_INFO tFamInfo;

_GPIO tGpio;
CtlNozzle_t CtlNozzle;
CtlChiller_t CtlChiller;

static void ControlNozzle(void);
static void ControlChiller(void);

void GPIO_Init(void)
{
	// Off buzzer
	HAL_GPIO_WritePin(OP_BUZ_GPIO_Port, OP_BUZ_Pin, GPIO_PIN_RESET);
	CtlNozzle = CTL_NOZZLE_OFF;
	SET_NOZZLE((GPIO_PinState)SET_OFF);
	SET_CHILLER((GPIO_PinState)SET_OFF);
}

void ControlNozzle(void)
{
	if (tSensorCliValue.fCliHumiAvg == 0) // No data from sensor
	{
		CtlNozzle = CTL_NOZZLE_AUTO;
	}
	else if ((tSensorCliValue.fCliHumiAvg <= tFamInfo.nThrehold_Cli_Humi / 10) && (tSensorCliValue.fCliHumiAvg > 0))
	{
		CtlNozzle = CTL_NOZZLE_AUTO;
	}
	else if (tSensorCliValue.fCliHumiAvg > (tFamInfo.nThrehold_Cli_Humi / 10))
	{
		CtlNozzle = CTL_NOZZLE_OFF;
	}
}

void ControlChiller(void)
{
	if (tSensorIrriValue.dWtAvg == 0) // No data fro sensor
	{
		CtlChiller = CTL_CHILLER_AUTO;
	}
	else if (tSensorIrriValue.dWtAvg >= tFamInfo.nThrehold_WTLow / 10)
	{
		CtlChiller = CTL_CHILLER_AUTO;
	}
	else if ((tSensorIrriValue.dWtAvg < tFamInfo.nThrehold_WTLow / 10) && (tSensorIrriValue.dWtAvg > 0))
	{
		CtlChiller = CTL_CHILLER_OFF;
	}
}

bool GPIO_Process(void)
{
	if (modeSetting != SETTING_ENABLE)
	{
		// control nozzle
		switch (tSensorCliValue.tHumiCheck)
		{
		case CHECK_HUMI_OK:
		case CHECK_HUMI_LOW:
		case CHECK_HUMI_DUAL:
			if (tRtc.State == DS3231_GET_TIME) // Check if RTC working ok
			{
				if ((tRtc.Hour >= tFamInfo.nTime_NozzleOn) && (tRtc.Hour < tFamInfo.nTime_NozzleOff))
				{
					ControlNozzle();
				}
				else
				{
					CtlNozzle = CTL_NOZZLE_OFF;
				}
			}
			else
			{
				// bo sung them thoi gian tu web

				ControlNozzle();
			}
			break;
		case CHECK_HUMI_HIGH:
			CtlNozzle = CTL_NOZZLE_OFF;
			break;
		}

		// control chiller
		switch (tSensorIrriValue.tWtCheck)
		{
		case CHECK_WT_DUAL:
		case CHECK_WT_HIGH:
		case CHECK_WT_OK:
			if (tRtc.State == DS3231_GET_TIME)
			{
				if ((tRtc.Hour >= tFamInfo.nTime_CoopadOn) && (tRtc.Hour < tFamInfo.nTime_CoopadOff))
				{
					ControlChiller();
				}
				else
				{
					CtlChiller = CTL_CHILLER_OFF;
				}
			}
			else
			{
				// bo sung them thoi gian tu web

				ControlChiller();
			}
			break;
		case CHECK_WT_LOW:
			SET_CHILLER((GPIO_PinState)SET_OFF);
			break;
		}
	}

	return true;
}

bool GPIO_Timer1s(void)
{

	if (tGpio.nTimerNozzleOn > 0)
		tGpio.nTimerNozzleOn--;
	if (tGpio.nTimerNozzleOnWait > 0)
		tGpio.nTimerNozzleOnWait--;

	if (modeSetting != SETTING_ENABLE)
	{
		switch (CtlNozzle)
		{
		case CTL_NOZZLE_AUTO:
			if (tGpio.nTimerNozzleOnWait == 0)
			{
				tGpio.nTimerNozzleOnWait = tFamInfo.nPump_TimeNozzleOff;
				tGpio.nTimerNozzleOn = tFamInfo.nPump_TimeNozzleOn;
			}
			break;
		case CTL_NOZZLE_OFF:
			SET_NOZZLE((GPIO_PinState)SET_OFF);
			break;
		}
		if (tGpio.nTimerNozzleOn != 0)
		{
			SET_NOZZLE((GPIO_PinState)SET_ON);
		}
		else
		{
			SET_NOZZLE((GPIO_PinState)SET_OFF);
		}

		switch (CtlChiller)
		{
		case CTL_CHILLER_AUTO:
			SET_CHILLER((GPIO_PinState)SET_ON);
			break;
		case CTL_CHILLER_OFF:
			SET_CHILLER((GPIO_PinState)SET_OFF);
			break;
		}
	}

	return true;
}
