/*
 * flash.c
 *
 *  Created on: 2020. 8. 16.
 *      Author: Cuong Le Hung
 */
 #include "../include.h"

/*
	Read and Write Flash
*/
void FLASH_Read(uint32_t adr, int32_t *data)
{
	*data = *(__IO uint32_t *)adr;
}
void FLASH_ReadString(uint32_t adr, __IO uint32_t * data)
{
	while (1)
	{
		*data = *(__IO uint32_t *)adr;
		if (*data == 0xffffffff)
		{
			*data = '\0';
			break;
		}
		adr += 4;
		data++;
	}
}
void FLASH_Unlock(void)
{
	FLASH->KEYR=0x45670123;
	FLASH->KEYR=0xCDEF89AB;
}
void FLASH_Lock(void)
{
	FLASH->CR=0x00000080;
}
void FLASH_Erase(uint32_t adr)
{
	FLASH->CR |= 0x00000002;	// CR Page Earse
	FLASH->ACR |= adr;					// CR Add Earse
	FLASH->CR |= 0x00000040;	// CR Mass Earse
	while((FLASH->SR & 0x00000001));	// Check is complete
	FLASH->CR &= ~0x00000042;		// Reset
}
void FLASH_Write(uint32_t adr, uint16_t data)
{
	FLASH->CR |= 0x00000001;		// CR Page Write
	*(__IO uint16_t*)adr = data;
	while((FLASH->SR & 0x00000001));	// Is complete
	CLEAR_BIT(FLASH->CR, FLASH_CR_PG);
}
void FLASH_WriteWord(uint32_t adr, uint32_t data)
{
	uint8_t index = 0;
	for (index = 0U; index < 2U; index++)
	{
		FLASH_Write(adr + (2U*index), (uint16_t)(data >> (16*index)));
	}
}
void FLASH_WriteString(uint32_t adr, uint32_t * data)
{
	uint8_t index = 0;
	uint8_t numberofwords = 0;
	
	numberofwords = (strlen((const char *)data)/4) + ((strlen((const char *)data) % 4) != 0);
	// numberofwords = strlen((const char *)data);
	
	while (index<numberofwords)
	{
		FLASH_WriteWord(adr,data[index]);
		adr += 4;
		index++;
	}
}


