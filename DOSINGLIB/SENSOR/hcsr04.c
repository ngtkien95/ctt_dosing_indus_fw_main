/*
 * hcsr04.c
 *
 *  Created on: 2020. 8. 21.
 *      Author: Cuong Le Hung
 *	US1_TRIG
 *	US1_ECHO
 */

#include "../include.h"
//#include "hcsr04.h"

TIM_HandleTypeDef *tim;
const float speedOfSound = 0.0343/2;

extern _FAM_INFO tFamInfo;

_HCSR04 tDistance;
STATE_US stateUS;
int32_t nDurationUs1 = 0;
int32_t nDurationUs2 = 0;
int32_t nDurationUs3 = 0;
int32_t nDurationUs4 = 0;
int32_t nDurationUs5 = 0;
int32_t nDurationUs6 = 0;
int32_t nHcsr04TimeOut = 0;

static void delay_us(uint32_t us);
static CheckTank1_t checkTank1(int32_t val1, int32_t val2);
static CheckTank2_t checkTank2(int32_t val1, int32_t val2);
static CheckTank3_t checkTank3(int32_t val1, int32_t val2);

const float tank_h = 70; // cm
const float tank_r = 25; // cm
#ifndef M_PI
# define M_PI 3.1415926535897932
#endif

void HCSR04_Init(TIM_HandleTypeDef *handle)
{
	tim = handle;
	stateUS = STATE_US1;
}

void delay_us(uint32_t us)
{
	HAL_TIM_Base_Start(tim);
	__HAL_TIM_SET_COUNTER(tim,0);
	while((__HAL_TIM_GET_COUNTER(tim))<us);
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	switch (stateUS)
  {
  	case STATE_US1:
			if(GPIO_Pin == US1_ECHO_EXTI10_Pin) // US1
			{
				if(HAL_GPIO_ReadPin(US1_ECHO_EXTI10_GPIO_Port,US1_ECHO_EXTI10_Pin)==GPIO_PIN_SET)
				{
					__HAL_TIM_SET_COUNTER(tim,0);
				}
				else
				{
					nDurationUs1 = __HAL_TIM_GET_COUNTER(tim);
					HAL_TIM_Base_Stop(tim);
				}
			}
  		break;
  	case STATE_US2:
			if(GPIO_Pin == US2_ECHO_EXTI14_Pin) // US2
			{
				if(HAL_GPIO_ReadPin(US2_ECHO_EXTI14_GPIO_Port,US2_ECHO_EXTI14_Pin)==GPIO_PIN_SET)
				{
					__HAL_TIM_SET_COUNTER(tim,0);
				}
				else
				{
					nDurationUs2 = __HAL_TIM_GET_COUNTER(tim);
					HAL_TIM_Base_Stop(tim);
				}
			}
  		break;
  	case STATE_US3:
			if(GPIO_Pin == US3_ECHO_EXTI13_Pin) // US3
			{
				if(HAL_GPIO_ReadPin(US3_ECHO_EXTI13_GPIO_Port,US3_ECHO_EXTI13_Pin)==GPIO_PIN_SET)
				{
					__HAL_TIM_SET_COUNTER(tim,0);
				}
				else
				{
					nDurationUs3 = __HAL_TIM_GET_COUNTER(tim);
					HAL_TIM_Base_Stop(tim);
				}
			}
			break;
		case STATE_US4:
			if(GPIO_Pin == US4_ECHO_EXTI11_Pin) // US3
			{
				if(HAL_GPIO_ReadPin(US4_ECHO_EXTI11_GPIO_Port,US4_ECHO_EXTI11_Pin)==GPIO_PIN_SET)
				{
					__HAL_TIM_SET_COUNTER(tim,0);
				}
				else
				{
					nDurationUs4 = __HAL_TIM_GET_COUNTER(tim);
					HAL_TIM_Base_Stop(tim);
				}
			}
			break;
		case STATE_US5:
			if(GPIO_Pin == US5_ECHO_EXTI3_Pin) // US3
			{
				if(HAL_GPIO_ReadPin(US5_ECHO_EXTI3_GPIO_Port,US5_ECHO_EXTI3_Pin)==GPIO_PIN_SET)
				{
					__HAL_TIM_SET_COUNTER(tim,0);
				}
				else
				{
					nDurationUs5 = __HAL_TIM_GET_COUNTER(tim);
					HAL_TIM_Base_Stop(tim);
				}
			}
			break;
		case STATE_US6:
			if(GPIO_Pin == US6_ECHO_EXTI15_Pin) // US3
			{
				if(HAL_GPIO_ReadPin(US6_ECHO_EXTI15_GPIO_Port,US6_ECHO_EXTI15_Pin)==GPIO_PIN_SET)
				{
					__HAL_TIM_SET_COUNTER(tim,0);
				}
				else
				{
					nDurationUs6 = __HAL_TIM_GET_COUNTER(tim);
					HAL_TIM_Base_Stop(tim);
				}
			}
			break;
  }
}

bool HCSR04_Timer(void)
{
	if (nHcsr04TimeOut > 0)	nHcsr04TimeOut--;
	if (nHcsr04TimeOut == 0)
	{
		switch (stateUS)
		{
			case STATE_US1:
				stateUS = STATE_US2;
				break;
			case STATE_US2:
				stateUS = STATE_US3;
				break;
			case STATE_US3:
				stateUS = STATE_US4;
				break;
			case STATE_US4:
				stateUS = STATE_US5;
				break;
			case STATE_US5:
				stateUS = STATE_US6;
				break;
			case STATE_US6:
				stateUS = STATE_US1;
				break;
		}
		nHcsr04TimeOut = 1;
	}
	
	return true;
}

bool HCSR04_getDistance(void)
{
	switch (stateUS)
  {
  	case STATE_US1:
			HAL_GPIO_WritePin(US1_TRIG_GPIO_Port,US1_TRIG_Pin,GPIO_PIN_RESET);
			delay_us(2);
			HAL_GPIO_WritePin(US1_TRIG_GPIO_Port,US1_TRIG_Pin,GPIO_PIN_SET);
			delay_us(10);
			HAL_GPIO_WritePin(US1_TRIG_GPIO_Port,US1_TRIG_Pin,GPIO_PIN_RESET);
			HAL_TIM_Base_Start(tim);
  		break;
  	case STATE_US2:
			HAL_GPIO_WritePin(US2_TRIG_GPIO_Port,US2_TRIG_Pin,GPIO_PIN_RESET);
			delay_us(2);
			HAL_GPIO_WritePin(US2_TRIG_GPIO_Port,US2_TRIG_Pin,GPIO_PIN_SET);
			delay_us(10);
			HAL_GPIO_WritePin(US2_TRIG_GPIO_Port,US2_TRIG_Pin,GPIO_PIN_RESET);
			HAL_TIM_Base_Start(tim);
  		break;
  	case STATE_US3:
			HAL_GPIO_WritePin(US3_TRIG_GPIO_Port,US3_TRIG_Pin,GPIO_PIN_RESET);
			delay_us(2);
			HAL_GPIO_WritePin(US3_TRIG_GPIO_Port,US3_TRIG_Pin,GPIO_PIN_SET);
			delay_us(10);
			HAL_GPIO_WritePin(US3_TRIG_GPIO_Port,US3_TRIG_Pin,GPIO_PIN_RESET);
			HAL_TIM_Base_Start(tim);
  		break;
		case STATE_US4:
			HAL_GPIO_WritePin(US4_TRIG_GPIO_Port,US4_TRIG_Pin,GPIO_PIN_RESET);
			delay_us(2);
			HAL_GPIO_WritePin(US4_TRIG_GPIO_Port,US4_TRIG_Pin,GPIO_PIN_SET);
			delay_us(10);
			HAL_GPIO_WritePin(US4_TRIG_GPIO_Port,US4_TRIG_Pin,GPIO_PIN_RESET);
			HAL_TIM_Base_Start(tim);
  		break;
		case STATE_US5:
			HAL_GPIO_WritePin(US5_TRIG_GPIO_Port,US5_TRIG_Pin,GPIO_PIN_RESET);
			delay_us(2);
			HAL_GPIO_WritePin(US5_TRIG_GPIO_Port,US5_TRIG_Pin,GPIO_PIN_SET);
			delay_us(10);
			HAL_GPIO_WritePin(US5_TRIG_GPIO_Port,US5_TRIG_Pin,GPIO_PIN_RESET);
			HAL_TIM_Base_Start(tim);
  		break;
		case STATE_US6:
			HAL_GPIO_WritePin(US6_TRIG_GPIO_Port,US6_TRIG_Pin,GPIO_PIN_RESET);
			delay_us(2);
			HAL_GPIO_WritePin(US6_TRIG_GPIO_Port,US6_TRIG_Pin,GPIO_PIN_SET);
			delay_us(10);
			HAL_GPIO_WritePin(US6_TRIG_GPIO_Port,US6_TRIG_Pin,GPIO_PIN_RESET);
			HAL_TIM_Base_Start(tim);
  		break;
  }
	

	//Estimate distance
	tDistance.dDisUs1 = nDurationUs1*speedOfSound;
	if((tDistance.dDisUs1 < 2)||(tDistance.dDisUs1 > 400))
		tDistance.dDisUs1 = 0;
	
	tDistance.dDisUs2 = nDurationUs2*speedOfSound;
	if((tDistance.dDisUs2 < 2)||(tDistance.dDisUs2 > 400))
		tDistance.dDisUs2 = 0;
	
	tDistance.dDisUs3 = nDurationUs3*speedOfSound;
	if((tDistance.dDisUs3 < 2)||(tDistance.dDisUs3 > 400))
		tDistance.dDisUs3 = 0;
	
	tDistance.dDisUs4 = nDurationUs4*speedOfSound;
	if((tDistance.dDisUs4 < 2)||(tDistance.dDisUs4 > 400))
		tDistance.dDisUs4 = 0;
	
	tDistance.dDisUs5 = nDurationUs5*speedOfSound;
	if((tDistance.dDisUs5 < 2)||(tDistance.dDisUs5 > 400))
		tDistance.dDisUs5 = 0;
	
	tDistance.dDisUs6 = nDurationUs6*speedOfSound;
	if((tDistance.dDisUs6 < 2)||(tDistance.dDisUs6 > 400))
		tDistance.dDisUs6 = 0;
	
	// FIX ERROR SMARTGREEN
	tDistance.dDisUs2 = tDistance.dDisUs1;
	
	tDistance.dLitUs1 = M_PI*pow(tank_r,2)*(tank_h - tDistance.dDisUs1)/1000 + tFamInfo.nOffset_US1;
	tDistance.dLitUs2 = M_PI*pow(tank_r,2)*(tank_h - tDistance.dDisUs2)/1000 + tFamInfo.nOffset_US2;
	tDistance.dLitUs3 = M_PI*pow(tank_r,2)*(tank_h - tDistance.dDisUs3)/1000 + tFamInfo.nOffset_US3;
	tDistance.dLitUs4 = M_PI*pow(tank_r,2)*(tank_h - tDistance.dDisUs4)/1000 + tFamInfo.nOffset_US4;
	tDistance.dLitUs5 = M_PI*pow(tank_r,2)*(tank_h - tDistance.dDisUs5)/1000 + tFamInfo.nOffset_US5;
	tDistance.dLitUs6 = M_PI*pow(tank_r,2)*(tank_h - tDistance.dDisUs6)/1000 + tFamInfo.nOffset_US6;
	
	tDistance.dLitTank1 = (tDistance.dLitUs1 + tDistance.dLitUs2)/2;
	tDistance.dLitTank2 = (tDistance.dLitUs3 + tDistance.dLitUs4)/2;
	tDistance.dLitTank3 = (tDistance.dLitUs5 + tDistance.dLitUs6)/2;
	tDistance.tTank1Check = checkTank1(tDistance.dLitUs1,tDistance.dLitUs2);
	tDistance.tTank2Check = checkTank2(tDistance.dLitUs3,tDistance.dLitUs4);
	tDistance.tTank3Check = checkTank3(tDistance.dLitUs5,tDistance.dLitUs6);
	
	return true;
}

bool HCSR04_calibTank1(uint16_t avg)
{
	double a0 = avg;
	double a1 = tDistance.dLitUs1;
	double a2 = tDistance.dLitUs2;

	if (a0 >= a1)
	{
		tFamInfo.nOffset_US1 += (uint16_t)(a0 - a1);
	}
	else
	{
		tFamInfo.nOffset_US1 -= (uint16_t)(a0 - a1);
	}
	
	if(a0 >= a2) 
	{
		tFamInfo.nOffset_US2 += (uint16_t)(a0 - a2);
	}
	else
	{
		tFamInfo.nOffset_US2 -= (uint16_t)(a0 - a2);
	}

	return true;
}

bool HCSR04_calibTank2(uint16_t avg)
{
	double a0 = avg;
	double a1 = tDistance.dLitUs3;
	double a2 = tDistance.dLitUs4;

	if (a0 >= a1)
	{
		tFamInfo.nOffset_US3 += (uint16_t)(a0 - a1);
	}
	else
	{
		tFamInfo.nOffset_US3 -= (uint16_t)(a0 - a1);
	}
	
	if(a0 >= a2) 
	{
		tFamInfo.nOffset_US4 += (uint16_t)(a0 - a2);
	}
	else
	{
		tFamInfo.nOffset_US4 -= (uint16_t)(a0 - a2);
	}

	return true;
}

bool HCSR04_calibTank3(uint16_t avg)
{
	double a0 = avg;
	double a1 = tDistance.dLitUs5;
	double a2 = tDistance.dLitUs6;

	if (a0 >= a1)
	{
		tFamInfo.nOffset_US5 += (uint16_t)(a0 - a1);
	}
	else
	{
		tFamInfo.nOffset_US5 -= (uint16_t)(a0 - a1);
	}
	
	if(a0 >= a2) 
	{
		tFamInfo.nOffset_US6 += (uint16_t)(a0 - a2);
	}
	else
	{
		tFamInfo.nOffset_US6 -= (uint16_t)(a0 - a2);
	}

	return true;
}

CheckTank1_t checkTank1(int32_t val1, int32_t val2)
{
	CheckTank1_t ret;
	double dDelta = fabs((double)(val1 - val2));
	double dAvg = (val1 + val2)/2;
	
	if(dDelta >= 5)
		ret = CHECK_TANK1_DUAL;
	else if(dAvg >= 100)
		ret = CHECK_TANK1_HIGH;
	else if(dAvg <= 20)
		ret = CHECK_TANK1_LOW;
	else
		ret = CHECK_TANK1_OK;
	
	return ret;
}

CheckTank2_t checkTank2(int32_t val1, int32_t val2)
{
	CheckTank2_t ret;
	double dDelta = fabs((double)(val1 - val2));
	double dAvg = (val1 + val2)/2;
	
	if(dDelta >= 5)
		ret = CHECK_TANK2_DUAL;
	else if(dAvg >= 100)
		ret = CHECK_TANK2_HIGH;
	else if(dAvg <= 20)
		ret = CHECK_TANK2_LOW;
	else
		ret = CHECK_TANK2_OK;
	
	return ret;
}

CheckTank3_t checkTank3(int32_t val1, int32_t val2)
{
	CheckTank3_t ret;
	double dDelta = fabs((double)(val1 - val2));
	double dAvg = (val1 + val2)/2;
	
	if(dDelta >= 5)
		ret = CHECK_TANK3_DUAL;
	else if(dAvg >= 100)
		ret = CHECK_TANK3_HIGH;
	else if(dAvg <= 20)
		ret = CHECK_TANK3_LOW;
	else
		ret = CHECK_TANK3_OK;
	
	return ret;
}
