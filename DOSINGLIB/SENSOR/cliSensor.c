/*
 * cliSensor.c
 *
 *  Created on: 2020. 8. 26.
 *      Author: Cuong Le Hung
 */
#include "../include.h"
#include "usart.h"

UART_HandleTypeDef *uart;

extern _FAM_INFO tFamInfo;
extern bool bZigbeeState;

_SENSOR_CLI tSensorCliValue;
char str[MAX_STRING];

static CheckTemp_t checkTemp(float t1, float t2);
static CheckHumi_t checkHumi(float h1, float h2);
static CheckLight_t checkLight(uint16_t l1, uint16_t l2);
static bool getStrAt(const char *str, char sep, int index, char *ret);

void SENSOR_CliInit(UART_HandleTypeDef *handle)
{
	uart = handle;
	HAL_UART_Receive_DMA(uart, (uint8_t *)str, MAX_STRING);
	HAL_GPIO_WritePin(ZB_RST_GPIO_Port, ZB_RST_Pin, GPIO_PIN_SET);

	sprintf(str, "ST,T1,00.00,H1,00.00,F1,00000,T2,00.00,H2,00.00,F2,00000,ED");
}

bool getStrAt(const char *str, char sep, int index, char *ret)
{
	unsigned int start = 0, stop;
	int i = 0;

	for (stop = 0; str[stop]; stop++)
	{
		if (str[stop] == sep)
		{
			if (i == index)
			{
				strncpy(ret, str + start, stop - start);
			}
			else
			{
				i++;
				start = stop + 1;
			}
		}
	}

	return true;
}

CheckTemp_t checkTemp(float t1, float t2)
{
	CheckTemp_t ret;
	double dDelta = fabs(t1 - t2);
	double dAvg = (t1 + t2) / 2;

	if (dDelta >= (double)tFamInfo.nThrehold_Cli_TempDual / 10)
		ret = CHECK_TEMP_DUAL;
	else if (dAvg >= (double)tFamInfo.nThrehold_Cli_TempHigh / 10)
		ret = CHECK_TEMP_HIGH;
	else if (dAvg <= (double)tFamInfo.nThrehold_Cli_TempLow / 10)
		ret = CHECK_TEMP_LOW;
	else
		ret = CHECK_TEMP_OK;

	return ret;
}

CheckHumi_t checkHumi(float h1, float h2)
{
	CheckHumi_t ret;
	double dDelta = fabs(h1 - h2);
	double dAvg = (h1 + h2) / 2;

	if (dDelta >= (double)tFamInfo.nThrehold_Cli_HumiDual / 10)
		ret = CHECK_HUMI_DUAL;
	else if (dAvg >= (double)tFamInfo.nThrehold_Cli_HumiHigh / 10)
		ret = CHECK_HUMI_HIGH;
	else if (dAvg <= (double)tFamInfo.nThrehold_Cli_HumiLow / 10)
		ret = CHECK_HUMI_LOW;
	else
		ret = CHECK_HUMI_OK;

	return ret;
}

CheckLight_t checkLight(uint16_t l1, uint16_t l2)
{
	CheckLight_t ret;
	double dDelta = fabs((double)(l1 - l2));
	double dAvg = (l1 + l2) / 2;

	if (dDelta >= tFamInfo.nThrehold_Cli_LightDual)
		ret = CHECK_LIGHT_DUAL;
	else if (dAvg >= tFamInfo.nThrehold_Cli_LightHigh)
		ret = CHECK_LIGHT_HIGH;
	else if (dAvg <= tFamInfo.nThrehold_Cli_LightLow)
		ret = CHECK_LIGHT_LOW;
	else
		ret = CHECK_LIGHT_OK;

	return ret;
}

bool SENSOR_CalibTemp(float avg)
{
	double a0 = avg*100;
	double a1 = tSensorCliValue.fCliTemp1*100;
	double a2 = tSensorCliValue.fCliTemp2*100;

	if (a0 >= a1) // calib for climate temp1
	{
		tFamInfo.nOffset_Cli_Temp1 += (uint16_t)(a0 - a1);
	}
	else
	{
		tFamInfo.nOffset_Cli_Temp1 -= (uint16_t)(a0 - a1);
	}
	
	if(a0 >= a2) // calib for climate temp2
	{
		tFamInfo.nOffset_Cli_Temp2 += (uint16_t)(a0 - a2);
	}
	else
	{
		tFamInfo.nOffset_Cli_Temp2 -= (uint16_t)(a0 - a2);
	}
	
	return true;
}

bool SENSOR_CalibHumi(float avg)
{
	double a0 = avg*100;
	double a1 = tSensorCliValue.fCliHumi1*100;
	double a2 = tSensorCliValue.fCliHumi2*100;

	if (a0 >= a1) // calib for climate temp1
	{
		tFamInfo.nOffset_Cli_Humi1 += (uint16_t)(a0 - a1);
	}
	else
	{
		tFamInfo.nOffset_Cli_Humi1 -= (uint16_t)(a0 - a1);
	}
	
	if(a0 >= a2) // calib for climate temp2
	{
		tFamInfo.nOffset_Cli_Humi2 += (uint16_t)(a0 - a2);
	}
	else
	{
		tFamInfo.nOffset_Cli_Humi2 -= (uint16_t)(a0 - a2);
	}

	return true;
}

bool SENSOR_CalibLight(uint16_t avg)
{
	double a0 = avg;
	double a1 = tSensorCliValue.nCliLight1;
	double a2 = tSensorCliValue.nCliLight2;

	if (a0 >= a1) // calib for climate temp1
	{
		tFamInfo.nOffset_Cli_Light1 += (uint16_t)(a0 - a1);
	}
	else
	{
		tFamInfo.nOffset_Cli_Light1 -= (uint16_t)(a0 - a1);
	}
	
	if(a0 >= a2) // calib for climate temp2
	{
		tFamInfo.nOffset_Cli_Light2 += (uint16_t)(a0 - a2);
	}
	else
	{
		tFamInfo.nOffset_Cli_Light2 -= (uint16_t)(a0 - a2);
	}

	return true;
}

bool SENSOR_CliRead(void) // Run in timer
{
	char *data;
	data = (char *)malloc(5 * sizeof(char));

	if (bZigbeeState == true)
	{
		getStrAt(str, ',', 0, data);
		if ((data[0] == 'S') && (data[1] == 'T'))
		{
			getStrAt(str, ',', 2, data);
			tSensorCliValue.fCliTemp1 = atof(&data[0]) + (float)tFamInfo.nOffset_Cli_Temp1/100;
			getStrAt(str, ',', 4, data);
			tSensorCliValue.fCliHumi1 = atof(&data[0]) + (float)tFamInfo.nOffset_Cli_Humi1/100;
			getStrAt(str, ',', 6, data);
			tSensorCliValue.nCliLight1 = atoi(&data[0]) + (float)tFamInfo.nOffset_Cli_Light1;
			getStrAt(str, ',', 8, data);
			tSensorCliValue.fCliTemp2 = atof(&data[0]) + (float)tFamInfo.nOffset_Cli_Temp2/100;
			getStrAt(str, ',', 10, data);
			tSensorCliValue.fCliHumi2 = atof(&data[0]) + (float)tFamInfo.nOffset_Cli_Humi2/100;
			getStrAt(str, ',', 12, data);
			tSensorCliValue.nCliLight2 = atoi(&data[0]) + (float)tFamInfo.nOffset_Cli_Light2;

			tSensorCliValue.fCliTempAvg = (tSensorCliValue.fCliTemp1 + tSensorCliValue.fCliTemp2) / 2;
			tSensorCliValue.fCliHumiAvg = (tSensorCliValue.fCliHumi1 + tSensorCliValue.fCliHumi2) / 2;
			tSensorCliValue.nCliLightAvg = (tSensorCliValue.nCliLight1 + tSensorCliValue.nCliLight2) / 2;

			tSensorCliValue.tTempCheck = checkTemp(tSensorCliValue.fCliTemp1, tSensorCliValue.fCliTemp2);
			tSensorCliValue.tHumiCheck = checkHumi(tSensorCliValue.fCliHumi1, tSensorCliValue.fCliHumi2);
			tSensorCliValue.tLightCheck = checkLight(tSensorCliValue.nCliLight1, tSensorCliValue.nCliLight2);
		}
		else
		{
//			HAL_UART_Init(uart);
//			MX_USART1_UART_Init();
			HAL_UART_AbortReceive(uart);
			HAL_UART_Receive_DMA(uart, (uint8_t *)str, MAX_STRING);
		}
	}
	else
	{
//		HAL_UART_AbortReceive(uart);
//		HAL_UART_DeInit(uart);
	}
	

	free(data);
	return true;
}

