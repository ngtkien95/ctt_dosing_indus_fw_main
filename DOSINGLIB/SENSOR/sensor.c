/*
*@Name: sensor.c
*@Description: Read irrigation sensor: Ph, Ec, Water Temp, Flow sensor, Ultrasonic sensor
*@EC1: PC0 - ADC_IN10 - SENSOR_BUF_ADC1[2] - EC1
*@WT1: PC1 - ADC_IN11 - SENSOR_BUF_ADC1[3] - WT1
*@EC2: PC2 - ADC_IN12 - SENSOR_BUF_ADC1[4] - EC2
*@WT2: PC3 - ADC_IN13 - SENSOR_BUF_ADC1[5] - WT2
*@PH1: PA0 - ADC_IN0 - SENSOR_BUF_ADC1[0] - PH1
*@PH2: PA1 - ADC_IN1 - SENSOR_BUF_ADC1[1] - PH2
*/

#include "../include.h"
//#include "sensor.h"

#define USE2ADC

_SENSOR_IRRI tSensorIrriValue;
extern _FAM_INFO tFamInfo;

#ifdef USE2ADC
__IO uint32_t nBufTest[2];
__IO uint32_t nBufAdc[4];
#else
__IO uint32_t nBufAdc[6];
#endif

__IO long double dEc1Sum;
__IO long double dWaterTemp1Sum;
__IO long double dEc2Sum;
__IO long double dWaterTemp2Sum;
__IO long double dPh1Sum;
__IO long double dPh2Sum;
__IO uint16_t nCountIrriSample;
__IO uint16_t nCountEc1Sample;
__IO uint16_t nCountWt1Sample;
__IO uint16_t nCountEc2Sample;
__IO uint16_t nCountWt2Sample;
__IO uint16_t nCountPh1Sample;
__IO uint16_t nCountPh2Sample;


// Const for water temperature NTC 10K
const uint16_t RT0 = 10000; //(Ohm)
const uint16_t B = 3977;	//(K)
const float T0 = 25 + 273.15;
double VRT, VR, RT, ln;

// Const for ec compensation by water temp
//const float a = 0.019;
const float a = 0.021;

void SENSOR_IrriInit(ADC_HandleTypeDef *handle)
{
	HAL_ADC_Start_DMA(handle, (uint32_t *)&nBufAdc, 6);
}

void SENSOR_IrriInit2(ADC_HandleTypeDef *handle1, ADC_HandleTypeDef *handle2)
{
	HAL_ADC_Start_DMA(handle1, (uint32_t *)&nBufAdc, 4);
	HAL_ADC_Start_DMA(handle2, (uint32_t *)&nBufTest, 2);
}

bool SENSOR_IrriRead(void)
{
	if (nCountIrriSample < 5000)
	{
		/* Read EC1 ADC Value */
		if (nBufAdc[2] != 0)
		{
			dEc1Sum += (float)nBufAdc[2];
			nCountEc1Sample++;
		}

		/* Read WT1 ADC value */
		if (nBufAdc[3] != 0)
		{
			dWaterTemp1Sum += (float)nBufAdc[3];
			nCountWt1Sample++;
		}

#ifdef USE2ADC
		/* Read EC2 ADC Value */
		if (nBufTest[0] != 0)
		{
			dEc2Sum += (double)nBufTest[0];
			nCountEc2Sample++;
		}

		/* Read WT2 ADC value */
		if (nBufTest[1] != 0)
		{
			dWaterTemp2Sum += (double)nBufTest[1];
			nCountWt2Sample++;
		}
#else
		/* Read EC2 ADC Value */
		if (nBufAdc[4] != 0)
		{
			dEc2Sum += (double)nBufAdc[4];
			nCountEc2Sample++;
		}

		/* Read WT2 ADC value */
		if (nBufAdc[5] != 0)
		{
			dWaterTemp2Sum += (double)nBufAdc[5];
			nCountWt2Sample++;
		}
#endif

		/* Read PH1 ADC Value */
		if (nBufAdc[0] != 0)
		{
			dPh1Sum += (float)nBufAdc[0];
			nCountPh1Sample++;
		}

		/* Read PH2 ADC value */
		if (nBufAdc[1] != 0)
		{
			dPh2Sum += (float)nBufAdc[1];
			nCountPh2Sample++;
		}

		nCountIrriSample++;
	}
	else
	{
/* Calculate WT1 value */
#ifdef VOL_AMP_3V3
		VRT = dWaterTemp1Sum / nCountWt1Sample;	  // acquisition analog value of VRT
		tVar.dWaterTemp1mv = (3300 / 4096) * VRT; // conversion to voltage
		VR = 3300 - tVar.dWaterTemp1mv;
		RT = tVar.dWaterTemp1mv / (VR / RT0); // RT0 = R = 10KOhm
		ln = log(RT / RT0);
		tVar.dWaterTemp1 = (1 / ((ln / B) + (1 / T0))); // Temperature from thermistor
		tVar.dWaterTemp1 = tVar.dWaterTemp1 - 273.15;
		tVar.dWaterTemp1 = (double)tParam.CALIB_WT1_A * tVar.dWaterTemp1 / 100 + (double)tParam.CALIB_WT1_B / 100;
#else
		VRT = dWaterTemp1Sum / nCountWt1Sample;	  // acquisition analog value of VRT
		tSensorIrriValue.dWaterTemp1mv = (5000 / 4096) * VRT; // conversion to voltage
		VR = 5000 - tSensorIrriValue.dWaterTemp1mv;
		RT = tSensorIrriValue.dWaterTemp1mv / (VR / RT0); // RT0 = R = 10KOhm
		ln = log(RT / RT0);
		tSensorIrriValue.dWaterTemp1 = (1 / ((ln / B) + (1 / T0))); // Temperature from thermistor
		tSensorIrriValue.dWaterTemp1 = tSensorIrriValue.dWaterTemp1 - 273.15;
		//tSensorIrriValue.dWaterTemp1 = (double)tSensorParam.CALIB_WT1_A * tSensorIrriValue.dWaterTemp1 / 100 + (double)tSensorParam.CALIB_WT1_B / 100;
		tSensorIrriValue.dWaterTemp1 += (double)tFamInfo.nCalib_Wt1B/100;
#endif

/* Calculate WT2 value */
#ifdef VOL_AMP_3V3
		VRT = dWaterTemp2Sum / nCountWt2Sample;	  // acquisition analog value of VRT
		tVar.dWaterTemp2mv = (3300 / 4096) * VRT; // conversion to voltage
		VR = 3300 - tVar.dWaterTemp2mv;
		RT = tVar.dWaterTemp2mv / (VR / RT0); // RT0 = R = 10KOhm
		ln = log(RT / RT0);
		tVar.dWaterTemp2 = (1 / ((ln / B) + (1 / T0))); // Temperature from thermistor
		tVar.dWaterTemp2 = tVar.dWaterTemp2 - 273.15;
		tVar.dWaterTemp2 = (double)tParam.CALIB_WT2_A * tVar.dWaterTemp2 / 100 + (double)tParam.CALIB_WT2_B / 100;
#else
		VRT = dWaterTemp2Sum / nCountWt2Sample;	  // acquisition analog value of VRT
		tSensorIrriValue.dWaterTemp2mv = (5000 / 4096) * VRT; // conversion to voltage
		VR = 5000 - tSensorIrriValue.dWaterTemp2mv;
		RT = tSensorIrriValue.dWaterTemp2mv / (VR / RT0); // RT0 = R = 10KOhm
		ln = log(RT / RT0);
		tSensorIrriValue.dWaterTemp2 = (1 / ((ln / B) + (1 / T0))); // Temperature from thermistor
		tSensorIrriValue.dWaterTemp2 = tSensorIrriValue.dWaterTemp2 - 273.15;
		//tSensorIrriValue.dWaterTemp2 = (double)tSensorParam.CALIB_WT2_A * tSensorIrriValue.dWaterTemp2 / 100 + (double)tSensorParam.CALIB_WT2_B / 100;
		tSensorIrriValue.dWaterTemp2 += (double)tFamInfo.nCalib_Wt2B/100;
#endif

/* Calculate EC1 value */
#ifdef VOL_AMP_3V3
		tVar.dEc1mv = dEc1Sum * 3300 / 4096 / nCountEc1Sample;
		tVar.dEc1 = (double)tParam.CALIB_EC1_A * tVar.dEc1mv / 100 / 1000 + (double)tParam.CALIB_EC1_B / 100;
		tVar.dEc1 /= (1 + a * tVar.dWaterTemp1 - a * (T0 - 273.15)); // calib at T0 degree
		if (tVar.dEc1 < 0)
			tVar.dEc1 = 0;
#else
		tSensorIrriValue.dEc1mv = dEc1Sum * 5000 / 4096 / nCountEc1Sample;
		tSensorIrriValue.dEc1 = (double)tFamInfo.nCalib_Ec1A * tSensorIrriValue.dEc1mv / 100 / 1000 + (double)tFamInfo.nCalib_Ec1B / 100;
		tSensorIrriValue.dEc1 /= (1 + a * tSensorIrriValue.dWaterTemp1 - a * (T0 - 273.15)); // calib at T0 degree
		if (tSensorIrriValue.dEc1 < 0)
			tSensorIrriValue.dEc1 = 0;
#endif

/* Calculate EC2 value */
#ifdef VOL_AMP_3V3
		tVar.dEc2mv = dEc2Sum * 3300 / 4096 / nCountEc2Sample;
		tVar.dEc2 = (double)tParam.CALIB_EC2_A * tVar.dEc2mv / 100 / 1000 + (double)tParam.CALIB_EC2_B / 100;
		tVar.dEc2 /= (1 + a * tVar.dWaterTemp2 - a * (T0 - 273.15));
		if (tVar.dEc2 < 0)
			tVar.dEc2 = 0;
#else
		tSensorIrriValue.dEc2mv = dEc2Sum * 5000 / 4096 / nCountEc2Sample;
		tSensorIrriValue.dEc2 = (double)tFamInfo.nCalib_Ec2A * tSensorIrriValue.dEc2mv / 100 / 1000 + (double)tFamInfo.nCalib_Ec2B / 100;
		tSensorIrriValue.dEc2 /= (1 + a * tSensorIrriValue.dWaterTemp2 - a * (T0 - 273.15));
		if (tSensorIrriValue.dEc2 < 0)
			tSensorIrriValue.dEc2 = 0;
#endif

		/* Calculate PH1 value */
		tSensorIrriValue.dPh1mv = dPh1Sum * 5000 / 4096 / nCountPh1Sample;
		tSensorIrriValue.dPh1 = (double)tFamInfo.nCalib_Ph1A * tSensorIrriValue.dPh1mv / 100000 + (double)tFamInfo.nCalib_Ph1B / 100;
		//		tVar.dPh1	= tVar.dPh1 + 0.02*(tVar.dWaterTemp1-25);

		/* Calculate PH2 value */
		tSensorIrriValue.dPh2mv = dPh2Sum * 5000 / 4096 / nCountPh2Sample;
		tSensorIrriValue.dPh2 = (double)tFamInfo.nCalib_Ph2A * tSensorIrriValue.dPh2mv / 100000 + (double)tFamInfo.nCalib_Ph2B / 100;
		//		tVar.dPh2	= tVar.dPh2 + 0.02*(tVar.dWaterTemp2-25);

		tSensorIrriValue.dEcAvg = (tSensorIrriValue.dEc1 + tSensorIrriValue.dEc2) / 2;
		tSensorIrriValue.dPhAvg = (tSensorIrriValue.dPh1 + tSensorIrriValue.dPh2) / 2;
		tSensorIrriValue.dWtAvg = (tSensorIrriValue.dWaterTemp1 + tSensorIrriValue.dWaterTemp2) / 2;
		
		/* Reset for next period */
		dEc1Sum = 0;
		dWaterTemp1Sum = 0;
		dEc2Sum = 0;
		dWaterTemp2Sum = 0;
		dPh1Sum = 0;
		dPh2Sum = 0;
		nCountIrriSample = 0;
		nCountEc1Sample = 0;
		nCountWt1Sample = 0;
		nCountEc2Sample = 0;
		nCountWt2Sample = 0;
		nCountPh1Sample = 0;
		nCountPh2Sample = 0;
	}

	return true;
}

bool SENSOR_IrriCheck(void)
{
	double dDeltaEc = fabs(tSensorIrriValue.dEc1 - tSensorIrriValue.dEc2);
	double dAvgEc = (tSensorIrriValue.dEc1 + tSensorIrriValue.dEc2) / 2;
	double dDeltaPh = fabs(tSensorIrriValue.dPh1 - tSensorIrriValue.dPh2);
	double dAvgPh = (tSensorIrriValue.dPh1 + tSensorIrriValue.dPh2) / 2;
	double dDeltaWt = fabs(tSensorIrriValue.dWaterTemp1 - tSensorIrriValue.dWaterTemp2);
	double dAvgWt = (tSensorIrriValue.dWaterTemp1 + tSensorIrriValue.dWaterTemp2) / 2;

	if (dDeltaEc >= (double)tFamInfo.nThrehold_ECDual / 100)
		tSensorIrriValue.tEcCheck = CHECK_EC_DUAL;
	else if (dAvgEc >= (double)tFamInfo.nThrehold_ECHigh / 100)
		tSensorIrriValue.tEcCheck = CHECK_EC_HIGH;
	else if (dAvgEc <= (double)tFamInfo.nThrehold_EcLow / 100)
		tSensorIrriValue.tEcCheck = CHECK_EC_LOW;
	else
		tSensorIrriValue.tEcCheck = CHECK_EC_OK;

	if (dDeltaPh >= (double)tFamInfo.nThrehold_PHDual / 100)
		tSensorIrriValue.tPhCheck = CHECK_PH_DUAL;
	else if (dAvgPh >= (double)tFamInfo.nThrehold_PHHigh / 100)
		tSensorIrriValue.tPhCheck = CHECK_PH_HIGH;
	else if (dAvgPh <= (double)tFamInfo.nThrehold_PHLow / 100)
		tSensorIrriValue.tPhCheck = CHECK_PH_LOW;
	else
		tSensorIrriValue.tPhCheck = CHECK_PH_OK;

	if (dDeltaWt >= (double)tFamInfo.nThrehold_WTDual / 10)
		tSensorIrriValue.tWtCheck = CHECK_WT_DUAL;
	else if (dAvgWt >= (double)tFamInfo.nThrehold_WTHigh / 10)
		tSensorIrriValue.tWtCheck = CHECK_WT_HIGH;
	else if (dAvgWt <= (double)tFamInfo.nThrehold_WTLow / 10)
		tSensorIrriValue.tWtCheck = CHECK_WT_LOW;
	else
		tSensorIrriValue.tWtCheck = CHECK_WT_OK;

	return true;
}

/*
Input: stand: standard solution
Output: retEc1: CALIB_EC1_A
Output: retEc2: CALIB_EC2_A
*/
bool SENSOR_CalibEc(int32_t stand, int32_t *retEc1, int32_t *retEc2)
{
	double cal_Ec1_A, cal_Ec2_A;

	cal_Ec1_A = (double)(stand * (1 + a * tSensorIrriValue.dWaterTemp1 - a * (T0 - 273.15)));
	cal_Ec1_A = (double)cal_Ec1_A / 10;
	cal_Ec1_A = (double)(cal_Ec1_A) - (double)(tFamInfo.nCalib_Ec1B);
	cal_Ec1_A = (double)cal_Ec1_A * 1000;
	cal_Ec1_A = (double)cal_Ec1_A / tSensorIrriValue.dEc1mv;
	*retEc1 = (int16_t)cal_Ec1_A;

	cal_Ec2_A = (double)(stand * (1 + a * tSensorIrriValue.dWaterTemp2 - a * (T0 - 273.15)));
	cal_Ec2_A = (double)cal_Ec2_A / 10;
	cal_Ec2_A = (double)(cal_Ec2_A) - (double)(tFamInfo.nCalib_Ec2B);
	cal_Ec2_A = (double)cal_Ec2_A * 1000;
	cal_Ec2_A = (double)cal_Ec2_A / tSensorIrriValue.dEc2mv;
	*retEc2 = (int16_t)cal_Ec2_A;

	return true;
}

/*
input: stand : standard solution
output: ph1A, ph1B, ph2A, ph2B
*/
bool SENSOR_CalibPh(int32_t stand, int32_t *ph1A, int32_t *ph1B, int32_t *ph2A, int32_t *ph2B)
{
	double cal;

	switch (stand)
	{
	case 401:
		cal = (double)tFamInfo.nVoltage_Ph1_Newtral - tSensorIrriValue.dPh1mv;
		cal = (double)(7.01 - 4.01) / cal;
		cal = (double)cal * 100000;
		*ph1A = (int16_t)cal;
		cal = (double)*ph1A / 100000;
		cal = (double)cal * tSensorIrriValue.dPh1mv;
		cal = (double)(4.01 - cal);
		cal = (double)cal * 100;
		*ph1B = (int16_t)cal;

		cal = (double)tFamInfo.nVoltage_Ph2_Newtral - tSensorIrriValue.dPh2mv;
		cal = (double)(7.01 - 4.01) / cal;
		cal = (double)cal * 100000;
		*ph2A = (int16_t)cal;
		cal = (double)*ph2A / 100000;
		cal = (double)cal * tSensorIrriValue.dPh2mv;
		cal = (double)(4.01 - cal);
		cal = (double)cal * 100;
		*ph2B = (int16_t)cal;

		tFamInfo.nVoltage_Ph1_Acid = tSensorIrriValue.dPh1mv;
		tFamInfo.nVoltage_Ph2_Acid = tSensorIrriValue.dPh2mv;
		break;
	case 701:
		cal = (double)tSensorIrriValue.dPh1mv - (double)tFamInfo.nVoltage_Ph1_Acid;
		cal = (double)(7.01 - 4.01) / cal;
		cal = (double)(cal * 100000);
		*ph1A = (int16_t)cal;
		cal = (double)*ph1A / 100000;
		cal = (double)cal * tSensorIrriValue.dPh1mv;
		cal = (double)(7.01 - cal);
		cal = (double)cal * 100;
		*ph1B = (int16_t)cal;

		cal = (double)tSensorIrriValue.dPh2mv - (double)tFamInfo.nVoltage_Ph2_Acid;
		cal = (double)(7.01 - 4.01) / cal;
		cal = (double)(cal * 100000);
		*ph2A = (int16_t)cal;
		cal = (double)*ph2A / 100000;
		cal = (double)cal * tSensorIrriValue.dPh2mv;
		cal = (double)(7.01 - cal);
		cal = (double)cal * 100;
		*ph2B = (int16_t)cal;

		tFamInfo.nVoltage_Ph1_Newtral = tSensorIrriValue.dPh1mv;
		tFamInfo.nVoltage_Ph2_Newtral = tSensorIrriValue.dPh2mv;
		break;
	}

	return true;
}

bool SENSOR_CalibWt(volatile float avg)
{
	double a0 = avg*100;
	double a1 = tSensorIrriValue.dWaterTemp1*100;
	double a2 = tSensorIrriValue.dWaterTemp2*100;
	
	/* calib for wt 1 */
	if (a0 >= a1)
	{
		tFamInfo.nCalib_Wt1B += (uint16_t)(a0 - a1);
	}
	else
	{
		tFamInfo.nCalib_Wt1B -= (uint16_t)(a0 - a1);
	}
	
	/* calib for wt 2 */
	if (a0 >= a2)
	{
		tFamInfo.nCalib_Wt2B += (uint16_t)(a0 - a2);
	}
	else
	{
		tFamInfo.nCalib_Wt2B -= (uint16_t)(a0 - a2);
	}

	return true;
}
