/*
 * cliSensor.h
 *
 *  Created on: 2020. 8. 26.
 *      Author: Cuong Le Hung
 */
#ifndef __CLISENSOR_H
#define __CLISENSOR_H

#include "stm32f4xx_hal.h"
#include <stdlib.h>
#include <stdbool.h>

#define MAX_STRING	60


typedef enum
{
	CHECK_TEMP_OK = 1,
	CHECK_TEMP_DUAL,
	CHECK_TEMP_HIGH,
	CHECK_TEMP_LOW
} CheckTemp_t;

typedef enum
{
	CHECK_HUMI_OK = 10,
	CHECK_HUMI_DUAL,
	CHECK_HUMI_HIGH,
	CHECK_HUMI_LOW
} CheckHumi_t;

typedef enum
{
	CHECK_LIGHT_OK = 20,
	CHECK_LIGHT_DUAL,
	CHECK_LIGHT_HIGH,
	CHECK_LIGHT_LOW
} CheckLight_t;

typedef struct
{
	float fCliTemp1;
	float fCliTemp2;
	float fCliHumi1;
	float fCliHumi2;
	uint16_t nCliLight1;
	uint16_t nCliLight2;
	float fCliTempAvg;
	float fCliHumiAvg;
	uint16_t nCliLightAvg;
	CheckTemp_t tTempCheck;
	CheckHumi_t tHumiCheck;
	CheckLight_t tLightCheck;
} _SENSOR_CLI;

void SENSOR_CliInit(UART_HandleTypeDef *handle);
bool SENSOR_CalibTemp(float avg);
bool SENSOR_CalibHumi(float avg);
bool SENSOR_CalibLight(uint16_t avg);
bool SENSOR_CliRead(void);
	
#endif
